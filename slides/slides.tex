\documentclass[xcolor=dvipsnames, 9pt]{beamer}

% Packages

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{fourier}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{nicefrac}
\usepackage[nouppercase]{frontespizio}
\usepackage{cancel}
\usepackage{tikz}
\usetikzlibrary{patterns}
\usepackage{cool}

%% Theorems

\theoremstyle{definition}
\newtheorem{mydef}{Definition}
\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{cor}{Corollary}
\newtheorem{lmm}{Lemma}

%% Theme

\usetheme{Madrid}
\usecolortheme[named=ForestGreen]{structure}

%% Newcommands definition 

\newcommand\cplus{\mathbin{\raisebox{-7pt}{$\,+\,$}}} % lower plus for c.f.
\newcommand\contdots{\raisebox{-7pt}{$\vphantom{+}\dotsm$}} % lower dots for c.f.
\newcommand{\K}{\mathop{\raisebox{-3pt}{\normalfont\huge K}}}
\newcommand{\sK}{\mathop{\raisebox{-0pt}{\normalfont\large K}}}
\newcommand{\br}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\conj}[1]{\overline{#1}}
\newcommand{\floor}[1]{\left\lfloor{#1}\right\rfloor}
\def\NN{\mathbb{N}}
\def\CC{\mathbb{C}}
\def\RR{\mathbb{R}}
\def\ZZ{\mathbb{Z}}
\def\calR{\mathcal{R}}
\def\calI{\mathcal{I}}
\newcommand{\lgamma}{\mathop{\normalfont{\text{\text{lgamma}}}}}
\newcommand{\lbeta}{\mathop{\normalfont{\text{lbeta}}}}
\newcommand{\logs}{\mathop{\normalfont{\text{logs}}}}
\newcommand{\erfc}{\mathop{\normalfont{\text{erfc}}}}
\newcommand{\sign}{\mathop{\normalfont{\text{sign}}}}
\newcommand{\Ei}{\mathop{\normalfont{\text{Ei}}}}
\newcommand{\Si}{\mathop{\normalfont{\text{Si}}}}
\newcommand{\Ci}{\mathop{\normalfont{\text{Ci}}}}
\newcommand{\Ai}{\mathop{\normalfont{\text{Ai}}}}
\newcommand{\ii}{\mathop{\normalfont{\text{i}}}}
\newcommand{\ee}{\mathop{\normalfont{\text{e}}}{}} % da usare al posto di e
\newcommand{\diff}{\mathop{}\!d}
\def\t{\tau}

%% Title

\title[Numerical Evaluation of Special Functions]{Numerical Evaluation \\of some Special Functions \\in GNU Octave}
\subtitle{Google Summer of Code 2017}
\author{Michele Ginesi}
\date{October 11, 2017}
%%%%%%%%%%%
%% Begin %%
%%%%%%%%%%%
\begin{document}
\begin{frame}
\frametitle{$\,$}
\titlepage
\end{frame}

\frametitle{Introduction}
\section{Introduction}
\begin{frame}[fragile]
\frametitle{Introduction}
\begin{mydef}
A special function is a ``non classical'' function which have a more or less established name and notation due to its importance.
\end{mydef}
\pause
\begin{itemize}
\item Incomplete gamma functions
\item Incomplete beta functions
\item Exponential integral, sine integral and cosine integral
\item Bessel functions
\end{itemize}
\color{white}$\,$
\begin{center}
This is \textbf{not} an old problem!
\end{center}
\begin{verbatim}
>> abs(gammainc(-50,1,'upper') - exp (50)) / exp(50)
ans =
   8.2396e+03
\end{verbatim}

\end{frame}

\begin{frame}[fragile]
\frametitle{Introduction}
\begin{mydef}
A special function is a ``non classical'' function which have a more or less established name and notation due to its importance.
\end{mydef}
\begin{itemize}
\item \textcolor{red}{\underline{Incomplete gamma functions}}
\item Incomplete beta functions
\item Exponential integral, sine integral and cosine integral
\item Bessel functions
\end{itemize}
\pause
$\,$
\begin{center}
This is \textbf{not} an old problem!
\end{center}\pause
\begin{verbatim}
>> abs(gammainc(-50,1,'upper') - exp (50)) / exp(50)
ans =
   8.2396e+03
\end{verbatim}
\end{frame}

\begin{frame}
\frametitle{Continued fractions: notation}
We will use two different notations:
\[ b_0 + \K_{m=1}^{\infty} \br{ \frac{ a_m }{ b_m } } ,\]
and
\[ b_0 + \frac{a_1}{b_1} \cplus \frac{a_2}{b_2} \cplus \frac{a_3}{b_3}\cplus\contdots, \]
to represent the same object:
\[ b_0 + \cfrac{ a_1 }{ b_1 + \cfrac{ a_2 }{ b_2 + \cfrac{ a_3 }{ b_3 + \raisebox{-0.6\height}{$\ddots$} } } }.\]
\end{frame}


%%%%%%%%%%%%%%%%
%%  GAMMAINC  %%
%%%%%%%%%%%%%%%%
\section{Incomplete Gamma function}
\subsection{Definitions}
\begin{frame}
\frametitle{Incomplete gamma functions}
\textcolor{blue}{
Incomplete gamma functions:
\begin{align*}
\gamma(a,x) & = \int_0^x \ee^{-t} t ^{a-1} \diff t, & \Gamma(a,x) & = \int_x^\infty \ee^{-t} t ^{a-1} \diff t, & \Re a & > 0.
\end{align*}\pause
\[ \gamma(a,x) + \Gamma(a,x) = \Gamma(a). \]
}
\pause
Incomplete gamma function ratios:
\begin{align*}
P(a,x) & = \frac{\textcolor{blue}{\gamma(a,x)}}{\Gamma(a)} & Q(a,x) & = \frac{\textcolor{blue}{\Gamma(a,x)}}{\Gamma(a)}, & \Re a & > 0.
\end{align*}
\pause
``Scaled'' options:
\begin{align*}
P_S(a,x) & = \frac {P (a, x)} {\textcolor{red}{D (a, x)}}, & Q_S(a,x) & = \frac {Q (a, x)} {\textcolor{red}{D (a, x)}},
\end{align*}
with
\[ \textcolor{red}{ D(a,x) = \frac {x ^ a \ee ^ {-x}}{\Gamma (a + 1)}. }\]
\end{frame}

\begin{frame}
\frametitle{Incomplete gamma functions: property}
\begin{prop}
The following identities hold:
\begin{align*}
P (a, x) + Q (a, x) &= 1, \\\\
P_S (a, x) + Q_S (a, x) & = \frac {\Gamma (a + 1) \ee ^ {x}} { x ^ a}.
\end{align*}
\end{prop}
\pause
\begin{center}
We will focus on real values of $x$ and real positive values of $a$.
\end{center}
\end{frame}

\subsection{Computation diagram}

\begin{frame}
\frametitle{Incomplete gamma function: computation diagram}
\begin{figure}[hbt]
\centering
\resizebox{0.9\textwidth}{!}{
\input{manuscript/gammaincscheme.tex}}
\end{figure}
\end{frame}

\subsection{Trivial values}
\begin{frame}
\frametitle{\textcolor{green}{Incomplete gamma function: trivial values}}
\begin{itemize}
\item $a = 0, x = 0$:
\begin{align*}
P(0,0) = P_S(0,0) & = 0, & Q(0,0) = Q_S(0,0) & = 1.
\end{align*}
\pause
\item $a \neq 0, x = 0$:
\begin{align*}
P(a, 0) & = 0, & P_S(a,0) & = 1, & Q(a,0) & = 1, & Q_S(a,0) & = +\infty.
\end{align*}
\pause
\item $a = 0, x \neq 0$:
\begin{align*}
P(0,x)&= 1, & P_S(0,x) &= \ee^{x}, & Q(0,x) & = 0, & Q_S(0,x) &= 0.
\end{align*}
\pause
\item $a = 1, x\neq 0$:
\begin{align*}
P(1,x) & = 1 - \ee^{-x}, & P_S(1,x) & = \frac{\ee^{-x}-1}{x}, & Q(1,x) & = \ee^{-x}, & Q_S(1,x) & = \frac{1}{x}.
\end{align*}
\end{itemize}
\end{frame}

\subsection{$a \in \NN$}
\begin{frame}
\frametitle{\textcolor{red}{Incomplete gamma function: $a \in \NN$}}
By successive integration by parts:
\begin{align*}
\gamma(n,x) & = \int_0^x \ee^{-t}t^{n-1} \diff t\\
  & = \sbr{ -\ee^{-t} t^{n-1} }_{t=0}^{t=x} + (n-1) \underbrace{ \int_0^x \ee^{-t} t ^{n-2} \diff t }_{ \gamma(n-1,x) } \\
  & = -\ee^{-x} ({ x ^ {n-1} + (n-1) x ^ {n-2} + (n-1)(n-2) x ^ {n-3} + \ldots + (n-1)! \underbrace{(1 - \ee^{-x} )}_{ \gamma (1,x) } } )
  \end{align*}
\pauses
\begin{align*}
\gamma(n,x) & = (n-1)!\br{ 1 - \ee^{-x} \sum_{k=0}^{n-1} \frac{x^k}{k!} }, & \Gamma(n,x) & = (n-1)! \ee^{-x} \sum_{k=0}^{n-1} \frac{x^k}{k!}.
\end{align*}
\pause
To recover $P(a,x)$ and $Q(a,x)$, we recall \textcolor{red}{$\Gamma(n) = (n-1)!$, for $n \in \NN$}:
\begin{align*}
P(n,x) & = 1 - \ee^{-x} \sum_{s=0}^{n-1} \frac{x^s}{s!}, & Q(n,x) & = \ee^{-x} \sum_{s=0}^{n-1} \frac{x^s}{s!}.
\end{align*}
To recover the ``scaled'' options, just divide by $D(n,x)$.
\end{frame}

\subsection{Series expansion}
\begin{frame}
\frametitle{\textcolor{yellow}{Incomplete gamma function: series expansion}}
Again by successive integration by parts:
\begin{align*}
\gamma(a,x) 	& = \int_0^x t^{a-1}\ee^{-t}\, dt \\
				& = \sbr{ \frac{\ee^{-t} t^a} {a} }_{t=0}^{t=x}  + \frac{1}{a} {\int_0^x t^a \ee^{-t}\diff t} \\
				& = \ee^{-x} x^a \br{\frac{1}{a} + \frac{x}{a(a+1)} + \frac{x^2}{a(a+1)(a+2)} + \ldots}
\end{align*}
\pause
$P(a,x)$ and $P_S(a,x)$ can be recovered as, \textcolor{red}{recalling $\Gamma(a+1) = a\Gamma(a)$},
\begin{align*}
P(a,x) & = D(a,x)\br{ 1 + \frac{x}{a+1} + \frac{x^2}{(a+1)(a+2)}} & P_S(a,x) &=  1 + \frac{x}{a+1} + \frac{x^2}{(a+1)(a+2)} + \ldots
\end{align*}
\pause
\begin{align*}
Q(a,x) & = 1 - P(a,x), & Q_S(a,x) & = \frac{1}{D(a,x)} - P_S(a,x).
\end{align*}
\end{frame}

\subsection{Continued fractions}

\begin{frame}
\frametitle{\textcolor{blue}{Incomplete gamma function: continued fractions}}
From the series expansion and using the \emph{Euler's connection} we get
\begin{align*}
S_a(x) & = \frac{\Gamma(a,x)}{x^a \ee ^{-x}} = \frac{1}{1+x-a}\cplus \frac{-(1-a)}{3+x-a} \cplus \frac{-2(2-a)}{5+x-a} \cplus \frac{-3(3-a)}{7+x-a} \cplus \contdots,
\end{align*}
\begin{align*}
s_a(x) & = \frac{\gamma(a,x)}{x ^ a \ee ^{-x}} = \frac{1}{x} \K_{m=1}^\infty \br{ \frac{c_m(a) x}{1} }
\end{align*}
with
\begin{align*}
c_1(a) & = \frac{1}{a}, & c_{2j}(a) &=\frac{-(a+j+1)}{(a+2j-2)(a+2j-1)}, &c_{2j+1}(a) & =\frac{j}{(a+2j-1)(a+2j)}, & j&\in\NN.
\end{align*}
\pause
We compute $P(a,x)$, $P_S(a,x)$, $Q(a,x)$ and $Q_S(a,x)$ as
\begin{align*}
P(a,x) & = a s_a(x) D(a,x), & P_S(a,x) & = a s_a(x),\\
Q(a,x) & = a S_a(x) D(a,x), & Q_S(a,x) & = a S_a(x).
\end{align*}
\end{frame}

\subsection{Recurrence relations}

\begin{frame}
\frametitle{Incomplete gamma function: recurrence relations}
\begin{prop}[Recurrence formula for the incomplete gamma function] The lower gamma function ratio $P(a,x)$ satisfies the following recurrence formula
\begin{equation*}
P(a + 1, x) = P(a,x) - D(a,x).
\end{equation*}
\end{prop}
\pause
By iterating and manipulating it, we obtain:
\begin{align*}
P(a,x) & = P(a+2,x) + D(a+1,x) + D(a,x), \\
Q(a,x) & = Q(a+2,x) - D(a+1,x) - D(a,x), \\
P_S(a,x) & = P_S(a+2,x) \frac{x^2}{(a+2)(a+1)} + \frac{x}{a+1} + 1,\\
Q_S(a,x) & = Q_S(a+2,x) \frac{x^2}{(a+2)(a+1)} - \frac{x}{a+1} - 1.
\end{align*}
\end{frame}

\subsection{Incomplete gamma function: results}
\begin{frame}
\frametitle{Incomplete gamma function: results}
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.9\columnwidth]{plots/plot_err_gammainc.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%
%%  BETAINC  %%
%%%%%%%%%%%%%%%
\section{Incomplete beta function}
\subsection{Definitions}
\begin{frame}
\frametitle{Incomplete beta functions}
\begin{align*}
I_x(a,b) & = \frac{1}{B(a,b)} \int_0^x t^{a-1} (1-t)^{b-1} \diff t, & 0 \leq x \leq 1, \quad a,b>0,\\
I_x^U(a,b) & = \frac{1}{B(a,b)} \int_x^1 t^{a-1} (1-t)^{b-1} \diff t, & 0 \leq x \leq 1, \quad a,b>0.
\end{align*}
\pause
$\,$\\
The following identities hold:
\[ I_x(a,b) + I_x^U(a,b) = 1 , \]
$\,$
\[ I_x^U(a,b) = I_{1-x} (b,a) .\]
\end{frame}

\subsection{Numerical evaluation}
\begin{frame}
\frametitle{Incomplete beta function: continued fractions}
\begin{align*}
\frac{ B(a,b) I_x(a,b) } { x^a (1-x)^b } & = \K_{m=1}^\infty \br{ \frac{ \alpha_m(a,b;x) }{ \beta_m(a,b;x) } }, & a,b & > 0,\quad 0 < x < 1,\\\\
\alpha_1(a,b;x) & = 1, \\
\alpha_{j+1} (a,b;x) & = \frac{ (a+j-1)(a+b+j-1)(b-j)j }{ (a+2j-1)^2 }x^2, & j&\geq 1, \\
\beta_{j+1}(a,b;x) & = a + 2j + \br{ \frac{ j(b-j) }{ a+2j-1 } - \frac{ (a+j)(a+b+j) }{ a+2j+1 } }x, & j & \geq 0.
\end{align*}
\pause
$\,$\\
This expansion is more useful when
\[\textcolor{red}{ x \leq \frac{ a }{ a+b }.} \]
\end{frame}

\subsection{Numerical results}
\begin{frame}
\frametitle{Incomplete beta function: results}
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.9\columnwidth]{plots/plot_err_betainc.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%
%%  EXPINT  %%
%%%%%%%%%%%%%%
\section{Integral functions}
\subsection{Definitions}
\begin{frame}
\frametitle{Integral functions}
\begin{align*}
E_1(z) & = \int_z^\infty \frac{\ee^{-t}}{t} \diff t, & \Re z & > 0, \\
\Si (z) & = \int_0^z \frac{\sin (t)}{t} \diff t, & z & \in \CC, \\
\Ci (z) & = - \int_z^\infty \frac{\cos (t)}{t} \diff t, & |\arg(z)| & < \pi.
\end{align*}
Analytic continuation extend $E_1(z)$ to all $|\arg(z)|<\pi$.
\end{frame}

\subsection{Numerical evaluation of $E_1(z)$}
\begin{frame}
\frametitle{Exponential integral: computation diagram}
\begin{figure}[hbt]
\centering
\resizebox{0.6\columnwidth}{!}{
\input{manuscript/expintscheme.tex}}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Exponential integrals: expansions}
\begin{align*}
\textcolor{blue}{\bullet}E_1(z) & = -\gamma -\log(z) - \sum_{k=1}^\infty \frac{ (-1)^k z^k }{ k\, k! }, \\\\
\textcolor{green}{\bullet}E_1(z) & = \ee^{-z} \br{ \frac{1}{z} \cplus \frac{1}{1} \cplus \frac{1}{z} \cplus\frac{2}{1}\cplus\frac{2}{z}\cplus\frac{3}{1}\cplus\frac{3}{z}\cplus\contdots},\\\\
\textcolor{red}{\bullet}E_1(z) & \approx \frac{\ee^{-z}}{z} \sum_{k=0}^N \frac{(-1)^k k!}{z^k}, & N = \floor{|z|}.
\end{align*}
\end{frame}

\subsection{Numerical results for $E_1(z)$}

\begin{frame}
\frametitle{Exponential integral: results}
\begin{figure}[hbt]
\centering
\includegraphics[width=0.9\columnwidth]{plots/plot_err_expint.eps}
\end{figure}
\end{frame}

\subsection{Numerical evaluation of $\Si(z)$ and $\Ci(z)$}

\begin{frame}
\frametitle{Sine and cosine integrals}
Series expansions:
\begin{align*}
\Si(z) & = \sum_{k=0}^\infty \frac{(-1)^k z^{2k+1}}{(2k+1) \, (2k+1)!}, \\
\Ci(z) & = \gamma + \log(z) + \sum_{k=0}^\infty \frac{ (-1)^k z^{2k} }{ 2k\, (2k)! }.
\end{align*}
\pause
Relations with $E_1(z)$:
\begin{align*}
\Si (z) & = -\frac{\ii}{2} (E_1(\ii z) - E_1(-\ii z)) + \frac{\pi}{2}, & |\arg(z)| & < \frac{\pi}{2},\\
\Ci (z) & = -\frac{1}{2} (E_1(\ii z) + E_1(-\ii z)) + \frac{\pi}{2}, & |\arg(z)| & < \frac{\pi}{2},
\end{align*}
\pause
extended to the whole complex plane by
\begin{align*}
\Si(-z) & = \Si(z), & \Si (\conj{z}) & = \conj{\Si(z)}, \\
\Ci(-z) & = \Ci (z) - \ii \pi, \quad 0<\arg(z)<\pi, & \Ci (\conj{z}) & = \conj{\Ci(z)}.
\end{align*}
\end{frame}

\subsection{Numerical results for $\Si(z)$ and $\Ci(z)$}

\begin{frame}
\frametitle{Sine and cosine integrals: results}
\begin{figure}[hbt]
\centering
\includegraphics[width=0.5\textwidth]{plots/plot_err_sinint.eps}\includegraphics[width=0.5\textwidth]{plots/plot_err_cosint.eps}
\end{figure}
\end{frame}

\section{}

\begin{frame}
\frametitle{$\,$}
\begin{center}
Thank you for your kind attention.
\end{center}
\end{frame}


\end{document}
