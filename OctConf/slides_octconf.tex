\documentclass[xcolor=dvipsnames, 9pt]{beamer}

% Packages

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{fourier}
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{nicefrac}
\usepackage[nouppercase]{frontespizio}
\usepackage{cancel}
\usepackage{tikz}
\usetikzlibrary{patterns}
\usepackage{cool}
\usepackage{background}
\usepackage{xcolor}
\usepackage[printwatermark]{xwatermark}
\usepackage{wrapfig}
%% Theorems

\theoremstyle{definition}
\newtheorem{mydef}{Definition}
\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{cor}{Corollary}
\newtheorem{lmm}{Lemma}

%% Theme
\usetheme[width=3\baselineskip]{Berkeley}%{Antibes}%{Madrid}
\usecolortheme[named=BlueViolet]{structure}
%% Comment the following lines to not use the logo in background
%\setbeamertemplate{background}{\BgMaterial}
%\backgroundsetup{
%        placement=top,
%        position={\paperwidth-0.75in,-3.0in},
%        scale=1,
%        opacity=0.3,
%        contents={\includegraphics[width = 0.3\linewidth]{OctConf/imgs/logo.png}},
%}

%% Newcommands definition 

\newcommand\cplus{\mathbin{\raisebox{-5pt}{$\,+\,$}}} % lower plus for c.f.
\newcommand\contdots{\raisebox{-7pt}{$\vphantom{+}\dotsm$}} % lower dots for c.f.
\newcommand{\K}{\mathop{\raisebox{-3pt}{\normalfont\huge K}}}
\newcommand{\sK}{\mathop{\raisebox{-0pt}{\normalfont\large K}}}
\newcommand{\br}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\conj}[1]{\overline{#1}}
\newcommand{\floor}[1]{\left\lfloor{#1}\right\rfloor}
\def\NN{\mathbb{N}}
\def\CC{\mathbb{C}}
\def\RR{\mathbb{R}}
\def\ZZ{\mathbb{Z}}
\def\calR{\mathcal{R}}
\def\calI{\mathcal{I}}
\newcommand{\lgamma}{\mathop{\normalfont{\text{\text{lgamma}}}}}
\newcommand{\lbeta}{\mathop{\normalfont{\text{lbeta}}}}
\newcommand{\logs}{\mathop{\normalfont{\text{logs}}}}
\newcommand{\erfc}{\mathop{\normalfont{\text{erfc}}}}
\newcommand{\sign}{\mathop{\normalfont{\text{sign}}}}
\newcommand{\Ei}{\mathop{\normalfont{\text{Ei}}}}
\newcommand{\Si}{\mathop{\normalfont{\text{Si}}}}
\newcommand{\Ci}{\mathop{\normalfont{\text{Ci}}}}
\newcommand{\Ai}{\mathop{\normalfont{\text{Ai}}}}
\newcommand{\ii}{\mathop{\normalfont{\text{i}}}}
\newcommand{\ee}{\mathop{\normalfont{\text{e}}}{}} % da usare al posto di e
\newcommand{\diff}{\mathop{}\!d}
\def\t{\tau}
\def\matlab{M{\small ATLAB} $\,$}
%% Title

\title{Make Specfuns Special Again}
\subtitle{Google Summer of Code 2017}
\author{Michele Ginesi}
\date{March 12 2018\\$\,$\\OctConf 2018}

\begin{document}
\begin{frame}
\frametitle{$\,$}
\titlepage
\end{frame}

\begin{frame}{Nice to meet you}
\textbf{Michele Ginesi}.\\
Bachelor Degree in Applied Mathematics, University of Verona.\\
Master Degree in Mathematics, University of Verona.\\
Currently: PhD student in Computer Science in University of Verona.\\
Mail: \href{mailto:michele.ginesi@gmail.com}{michele.ginesi@gmail.com}.\\
Google Summer of Code 2017. Mentors: Marco Caliari and Colin Macdonald.\\
Blog of the project: \url{http://gsocspecfun.blogspot.it/}.\\
Repository with the code: \url{https://bitbucket.org/M_Ginesi/octave}.\\
Repository with my MD thesis: \url{https://bitbucket.org/M_Ginesi/ginesi_michele_thesis}.\\
\end{frame}

\begin{frame}{Contents}
\tableofcontents
\end{frame}

\section{Why?}
\begin{frame}[fragile]{Why special functions need(ed?) a revision?}
Which are the problem with the current (Octave 4.2) release?
\begin{itemize}
\item High relative errors: for example $E_1 (10 \ii)$ has a relative error of \verb|5.99950e-05|;
\item Unnecessary restricted domain: for example $P(a,x)$ requires $x \geq 0$;
\item Missing functions: for example the inverse of the incomplete gamma function.
\end{itemize}
%\pause
List of bugs: \texttt{\href{https://savannah.gnu.org/bugs/?func=detailitem&item_id=47800}{47800}, \href{https://savannah.gnu.org/bugs/?func=detailitem&item_id=51157}{51157}, \href{https://savannah.gnu.org/bugs/?func=detailitem&item_id=47738}{47738}, \href{https://savannah.gnu.org/bugs/?func=detailitem&item_id=48316}{48316}, \href{https://savannah.gnu.org/bugs/?func=detailitem&item_id=48036}{48036}.}
\end{frame}
\section{Integral functions}

\begin{frame}[fragile]{expint}
The term exponential integral refers to the set of functions
\begin{align*}
E_n (z) & = \int_1^\infty \dfrac{\ee^{-zt}}{t^n} \diff t ,& \Re z & > 0,\quad n \in \NN.
\end{align*}
Some use the term to refer to
\begin{align*}
\Ei(z) & = - \int_{-x}^\infty \dfrac{\ee^{-t}}{t} \diff t, & x & > 0.
\end{align*}
For \matlab compatibility, \verb|expint| compute
\begin{align*}
E_1(z) & = \int_z^\infty \frac{\ee^{-t}}{t} \diff t, & \Re z & > 0 \\
\end{align*}
\end{frame}

\begin{frame}{Domain splitting}
\begin{figure}
\centering
\resizebox{0.6\columnwidth}{!}{
\input{slides/expintscheme.tex}}
\caption{Domain splitting for the exponential integral function. Legend: \textcolor{blue}{(blue)} Taylor expansion, \textcolor{green}{(green)} continued fraction, \textcolor{red}{(red)} asymptotic expansion.}
\end{figure}
\end{frame}

\begin{frame}{Numerical expansions}
\begin{align*}
\textcolor{blue}{\bullet}E_1(z) & = -\gamma -\log(z) - \sum_{k=1}^\infty \frac{ (-1)^k z^k }{ k\, k! }, \\ \, \\
\textcolor{green}{\bullet}E_1(z) & = \ee^{-z} \left( \frac{1}{z} \cplus \frac{1}{1} \cplus \frac{1}{z} \cplus\frac{2}{1}\cplus\frac{2}{z}\cplus \frac{3}{1}\cplus\frac{3}{z}\cplus\frac{4}{1}\cplus\frac{4}{z}\cplus\contdots\right),\\ \, \\
\textcolor{red}{\bullet}E_1(z) & \approx \frac{\ee^{-z}}{z} \sum_{k=0}^N \frac{(-1)^k k!}{z^k}, \qquad N = \floor{|z|}.
\end{align*}
\end{frame}

\begin{frame}{Numerical results}
\begin{figure}[hbt]
\centering
\includegraphics[width=0.8\columnwidth]{plots/plot_err_expint.eps}
\caption{Relative error (in $\log_{10}$) for the exponential integral function $E_1(z)$.}
\end{figure}
\end{frame}

\begin{frame}{sinint and cosint}
Integral trigonometric functions:
\begin{align*}
\Si (z) & = \int_0^z \frac{\sin (t)}{t} \diff t, & z & \in \CC, \\
\Ci (z) & = - \int_z^\infty \frac{\cos (t)}{t} \diff t \\
        & = \gamma + \log (z) + \int_0^z \dfrac{ \cos(t) - 1 }{ t } \diff t , & |\arg(z)| & < \pi.
\end{align*}
\end{frame}
\begin{frame}{Numerical expansions}
Implemented using series expansion for $|z| \leq 2$
\begin{align*}
\Si (z) & = \sum_{k=0}^\infty \dfrac{ (-1)^k z ^ {2k+1} }{(2k+1)(2k+1)!}, & \Ci & = \gamma + \log(z) + \sum_{k=0}^\infty \dfrac{(-1)^k z^{2k}}{2k(2k)!},
\end{align*}\pause
and using relations with $E_1(z)$ for $|z| > 2$
\begin{align*}
\Si (z) & = -\frac{\ii}{2} ( E_1(\ii z) - E_1(-\ii z ) +\frac{\pi}{2}, & |\arg(z)| < \frac{\pi}{2}, \\
\Ci (z) & = -\frac{1}{2} ( E_1(\ii z) + E_1(-\ii z ) , & |\arg(z)| < \frac{\pi}{2}.
\end{align*}\pause
These are valid only in some part of the complex domain, but can be extend to the whole $\CC$ using some symmetry and reflection properties.
\begin{align*}
\Si(-z) & = -\Si(z), & \Si(\conj{z}) & = \conj{\Si(z)}, & \Ci(\conj{z}) & = \conj{\Ci(z)}, & z & \in \CC
\end{align*}
and
\begin{align*}
\Ci(-z) & = \Ci (z) - \ii \pi, & 0 < \arg(z) & < \pi.
\end{align*}
\end{frame}

\begin{frame}{Numerical results}
\begin{figure}[hbt]
\centering
\includegraphics[width=0.5\textwidth]{plots/plot_err_sinint.eps}\includegraphics[width=0.5\textwidth]{plots/plot_err_cosint.eps}
\caption{Relative error (in $\log_{10}$) for the sine integral function $\Si(z)$ (left) and cosine integral function $\Ci(z)$ (right).}
\end{figure}
\end{frame}



\section{Incomplete functions}
\begin{frame}{gammainc}
In \matlab four different ``types'' of incomplete gamma function are available, but Octave 4.2 have only the first two of them:
\begin{align*}
P(a,x) & = \dfrac{1}{\Gamma(a)} \int_0^x \ee^{-t} t ^{a-1} \diff t, & \Re a & > 0, \\
Q(a,x) & = \dfrac{1}{\Gamma(a)} \int_x^\infty \ee^{-t} t ^{a-1} \diff t, & \Re a & > 0,
\end{align*}
and the ``scaled'' versions
\begin{align*}
P_S(a,x) & = \frac {P (a, x)} {D (a, x)}, & Q_S(a,x) & = \frac {Q (a, x)} {D (a, x)},
\end{align*}
with
\[ D(a, x) =  \frac {x ^ a \ee ^ {-x}}{\Gamma (a + 1)}. \]

Mathematically speaking, the domain can be extended to $ x\in \CC$, $ \Re a > 0 $. However, there are currently no accurate numerical methods working in the whole domain. In fact, even \matlab accept only $ a \in \RR^+, x \in \RR $, while Octave 4.2 requires also $x \in \RR^+$.
\end{frame}

\begin{frame}{Domain splitting}
\begin{figure}[hbt]
\centering
\resizebox{0.8\textwidth}{!}{
\input{slides/gammaincscheme.tex}}
\caption{Domain splitting for the incomplete gamma function. Legend: \textcolor{green}{(green)} exact formulae, \textcolor{red}{(red)} exact finite sum, \textcolor{yellow}{(yellow)} series expansion, \textcolor{blue}{(blue)} continued fractions. The arrows symbolize the use of a recurrence formula.}
\end{figure}
\end{frame}

\begin{frame}{Numerical expansions I}
The \textcolor{green}{exact formulae} are
\begin{align*}
P(0,0) & = 0, & Q(0,0) & = 1, & P_S(0,0) & = 0, & Q_S(0,0) & = 1,
\end{align*}
\begin{align*}
P(a,0) & = 0, & Q(a, 0) & = 1, & Q_S(a, 0) & = \infty, & P_S(a,0) & = 1,
\end{align*}
\begin{align*}
P(0,x) & = 1, & Q(0,x) & = 0, & P_S(0,x) & = \ee^x, &  Q_S(0, x) & = 0,
\end{align*}
\begin{align*}
P(1,x) & = 1 - \ee^{-x}, & Q(1,x) & = \ee^{-x}, & P_S(1,x) & = \dfrac{\ee^{-x}-1}{x}, & Q_S(1,x) & = \dfrac{1}{x}.
\end{align*}
\end{frame}

\begin{frame}{Numerical expansions II}
\begin{align*}
\textcolor{red}{\bullet} P(n,x) & = 1 - \ee ^{-x} \sum_{s=0}^{n-1} \dfrac{x^s}{s!}, & Q(n,x) & = \ee ^{-x} \sum_{s=0}^{n-1} \dfrac{x^s}{s!} , \qquad n=1,2,\ldots
\end{align*}
\begin{align*}
\textcolor{yellow}{\bullet}  P(a,x) & = \ee^{-x} x ^ a \sum_{n=0}^\infty \dfrac{x^n}{\Gamma(a+n+1)}, & P_S(a,x) &= \Gamma(a+1) \sum_{n=0}^\infty \dfrac{x^n}{\Gamma(a+n+1)}
\end{align*}
\begin{align*}
\textcolor{blue}{\bullet} Q_S(a, x) & = a \br{ \dfrac{1}{1+x-a} \cplus \dfrac{-(1-a)}{3+x-a} \cplus \dfrac{-2(2-a)}{ 5 + x - a } \cplus \dfrac{-3(3-a)}{ 7+x-a }\cplus\contdots }
\end{align*}
\end{frame}
\begin{frame}{Numerical expansions III}
All completed with the identities
\begin{align*}
P(a,x) + Q(a,x) &= 1, & P_S(a,x) + Q_S(a,x) & = \dfrac{1}{D(a,x)},
\end{align*}
and the recurrence formulae
\begin{align*}
P(a,x) & = P(a+2,x) + D(a+1,x) + D(a,x),\\
 Q(a,x) & = Q(a+2, x) - D(a+1, x) - D(a,x), \\
P_S(a,x) & = P_S(a+2,x) \dfrac{x^2}{(a+2)(a+1)} + \dfrac{x}{a+1} + 1, \\
 Q_S(a,x) & = Q_S(a+2,x) \dfrac{x^2}{(a+2)(a+1)} - \dfrac{x}{a+1} - 1.
\end{align*}
\end{frame}

\begin{frame}{Numerical results}
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\columnwidth]{plots/plot_err_gammainc.eps}
\caption{Relative error (in $\log_{10}$) for the lower incomplete gamma function $P(a,x)$.}
\end{figure}
\end{frame}

\begin{frame}{betainc}
In \matlab there are two kinds of incomplete beta function
\begin{align*}
I_x(a,b) & = \frac{1}{B(a,b)} \int_0^x t^{a-1} (1-t)^{b-1} \diff t, & 0 \leq x \leq 1, \quad a,b>0,\\
I_x^U(a,b) & = \frac{1}{B(a,b)} \int_x^1 t^{a-1} (1-t)^{b-1} \diff t, & 0 \leq x \leq 1, \quad a,b>0,
\end{align*}
while in Octave 4.2 only the first one is available.
\end{frame}

\begin{frame}{Numerical expansion}
In this case, only the continued fraction representation is used:
\[ \dfrac{ B(a,b) I_x(a,b) }{ x^a(1-x)^b } = \K_{m=1}^\infty \dfrac{ \alpha_m (a,b;x) }{ \beta_m(a,b;x) } , \]
with
\begin{align*}
\alpha_1 (a,b;x) & = 1, \\
\alpha_{j+1} (a,b;x) & = \dfrac{ (a+j-1) (a+b+j-1) (b-j) j }{ (a+2j-1)^2 } x^2, & j \geq 1, \\
\beta_{j + 1} (a, b; x) & = a + 2j + \br { \dfrac{ j(b-j) }{ a+2j-1 } + \dfrac{ (a+j)(a+b+j) }{ a+2j-1 }} x , & j \geq 0;
\end{align*}
\pause
completed with the properties
\begin{align*}
I_x(a,b) + I_x^U(a,b) & = 1, & I_x^U(a,b) & = I_{1-x}(b,a).
\end{align*}
\end{frame}

\begin{frame}{Numerical results}
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\columnwidth]{plots/plot_err_betainc.eps}
\caption{Relative error (in $\log_{10}$) for the lower incomplete beta function $ I_x(a,b) $.}
\end{figure}
\end{frame}

\begin{frame}{gammaincinv, betaincinv}
The inverse of the incomplete gamma functions (only ``upper'' and ``lower'') and of the incomplete beta functions are implemented via a simple Newton method: we search for $x$ which solves, respectively,
\begin{align*}
y - P(a,x) & = 0, & y - Q(a,x) & = 0 \\
y - I_x(a,b) & = 0, & y - I_x^U(a,b) & = 0.
\end{align*}
for given $y, a$ and (in the case of the beta functions) $b$. The initial guesses are choose by inverting the first term of the relative expansions.
\end{frame}

\section{Bessel functions}

\begin{frame}[fragile]{Bessel functions}
Currently, Bessel functions in Octave (and also in \matlab\!\!) are implemented by the Amos library. I was not able to find a better alternative, nor to make it by myself.\\
The main problem was that the Bessel functions returned \verb|NaN| if the argument was too big (approximatively when bigger than \verb|1e10|) in magnitude. But this is not a problem of numerical instability: the library itself stop if the argument is too big. Thus the best way I found to solve the problem was to ``unlock'' the library (making some tests, seems that \matlab did the same).
\end{frame}

\section{Resume}
\begin{frame}[fragile]{Quick resume}
\begin{itemize}
\item Improved accuracy for \verb|expint|;
\item Added two options, extended the domain and improved accuracy in \verb|gammainc|;
\item Added one option, and improved accuracy in \verb|betainc|;
\item Added the functions \verb|gammaincinv|, \verb|sinint|, and \verb|cosint|;
\item Improved accuracy in \verb|betaincinv|;
\item Extended the domain for \verb|bessel*|.
\end{itemize}
\end{frame}

\section*{}

\begin{frame}
\begin{center}
\huge Thank you for your kind attention.
\end{center}
\end{frame}

\end{document}
