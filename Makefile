octave = octave-specfun

all: main_thesis_MG.pdf

main_thesis_MG.pdf: manuscript/main_thesis_MG.tex
	pdflatex manuscript/main_thesis_MG.tex

manuscript/main_thesis_MG.tex: manuscript/continued_fractions.tex manuscript/gamma.tex manuscript/biblio_thesis.bib manuscript/intro.tex manuscript/beta.tex manuscript/hypergeom.tex manuscript/expint.tex manuscript/bessel.tex manuscript/conclusion.tex
	touch manuscript/main_thesis_MG.tex

manuscript/continued_fractions.tex: manuscript/lentz_algorithm.tex
	touch manuscript/continued_fractions.tex

manuscript/gamma.tex: plots/plot_gamma.eps manuscript/gammaincscheme.tex plots/plot_err_gammainc.eps
	touch manuscript/gamma.tex

plots/plot_gamma.eps: codes/plot_gamma.m
	$(octave) codes/plot_gamma.m
	mv plot_gamma.eps plots/plot_gamma.eps

plots/plot_err_gammainc.eps: codes/test_gammainc.m
	$(octave) codes/test_gammainc.m
	mv plot_err_gammainc.eps plots/plot_err_gammainc.eps

manuscript/beta.tex: plots/plot_beta.eps plots/plot_err_betainc.eps
	touch manuscript/beta.tex

plots/plot_beta.eps: codes/plot_beta.m
	$(octave) codes/plot_beta.m
	mv plot_beta.eps plots/plot_beta.eps
	mv plot_beta_log.eps plots/plot_beta_log.eps

plots/plot_err_betainc.eps: codes/test_betainc.m
	$(octave) codes/test_betainc.m
	mv plot_err_betainc.eps plots/plot_err_betainc.eps

manuscript/expint.tex: plots/plot_err_sinint.eps manuscript/expintscheme.tex plots/plot_err_expint.eps
	touch manuscript/expint.tex

plots/plot_err_sinint.eps: codes/test_trigint.m
	$(octave) codes/test_trigint.m
	mv plot_err_sinint.eps plots/plot_err_sinint.eps
	mv plot_err_cosint.eps plots/plot_err_cosint.eps

plots/plot_err_expint.eps: codes/test_expint.m
	$(octave) codes/test_expint.m
	mv plot_err_expint.eps plots/plot_err_expint.eps

manuscript/bessel.tex: manuscript/besselIscheme.tex manuscript/besselKscheme.tex
	touch manuscript/bessel.tex

codes/test_expint.m: codes/expint_ground_truth.mat
	touch codes/text_expint.m

codes/test_trigint.m: codes/trigint_ground_truth.mat
	touch codes/test_trigint.m

thesis:
	pdflatex manuscript/main_thesis_MG.tex
	bibtex main_thesis_MG.aux
	pdflatex main_thesis_MG-frn.tex
	pdflatex manuscript/main_thesis_MG.tex
	pdflatex manuscript/main_thesis_MG.tex

presentation:
	pdflatex slides/slides.tex
	pdflatex slides/slides.tex
	pdflatex OctConf/slides_octconf.tex
	pdflatex OctConf/slides_octconf.tex

clean:
	rm -rf *.pdf *.log *.out *.aux *.bbl *.bgl *.gz *.eps *.dvi *.toc *.blg *.tex *.nav *.snm *.vrb *.xwm octave-workspace
	rm -rf codes/*.pdf codes/*.log codes/*.out codes/*.aux codes/*.bbl codes/*.bgl codes/*.gz codes/*.eps codes/*.dvi codes/*.toc codes/*.blg codes/*.tex codes/*.nav codes/*.snm codes/*.vrb codes/*.xwm codes/octave-workspace plots/*
