# README #

Michele Ginesi's Master Thesis in Mathematics at the University of Verona.
Supervisor: Marco Caliari

### Short description of the thesis ###

In this thesis we study and implement various numerical expansions for special
functions approximations, in particular for the following functions:

* Incomplete Gamma function and its inverse;
* Incomplete Beta function and its inverse;
* Exponential integral, Sine integral and Cosine integral;
* Bessel functions.

We implemented and improved these function for GNU Octave for the project
"Make Specfuns Special Again", during the Google Summer of Code program,
promoted by Google, in 2017.

Blog of the project: http://gsocspecfun.blogspot.it/

### How to build the thesis ###

It is possible to build the thesis with the help of a Makefile.
To compile the thesis.pfd file on Linux, use the following instructions:

* Open terminal;
* type the command "make" to have the .pdf file without bibliography and
  title page;
* type the command "make full" to have the complete .pdf file, with also the
  slides of the discussion and of my talk in CERN during OctConf2018;
* type the command "make clean" to remove all the non .tex files of the folder.

The make will work ONLY if you have an Octave version updated with my codes,
installed on your computer under the name "octave-specfun". You can find the
repository with these codes at the link
https://bitbucket.org/M_Ginesi/octave
at the bookmark "gsocspecfun2017".

### How to contact ###
To contact me, use the following e-mail address:
michele.ginesi@gmail.com
