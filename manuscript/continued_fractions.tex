In this chapter we will see a brief introduction to continued fractions. We will discuss just the basic concepts we need to understand the rest of this work. For anyone who is interested in more details about the topic, we remand to \cite{CPVWJ08}.
\section{Symbols and notation}
The expression
\[b_0+\cfrac{a_1}{b_1+\cfrac{a_2}{b_2+\cfrac{a_3}{b_3+\raisebox{-0.6\height}{$\ddots$}}}}\]
is called a \emph{continued fraction}, where $a_m$ and $b_m$ are complex numbers and $a_m\neq 0$ for all $m$. For convenience, other notations are available, for example
\begin{equation}
b_0 + \frac{a_1}{b_1}\cplus\frac{a_2}{b_2}\cplus\frac{a_3}{b_3}\cplus\contdots
\label{eq:def_cf}\end{equation}
and
\[b_0+\K_{m=1}^\infty\br{\frac{a_m}{b_m}},\]
or, for short
\[b_0+\K\br{\frac{a_m}{b_m}}.\]

Correspondingly, the $n$-th approximant $f_n$ of the continued fraction is expressed by
\[f_n = b_0+\cfrac{a_1}{b_1+\cfrac{a_2}{b_2+\cfrac{a_3}
{b_3+\raisebox{-0.6\height}{$\ddots$} \raisebox{-1.0\height}{$\,+\cfrac{a_n}{b_n}$}}}},\]
\[f_n = b_0 + \frac{a_1}{b_1}\cplus\frac{a_2}{b_2}\cplus\frac{a_3}{b_3}\cplus\contdots \cplus\frac{a_n}{b_n},\]
and
\[f_n = b_0 + \K_{m=1}^n \br{\frac{a_m}{b_m}}.\]

The continued fraction is more than just the sequence of approximants $\{f_n\}$ or the limit of this sequence, if it exists. In fact, the continued fraction is the mapping of the ordered pair of sequences $(\{a_m\},\{b_m\})$ onto the sequence $\{f_n\}.$

\begin{mydef}[Continued fraction] An ordered pair of sequences
\[(\{a_m\}_{m\in\NN},\{b_m\}_{m\in\NN_0})\]
of complex numbers, with $a_m\neq0$ for $m\geq1$ gives rise to sequences $\{s_n(w)\}_{n\in \NN_0}$ and $\{S_n(w)\}_{n\in \NN_0}$ of \emph{linear fractional transformations}
\[s_0(w)=b_0+w,\quad s_n(w)=\frac{a_n}{b_n+w},\quad n=1,2,3,\ldots,\]
\[S_0(w)=s_0(w),\quad S_n(w)=S_{n-1}(s_n(w)),\quad n=1,2,3,\ldots,\]
and to a sequence $\{f_n\}$ given by
\begin{align*}
f_n & = S_n(0)\in\hat\CC, & n=0,1,2,\ldots.
\end{align*}
where $\hat\CC$ denotes the set $\hat\CC=\CC\cup\{\infty\}$.\\

The ordered pair
\[((\{a_m\},\{b_m\}),\{f_n\})\]
is the \emph{continued fraction}. The numbers $a_m$ and $b_m$ are called respectively \emph{$m$-th partial numerator} and \emph{$m$-th partial denominator} of the continued fraction. the value $f_n$ is called the \emph{$n$-th approximant}.
\end{mydef}
The linear fractional transformation $S_n(w)$ can be expressed as
\[S_n(w)=b_0+\cfrac{a_1}
{b_1+\cfrac{a_2}
{b_2+\cfrac{a_3}
{b_3 + \raisebox{-0.6\height}{$\ddots$} \raisebox{-1.0\height}{$\,+\cfrac{a_n}{b_n+w}$} }}},\]
or, more conveniently as
\[S_n(w) = b_0+\frac{a_1}{b_1}\cplus\frac{a_2}{b_2}\cplus\frac{a_3}{b_3}\cplus\contdots\cplus\frac{a_{n-1}}{b_{n-1}}\cplus\frac{a_n}{b_n+w}.\]
Equivalently,
\[S_n(w) = s_o\circ s_1\circ s_2\circ\cdots\circ s_n(w),\]
where $\circ$ denotes the composition.
\begin{mydef}[Modified approximant]
For a given sequence $\{w_n\}_{n\in\NN_0}$, the number $S_n(w_n) \in\hat\CC$ is called the \emph{$n$-th modified approximant}.
\end{mydef}
\begin{mydef}[Convergence] A continued fraction $b_0+\sK(a_m/b_m)$ is said to \emph{converge} if and only if the sequence of approximants $\{f_n\}=\{S_n(0)\}$ converges to a limit $f\in\hat\CC$. In this case $f$ is called the \emph{value} of the continued fraction. Note that convergence to $\infty$ is accepted.
\end{mydef}
Finally, let us introduce the following concepts
\begin{mydef}[Numerator and denominator]
The \emph{$n$-th numerator} $A_n$ and the \emph{$n$-th denominator} $B_n$ of a continued fraction $b_0+\sK (a_m/b_m)$ are defined by the recurrence relations
\begin{align}
A_n &= b_n A_{n-1} + a_n A_{n-2}\\
B_n &= b_n B_{n-1} + a_n B_{n-2}
\label{eq:recurrence_cf}\end{align}
with initial conditions
\[A_{-1}=1,\quad B_{-1} = 0,\quad  A_0=b_0,\quad B_0=1.\]
\end{mydef}
The modified approximant $S_n(w_n)$ can be written as
\begin{align*}
S_n(w_n) & = \frac{A_n + a_{n-1} w_n}{B_n + B_{n-1}w_n}, & n=0,1,2,\ldots.
\end{align*}
and hence for the $n$-th approxmimant $f_n$ we have
\[f_n = S_n(0) = \frac{A_n}{B_n},\quad f_{n-1} = S_n(\infty) = \frac{A_{n-1}}{B_{n-1}}.\]

\section{Continued fraction representation of functions}
To represent functions of a complex variable $z$ in continued fraction form, we need to have continued fractions with elements $a_m(z)$ and $b_m(z)$ that depend on $z$. Here we briefly discuss the most important families of continued fractions.
\begin{mydef}[C-fractions]
A continued fraction of the form 
\begin{align*}
b_0+\K_{m=1}^\infty \br{\frac{a_mz^{\alpha_m}}{1}}, && a_m\in\CC\setminus\{0\},\quad \alpha_m\in\NN 
\end{align*}
is called \emph{C-fraction}. If $\alpha_m=1$ for $m\geq1$, then it is called a \emph{regular C-fraction}.
\end{mydef}
\begin{mydef}[S-fractions]
A continued fraction of the form
\begin{align}
F(z) & = \K_{m=1}^\infty \br{\frac{a_m z}{1}}, & a_m>0
\label{eq:def_C-frac}
\end{align}
is called a Stieltjes fraction or \emph{S-fraction} and any continued fraction that is equivalent to \eqref{eq:def_C-frac} is also called an S-fraction.
\end{mydef}
For example,
\begin{align*}
E(z) & = \K_{m=1}^\infty \br{\frac{z}{b_m}}, & b_m>0
\end{align*}
is an S-fraction since with $a_1=1/b_1$ and $a_m=1/(b_{m-1}b_m)$ for $m>1$, one finds $F(z)\equiv E(z)$.
\begin{mydef}[Associated continued fractions]
A continued fraction of the form
\begin{align*}
\frac{\alpha_1 z}{1+\beta_1 z}\cplus \K_{m=2}^\infty \br{\frac{-\alpha_m z^2}{1+\beta_m z}},&& \alpha_m\in\CC\setminus\{0\},\quad \beta_m\in\CC
\end{align*}
is called an \emph{associated continued fraction}.
\end{mydef}
\begin{mydef}[P-fractions]
Continued fractions of the form 
\[b_0(z)+\K_{m=1}^\infty \br{\frac{1}{b_m(z)}}\]
where $b_m(z)$ is a polynomial in $1/z$:
\begin{align*}
b_m(z) & = \sum_{k=-N_m}^0 c_k^{(m)}z^k, & c_{-N_m}^{(m)}\neq0,\quad N_0\geq0,\quad N_m\geq1,\quad m\in\NN,
\end{align*}
are called \emph{P-fractions}.
\end{mydef}
\begin{mydef}[J-fractions]
Continued fractions of the form 
\begin{align*}
\frac{\alpha_1}{\beta_1+z}\cplus\K_{m=2}^\infty \br{\frac{-\alpha_m}{\beta_m+z}}, && \alpha_m\in\CC\setminus\{0\},\,\beta_m\in\CC,
\end{align*}
are called \emph{J-fractions}. A J-fraction is said to be \emph{real} if $\alpha_m>0$ and $\beta_m\in\RR$.
\end{mydef}
The even contraction (see Definition~\ref{def:even_odd_CF}) of a modified regular C-fraction
\[\frac{a_1}{z}\cplus \frac{a_2}{1}\cplus\frac{a_3}{z}\cplus\frac{a_4}{1}\cplus\contdots\]
is a J-fraction, but the converse does not always hold.
\begin{mydef}[T-fractions]
Continued fractions of the form 
\begin{align}
\K_{m=1}^\infty \br{\frac{F_m z}{1+G_m z}}, && F_m\in\CC\setminus\{0\},\quad G_m\in\CC
\label{eq:def_T-frac}
\end{align}
are called \emph{Thron fractions} of \emph{general T-fractions}. Equivalent forms of general T-fractions are
\begin{align*}
\K_{m=1}^\infty \br{\frac{c_mz}{e_m+d_m z}}, && c_m,e_m\in\CC\setminus\{0\},\quad d_m\in\CC
\end{align*}
and
\begin{align*}
\frac{\lambda_1}{\frac{1}{\beta_0 z}+\beta_1}\cplus\frac{\lambda_2}{\frac{z}{\beta_1}+\beta_2}\cplus \frac{\lambda_3}{\frac{1}{\beta_2 z}+\beta_3}\cplus \frac{\lambda_4}{\frac{z}{\beta_3}+\beta_4}\cplus\contdots, && \lambda_m,\beta_m\in\CC\setminus\{0\}.
\end{align*}
If all $F_m=1$ in \eqref{eq:def_T-frac} then it is called a \emph{T-fraction}, without further specification.
\end{mydef}
\begin{mydef}[M-fractions]
When $F_1z$ in \eqref{eq:def_T-frac} is replaced by $F_1$ we obtain the continued fraction
\begin{align*}
\frac{F_1}{1+G_1z}\cplus\K_{m=2}^\infty\br{\frac{F_mz}{1+G_mz}}, && F_m\in\CC\setminus\{0\},\,G_m\in\CC,
\end{align*}
which is called an \emph{M-fraction}
\end{mydef}
\begin{mydef}[PC-fractions]
Perron-Carath\`eodory or \emph{PC-fractions} are of the form
\begin{multline*}
\beta_0+\frac{\alpha_1}{\beta_1}\cplus \frac{1}{\beta_2 z}\cplus\frac{\alpha_3}{\beta_3}\cplus \frac{1}{\beta_4 z}\cplus \frac{\alpha_5}{\beta_5}\cplus \frac{1}{\beta_6 z}\cplus\contdots ,\\
\alpha_{2m+1},\beta_m\in\CC,\quad \alpha_{2m+1}=1-\beta_{2m}\beta_{2m+1}\neq0.
\end{multline*}
\end{mydef}
\begin{mydef}[Thiele-fractions]
A \emph{Thiele interpolating continued fraction} is of the form
\begin{align*}
b_0+\K_{m=1}^\infty \br{\frac{z-z_{m-1}}{b_m}},&& b_m\in\CC,\quad z_m\in\CC.
\end{align*}
\end{mydef}
\section{The Euler connection}\label{sec:Euler_connection}
We now see an important result, known as \emph{Euler connection}, which relates continued fractions and series.

\begin{thm}[Euler connection]
Let $\{c_k\}_k$ be a sequence in $\CC\setminus\{0\}$, and
\begin{align*}
f_n & = \sum_{k=0}^n c_k, & n = 0,1,2,\ldots.
\end{align*}
Then
\begin{equation}
f_n = c_0 + \frac{c_1}{1}\cplus\frac{-\nicefrac{c_2}{c_1}}{1+\nicefrac{c_2}{c_1}}\cplus\contdots\cplus\frac{-\nicefrac{c_m}{c_{m-1}}}{1 + \nicefrac{c_m}{c_{m-1}}}\cplus\contdots .
\label{eq:euler_connection}
\end{equation}
\end{thm}
In particular, if we consider the sum
\[\sum_{k=0}^n \br{\prod_{i=0}^k c_i} = c_0 + c_0c_1 + c_0c_1c_2+\ldots+c_0c_1c_2\cdots c_{n-1}c_n,\]
it can be rewritten as
\[\frac{c_0}{1} \cplus \frac{-c_1}{1+c_1}\cplus\frac{-c_2}{1+c_2}\cplus\contdots\cplus \frac{-c_{n-1}}{1+c_{n-1}}\cplus\frac{-c_n}{1+c_n} .\]

\section{Convergence criteria}
Since a continued fraction is a non-terminating expression, it is important to know if converges and at which rate. For a continued fraction with elements $a_m(z)$ and $b_m(z)$ which are functions of $x$ it is also important to know where in the complex plane it converges.\\
Here we will see some classical result about convergence of continued fractions.
\begin{thm}[Worpitzky]
Let $|a_m|\leq\nicefrac{1}{4}$ for all $m\in\NN$. Then the continued fraction
\[\K\br{\frac{a_m}{1}} = \frac{a_1}{1}\cplus\frac{a_2}{1}\cplus\contdots\]
converges, all approximants $f_n$ are in the disc $|w|<\nicefrac{1}{2}$ and the value $f$ is in the disc $|w|\leq\nicefrac{1}{2}$.
\end{thm}
\begin{thm}[\'Slezy\'nski-Pringsheim]
The continued fraction
\[\K_{m=1}^\infty \br{\frac{a_m}{b_m}}=\frac{a_1}{b_1}\cplus\frac{a_2}{b_2}\cplus\frac{a_3}{b_3}\cplus\contdots\]
converges if 
\begin{align*} 
|b_m| &\geq|a_m|+1, & m\geq1.
\end{align*}
Under the same condition the property $|f_n| < 1$ holds for all approximants $f_n$ and $|f|\leq 1$ for the value of the continued fraction.
\end{thm}
\begin{thm}[Van Vleck]
Let $0 < \epsilon < \pi/2$ and let $b_m$ satisfy
\begin{align*}
-\frac{\pi}{2}\epsilon < \arg ({b_m}) < \frac{\pi}{2} - \epsilon, && m\geq1.
\end{align*}
Then all approximants $f_n$ of the continued fraction
\[\K_{m=1}^\infty \br{\frac{1}{b_m}} = \frac{1}{b_1}\cplus\frac{1}{b_2}\cplus\contdots\]
are finite and in the angular domain
\[-\frac{\pi}{2}\epsilon < \arg ({f_n}) < \frac{\pi}{2} - \epsilon.\]
The sequences $\{f_{2n}\}$ and $\{f_{2n+1}\}$ converge to finite values. The continued fraction $\sK (1/b_m)$ converges if and only if, in addition,
\[\sum_{m=1}^\infty |b_m|=\infty.\]
When convergent, the value $f$ is finite and satisfies $|\arg (f)|<\pi/2$.
\end{thm}
Many of the important functions used in applications are S-fractions.
\begin{thm}
An S-fraction $\sK (a_m z / 1) $ corresponding at $z=0$ to $L(z)=\sum_{k=1}^\infty c_k z^k$ is convergent in $\{ z\in \CC:|\arg(z)|<\pi \}$ if one of the following conditions holds:
\begin{enumerate}
\item The sequence $a_m$ is bounded:
\begin{align*}
 a_m & \leq M, & m=1,2,3,\ldots.
 \end{align*}
\item The series of the reciprocal of the square roots diverges:
\[\sum_{m=1}^\infty \frac{1}{\sqrt{a_m}} = \infty.\]
\item \emph{Carleman criterion}:
\[ \sum_{k=1}^\infty \frac{1}{|c_k|^{\frac{1}{2k}}} = \infty.\]
\end{enumerate}
If the S-fraction $\sK (a_m z /1)$ is convergent, then it converges to a finite value.
\end{thm}
\section{Continued fractions evaluation}
We will avoid talking about the problem related to machine arithmetic, instead, we will focus on the algorithms.\\
In many applications of continued fractions $\sK(a_m / b_m )$ the elementes $a_m$ and $b_m$ are given and one must evaluate the $n$-th approximant
\begin{align*}
f_n(0) & = \frac{a_1}{b_1}\cplus\frac{a_2}{b_2}\cplus\frac{a_3}{b_3}\cplus\contdots\cplus\frac{a_n}{b_n},& a_m\in\CC\setminus\{0\},\,b_m\in\CC.
\end{align*}
Based on the fact that the $n$-th approximant $f_n$ equals the first unknown $x_1$ of the linear system
\begin{equation}
\begin{bmatrix}
b_1    & -1  & 0        & \cdots & \cdots & \cdots & 0\\
a_2    & b_2 & -1       & 0      & \cdots & \cdots & 0\\
0      & a_3 & b_3      & -1     & 0      & \cdots & 0\\
\vdots &\ddots     & \ddots   &\ddots  &\ddots  & \ddots & \vdots \\
\vdots &     &   \ddots  &\ddots  & \ddots & \ddots &0 \\
\vdots &     &          &\ddots   & a_{n-1}& b_{n-1}&-1  \\
  0     &\cdots     &\cdots   &\cdots  &   0    &  a_n   & b_n 
\end{bmatrix}
\begin{bmatrix}
x_1\\x_2\\x_3\\\vdots\\\vdots\\x_{n-1}\\x_n
\end{bmatrix}
 = 
\begin{bmatrix}
a_1\\0\\0\\\vdots\\\vdots\\0\\0
\end{bmatrix}.
\label{eq:linear_system_cf}
\end{equation}
Several algorithms can be devised for the computation of $f_n$. We revise some of them.
\subsection{Euler-Minding summation}
Reducing the tridiagonal matrix in \eqref{eq:linear_system_cf} to an upper triangular form by Gaussian elimination, is equivalent to using the series representation of $f_n$
\begin{align*}
f_n & = \sum_{k=0}^n c_k, & n=0,1,2,\ldots,
\end{align*}
with 
\[c_0 = 0,\quad c_k=\dfrac{(-1)^{k-1}\prod_{j=1}^k a_j}{B_kB_{k-1}},\quad k=1,2,3,\ldots.\]
Thus, we end up with
\begin{align*}
h_1 &= b_1,\\
h_k &= b_k + \frac{a_k}{h_{k-1}},& k\geq 2,\\
f_1 &= \frac{a_1}{b_1},\\
f_n &= \sum_{k=1} ^ n (-1)^{k-1} \frac{a_1\cdots a_k}{h_1^2\cdots h_{k-1}^2 h_k},& n>1.
\end{align*}
\subsection{Backward recurrence}
Reducing the tridiagonal matrix in \eqref{eq:linear_system_cf} to a lower triangular form by Gaussian elimination, leads to a very efficient algorithm to compute a single approximant:
\begin{align*}
F_{n+1}^{(n)} &= 0,\\
F_{k}^{(n)} &= \frac{a_k}{b_k + F_{k+1}^{(n)}},& k=n,n-1,n-2,\ldots, 1,\\
f_n &= F_1^{(n)}.
\end{align*}
\subsection{Product form}
Introducing the related linear system
\begin{equation}
\begin{bmatrix}
b_2    & -1  & 0        & \cdots & \cdots & \cdots & 0\\
a_3    & b_3 & -1       & 0      & \cdots & \cdots & 0\\
0      & a_4 & b_4      & -1     & 0      & \cdots & 0\\
\vdots &\ddots     & \ddots   &\ddots  &\ddots  & \ddots & \vdots \\
\vdots &     &   \ddots  &\ddots  & \ddots & \ddots &0 \\
\vdots &     &          &\ddots   & a_{n-1}& b_{n-1}&-1  \\
  0     &\cdots     &\cdots   &\cdots  &   0    &  a_n   & b_n 
\end{bmatrix}
\begin{bmatrix}
y_1\\y_2\\y_3\\\vdots\\\vdots\\y_{n-1}\\y_n
\end{bmatrix}
 = 
\begin{bmatrix}
a_2\\0\\0\\\vdots\\\vdots\\0\\0
\end{bmatrix},
\label{eq:linear_modified}\end{equation}
allows to write $f_n = x_n/y_{n-1}.$ Reducing the coefficient matrices in both \eqref{eq:linear_system_cf} and \eqref{eq:linear_modified} to upper triangular form leads to
\begin{align*}
g_1 &= a_1,\quad g_2 = b_2,\\
g_k &= b_k + a_k/g_{k-1},& k> 2,\\
h_1 &= b_1,\\
h_k &= b_k + a_k/h_{k-1},& k>1,\\
f_n &= \prod_{k=1}^n \frac{g_k}{h_k}.
\end{align*}
\subsection{Forward recurrence}
Let us consider the $n$-th numerator and $n$-th denominator, $A_n$ and $B_n$, of $\sK(a_m / b_m)$. Then by the recurrence relation \eqref{eq:recurrence_cf} one computes $a_1,B_1,A_2,B_2,\ldots,A_n,B_n$ and
\[f_n = \frac{A_n}{B_n}.\]
In practice, this algorithm has some unattractive features: the recurrence \eqref{eq:recurrence_cf} frequently generates very large or very small values for the numerators and denominators $A_j$ and $B_j$. There is thus danger of overflow or underflow of the floating point representation. However, the recurrence \eqref{eq:recurrence_cf} is linear in the $A$'s and $B$'s. At any point one can rescale the currently saved two levels of the recurrence, e.g., divides $A_j$, $B_j$, $A_{j-1}$ and $B_{j-1}$ all by $B_j$. This incidentally makes $A_n=f_n$ and is convenient for testing whether we have gone far enough: if $f_n$ and $f_{n-1}$ are closer than a given tolerance, then we can stop the algorithm (if $B_j$ happens to be zero for some $j$'s, just skip the normalization for this cycle).\\

Two newer alghoritms have been proposed for evaluating continued fractions.\\
\emph{Steed's method} does not use $A_j$ and $B_j$ explicitly, but only the ratio $D_j = B_{j-1}/B_j$. One calculates $D_j$ and $\Delta f_j=f_j-f_{j-1}$ recursively using
\begin{equation}
D_j = \frac{1}{b_j + a_j D_{j-1}},
\label{eq:D_j_val}
\end{equation}
\[\Delta f_j = \br{b_j D_j -1}\Delta f_{j-1}.\]
Steed's method avoids the need for rescaling of intermediate results. However, for certain continued fraction, the denominator in \eqref{eq:D_j_val} approaches zero, so that $D_j$ and $\Delta f_j$ are very large. The next $\Delta f_{j+1}$ will typically cancel this large change, but with loss of accuracy in the numerical running sum of the $f_j$'s. Steed's method is recommended only for cases where is known in advantage that no denominator can vanish.\\

The best general method for evaluating continued fractions seems to be the \emph{modified Lentz's method} (which is the one used in the implementation of this thesis). The need for rescaling intermediate results is avoided using both the ratios
\[C_j=\frac{A_j}{A_{j-1}},\quad D_j=\frac{B_{j-1}}{B_j},\]
and calculating $f_j$ by
\[f_j = f_{j-1}C_j D_j\]
From \eqref{eq:recurrence_cf}, it is easy to show that the ratios satisfies the recurrence relations
\begin{equation}
D_j = \frac{1}{b_j+a_j D_{j-1}} , \quad C_j=b_j+\frac{a_j}{C_{j-1}}.
\label{eq:CD_mod_lentz}\end{equation}
In this algorithm there is the danger that the denominator in the expression for $D_j$, or the quantity $C_j$ itself, might approach zero. Either of these conditions invalidates \eqref{eq:CD_mod_lentz}. However, the Lentz's algorithm can be quickly modified to fix this: just shift the offending term by a small amount, e.g., $10^{-30}$.
\input{manuscript/lentz_algorithm.tex}
In detail, the modified Lentz's algorithm is presented in Algorithm~\ref{alg:lentz}, where $\texttt{eps}$ is the floating point precision, say $10^{-7}$ or $10^{-15}$. The parameter $\texttt{tiny}$ should be less than typical values of $\texttt{eps}|b_j|$, say $10^{-30}$.\\
The above algorithm assumes that it is possible to terminate the evaluation of the continued fraction when $|f_j-f_{j-1}|$ is sufficiently small. This is usually the case, but by no means guaranteed. There is at present no rigorous analysis of error propagation in Lentz's algorithm. However, empirical test suggests that it is at least as good as other methods.
\subsection{Manipulating continued fractions}\label{sec:manipulating_CF}
Several important properties of continued fractions can be used to rewrite them in forms that can speed up numerical computations.
\begin{mydef}[Equivalence transformation]
An \emph{equivalence transformation} is a map
\[a_n\mapsto\lambda a_n,\quad b_n\mapsto\lambda b_n,\]
which leaves the value of a continued fraction unchanged.
\end{mydef}
By a suitable choice of the factor $\lambda$, it is possible often to simplify the form of the $a$'s and the $b$'s. Of course, it is possible to carry out successive equivalence transformations, possibly with different $\lambda$'s, on successive terms of the continued fraction.\\
\begin{mydef}[Even and odd part of a continued fraction]
The \emph{even} and \emph{odd} parts of a continued fraction are continued fractions whose successive convergents are $f_{2n}$ and $f_{2n+1}$, respectively.
\label{def:even_odd_CF}\end{mydef}
Their main use is that they converge twice as fast as the original continued fraction, and so if their term are not much more complicates than the terms in the original expression, there can be a big saving in computation. The formula for the even part of \eqref{eq:def_cf} is
\[d_0 + \frac{c_1}{d_1}\cplus\frac{c_2}{d_2}\cplus\frac{c_3}{d_3}\cplus\contdots\]
where, in terms of intermediate variables
\[ \alpha_1 = \frac{a_1}{b_1},\]
and
\begin{align*}
 \alpha_n & = \frac{a_n}{b_nb_{n-1}}, & n\geq 2,
 \end{align*}
we have
\[d_0 = b_0,\quad c_1=\alpha_1,\quad d_1 = 1+\alpha_2,\]
and
\[c_n = -\alpha_{2n-1}\alpha_{2n-2},\quad d_n = 1+\alpha_{2n-1}+\alpha_{2n},\qquad n\geq 2.\]
