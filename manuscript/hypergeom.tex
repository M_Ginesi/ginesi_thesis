Here we present a brief introduction on \emph{hypergeometric} and \emph{confluent hypergeometric functions}. We will focus only on the basic concepts necessary to understand the rest of this thesis.

\section{Hypergeometric functions}\label{sec:hyperg}
Many of the special functions in mathematics, physics and engineering are hypergeomteric functions, or can be expressed in terms of them. In particular the Gauss hypergeometric function ${}_2F_1$ in fundamental to study the incomplete beta functions (see \S~\ref{sec:betainc}).

\subsection{Definition and basic properties}
A \emph{hypergeometric series} of \emph{function} is a series for which the ratio of successive terms in the series is a rational function of the index of the term.

\begin{mydef}[Hypergeometric series] The \emph{hypergeometric series} is defined by
	\begin{multline}
	{}_p F_q (a_1,\ldots,a_p;b_1,\ldots,b_q;z) = \sum_{k=0}^\infty \frac{(a_1)_k \cdots (a_p)_k} {(b_1)_k\cdots (b_q)_k} \, \frac{z^k}{k!},\\
		a_i \in \CC , \quad b_j \in \CC \setminus \ZZ_0^-, \quad 1 \leq i \leq p,\quad 1 \leq j \leq q,
	\label{eq:def_hypergeom}
	\end{multline}
where
\begin{align*}
(a)_0 = 1,\quad (a)_k = a(a+1)(a+2)\cdots(a+k-1),\quad a \in \CC, && k \in \NN
\end{align*}
is the \emph{Pochhammer symbol}.
\end{mydef}
Assuming that also all $ a_j \in \CC \setminus \ZZ _0 ^ - $, the following holds for the convergence of \eqref{eq:def_hypergeom}:
\begin{itemize}
	\item $ p < q + 1 $: the series converges absolutely for $ z \in \CC $,\\
	\item $ p = q + 1 $: the series converges absolutely for $|z| < 1 $ and diverges for $|z| > 1$, and for $ |z| = 1 $ it converges absolutely for $\Re \br{\sum_{k=1}^q b_k - \sum_{k=1} ^ p a_k } > 0$,
	\item $ p > q + 1 $: the series converges only for $ z = 0 $. 
\end{itemize}

The hypergeometric series \eqref{eq:def_hypergeom} is a solution to the differential equation
\[z\,\frac{d}{dz} \prod _{j=1} ^ q \br {z\,\frac{dy}{dz} + (b_j-1)y} - z \prod_{j=1} ^ p \br {z\, \frac{dy}{dz} + a_j y} = 0. \]
In particular for $p=2$ and $q=1$, this second order ODE reads
\begin{equation}
z(1-z)\,\frac{d^y}{dz^2} + (c-(a+b+1)z) \frac{dy}{dz} - aby = 0
\label{eq:hypergeom_DE}\end{equation}
known as \emph{hypergeometric differential equation}. The solution of \eqref{eq:hypergeom_DE} with initial conditions 
\[ y(0) = 1 \quad \text{and} \quad \frac{dy}{dz} (0) = \frac{ab}{c} \]
is called the \emph{Gauss hypergeometric series} ${}_2F_1(a,b;c;z)$ and is given by
\begin{align}
{}_2 F _1 (a,b;c;z) & = \sum_{k=0}^\infty \frac{(a)_k(b)_k}{(c)_k}\,\frac{z^k}{k!},& a,b\in\CC,\quad c\in\CC\setminus \ZZ_0^-.
\label{eq:hyperg21}
\end{align}
The series \eqref{eq:hyperg21} converges for $|z|<1$ and diverges for $|z|>1$. In case of convergence we use the term \emph{Gauss hypergeometric function}. It is an analytical function of $a,b,c$ and $z$. It also satisfies various symmetry relations and transformation formulae.
\begin{prop}[Relations for Gauss hypergeometric function]
The Gauss hypergeometric functions satisfies
\begin{subequations}
\label{eq:hypergeom_relations}
\begin{align}
{}_2F_1(a,b;c;z) & = {}_2F_1(b,a;c;z),\label{eq:hypergeom_symm}\\
{}_2F_1(a,b;c;z) & = (1-z) ^ {-a} {}_2F_1\br{a,c-b;c;\frac{z}{z-1}} , \label{eq:hypergeom_recurr1}\\
{}_2F_1(a,b;c;z) & = (1-z) ^ {-b} {}_2F_1 \br{c-a,b;c;\frac{z}{z-1}}, \label{eq:hypergeom_recurr2}\\
{}_2F_1(a,b;c;z) & = (1-z) ^ {c-a-b} {}_2F_1 (c-a,c-b;c;z). \label{eq:hypergeom_recurr3}
\end{align}
\end{subequations}
\end{prop}
Via \eqref{eq:hypergeom_relations}, one can obtain the following equality, that is used to obtain a continuous fraction representation,
\begin{equation}
{}_2F_1(a,b;c;z) = {}_2F_1(a,b+1;c+1;z) - 	\frac{a(c-b)}{c(c+1)} \, {}_2F_1(a+1,b+1;c+2;z).
\label{eq:hyper_cont_rel}
\end{equation}
or the basic form
\begin{subequations}
\begin{multline}
{}_2F_1(a,b;c+1;z) =-\frac{c(c-1-(2c-a-b-1)z)}{(c-a)(c-b)z} \, {}_2F_1(a,b;c;z)\\
		-\frac{c(c-1)(z-1)}{(c-a)(c-b)z} \, {}_2F_1(a,b;c-1;z).
\label{eq:hypergeom_recurr4}
\end{multline}
By means of \eqref{eq:hypergeom_relations}, the following recurrence relations can be obtained from \eqref{eq:hypergeom_recurr4}:
\begin{multline}
{}_2F_1(a,b+1;c+1;z) = \frac{c(c-1+(b-a)z)}{b(c-a)z} \, {}_2F_1(a,b;c;z)\\
	- \frac{c(c-1)}{b(c-a)z} \, {}_2F_1(a,b-1;c-1;z),
\label{eq:hypergeom_recurr5}
\end{multline}
\begin{multline}
{}_2F_1(a+1,b+1;c+1;z) = \frac{c(-c+1+(a+b+1)z)}{abz(1-z)} \, {}_2F_1(a,b;c;z)\\
	+\frac{c(c-1)}{abz(1-z)} \, {}_2F_1(a-1,b-1;c-1;z).
\label{eq:hypergeom_recurr6}
\end{multline}
\end{subequations}

\subsection{Continued fractions representation}
We will now give some continued fraction representation for ratios of hypergeometric functions of the form
\[ \frac{{}_2F_1(a,b;c;z)}{{}_2F_1(a,b+1;c+1;z)}. \]
\paragraph{C-fraction.}
Continued fraction representations for other ratios can be obtained by applying the transformation formulae \eqref{eq:hypergeom_relations}.\\
From \eqref{eq:hyper_cont_rel} is possible to obtain the regular C-fraction
\begin{subequations}
\begin{multline}
\frac{{}_2F_1(a,b;c;z)}{{}_2F_1(a,b+1;c+1;z)} = 1 + \K_{m=1}^\infty \br{\frac{a_m z}{1}},\\
z\in \CC\setminus [1,+\infty), \quad a,b\in\CC, c\in \CC\setminus \ZZ_0^-,
\label{eq:hyperg_C_frac}
\end{multline}
where the coefficients $ a_m $ are given by
\begin{align}
a_{2j + 1} & = \frac{-(a+j)(c-b+j)}{(c+2_j)(c+2j+1)}, & j\in\NN_0,\\
a_{2j} & = \frac{-(b+j)(c-a+j)}{(c+2j-1)(c+2j)}, & j\in\NN 
\end{align}
\end{subequations}
The continued fraction \eqref{eq:hyperg_C_frac} is called the \emph{Gauss continued fraction}. From \eqref{eq:hyper_cont_rel} and \eqref{eq:hyperg_C_frac} we obtain the C-fraction representation
\begin{subequations}\label{eq:hyperg_C_frac_2}
\begin{align}
z\, {}_2F_1(a,1;c;z) & = \K_{m=1}^\infty \br{\frac{c_m z}{1}},& z\in \CC\setminus [1,\infty),\quad a \in \CC,\quad c \in \CC\setminus \ZZ_0^-,
\end{align}
where the coefficients $c_m$ are given by
\begin{align}
c_1 & = 1,\\
c_{2j + 2} & = \frac{-(a+j)(c+j)}{(c+2j)(c+2j+1)}, & j\in\NN_0,\\
c_{2j + 1} & = \frac{-j(c-a+j)}{(c+2j-1)(c+2j)}, & j\in\NN. 
\end{align}
\end{subequations}

\paragraph{T-fraction.}
For the ratio of hypergeometric series in \eqref{eq:hyperg_C_frac} a T-fraction representation, already found by Euler, can be obtained from recurrence relation \eqref{eq:hypergeom_recurr5}:
\begin{subequations}\label{eqs:hyperg_T_frac}
\begin{multline}
\frac{{}_2F_1(a,b;c;z)}{{}_2F_1(a,b+1;c+1;z)} = \frac{c+(b-a+1)z}{c} + \frac{1}{c} \K_{m=1}^\infty \br{\frac{c_mz}{e_m+d_mz}},\\
|z|<1,\quad a,b\in\CC ,\quad c\in\CC\setminus \ZZ_0^-,
\end{multline}
\begin{multline}
\frac{(b-a+1)z}{c} \frac{{}_2F_1(b-c+1,b;b-a+1;\nicefrac{1}{z})}{{}_2F_1(b-c+1,b+1;b-a+2;\nicefrac{1}{z})}=\\
	\frac{c+(b-a+1)z}{c}+\frac{1}{c} \K_{m=1}^\infty \br{ \frac{c_mz}{e_m + d_mz} },\\
	|z|>1,\quad b-a\neq -2, -3,\ldots,\quad c\neq 0,
\end{multline}
where the coefficients are given by
\begin{align}
c_j = -(c-a+j)(b+j),\quad e_j = c + j,\quad d_j = b-a+j+1, && j\geq 1.
\end{align}
\end{subequations}
For $b=0$ in \eqref{eqs:hyperg_T_frac} we find the M-fraction representation
\begin{subequations}\label{eqs:hyperg_M_frac}
\begin{multline}
{}_2F_1(a,1;c+1;z) = \frac{c}{c+(1-a)z} \cplus \K_{m=1}^\infty \br{\frac{c_mz}{e_m+d_mz}},\\
	|z|<1,\quad a\in\CC,\quad c\in \CC\setminus \ZZ_0^-,
\end{multline}
\begin{multline}
\frac{cz^{-1}}{(1-a)}\,{}_2F_1 (1-c, 1; 2-a;\nicefrac{1}{z}) = \frac{c}{c+(1-a)z}\cplus\K_{m=1}^\infty \br{\frac{c_m z}{e_m + d_mz}},\\
	|z|>1,\quad a\neq 2,3,\ldots,\quad c\in\CC,
\end{multline}
where the coefficients are given by
\begin{align}
c_j = -j(c-a+j),\quad e_j = c+j,\quad d_j = j+1-a, && j\geq 1.
\end{align}
\end{subequations}
%%%%%%%%%%%%%%%
%  CONFLUENT  %
%%%%%%%%%%%%%%%
\section{Confluent hypergeometric function of the second kind}\label{sec:hyperg_confl_second_kind}
The \emph{confluent hypergeometric function} ${}_1F_1(a,b;z)$ can be obtained as the result of a limit process applied to the hypergeomteric function ${}_2F_1(a,b;z)$ introduced in \S~\ref{sec:hyperg}.

\subsection{Definition and elementary property}
The confluent hypergeometric functions can be defined as solution of a second order differential equations.
\begin{mydef}[Confluent hypergeometric functions]
The second order differential equation
\begin{align}\label{eq:confl_hyper_ODE}
z\, \frac{d^2w}{dz^2} + (b-z) \frac{dw}{dz} - aw &= 0, &a\in\CC,\quad b\in\CC\setminus \ZZ_0^-
\end{align}
is called \emph{confluent hypergeometric differential equation} or \emph{Kummer's differential equation}. It has a regular singularity at the origin and an irregular singularity at infinity. Among the solutions are the \emph{Kummer functions} $M(a,b;z)$ and $U(a,b;z)$.\\

The solution $M(a,b;z)$ to \eqref{eq:confl_hyper_ODE} with initial conditions
\[ w(0) = 0,\quad \frac{dw}{dz} (0) = \frac{a}{b}\]
is called \emph{confluent hypergeometric function of the first kind} or \emph{Kummer's confluent hypergeometric function of the first kind}.
\end{mydef}
The Kummer's confluent hypergeometric function of the first kind $M(a,b;z)$ has the following hypergeomteric series representation:
\begin{align}\label{eq:conf_hyperg_first_kind}
M(a,b;z) &= {}_1F_1 (a,b;z) = \sum_{k=0} ^ \infty \frac{(a)_k}{(b)_k} \,\frac{z^k}{k!}, & z\in\CC,\quad a\in\CC,\quad b\in\CC\setminus\ZZ_0^-.
\end{align}
It converges locally uniformly in $\CC$ to an entire function.\\

The confluent hypergeometric function of the first kind \eqref{eq:conf_hyperg_first_kind} can be obtained, in two different ways, by the Gauss hypergeometric series \eqref{eq:hyperg21}. In fact
\begin{subequations}\label{eqs:lim_hyperg}
\begin{align}
\lim_{a\to\infty} {}_2F_1\br{a,b,c;\frac{z}{a}} & = M(b,c;z) = {}_1F_1(b,c;z),\\
\lim_{b\to\infty} {}_2F_1\br{a,b,c;\frac{z}{b}} & = M(a,c;z) = {}_1F_1(a,c;z).
\end{align}
\end{subequations}
Equation \eqref{eq:confl_hyper_ODE} has two linearly independent solutions: ${}_1F_1(a,b;z)$, which is $M(a,b;z)$, and $z^{1-b} {}_1F_1 (a-b+1,2-b;z)$.
\begin{mydef}[Confluent hypergeometric function of the second order]
The function $U(a,b;z)$ is a linear combination of the two solutions to \eqref{eq:confl_hyper_ODE} and is given by
\begin{multline}
U(a,b;z) = \frac{\Gamma (1-b)}{\Gamma(a-b+1)}\,{}_1F_1(a,b;z) + z ^{1-b}\frac{\Gamma (b-1)}{\Gamma (a)}\, {}_1F_1 (a-b+1,2-b;z),\\
z\in\CC,\quad a\in\CC,\quad b\in\CC\setminus\ZZ.
\end{multline}
The function $U(a,b;z)$ is called \emph{confluent hypergeometric function of the second kind} or \emph{Kummer's confluent hypergeometric function of the second kind}.
\end{mydef}
This function satisfies various properties
\begin{prop}[Kummer transformation]
The confluent hypergeometric function of the second kind satisfies
\begin{equation}
U(a,b;z) = z^{1-b} U(a-b+1,2-b;z).
\end{equation}
\end{prop}
\begin{prop}[Recurrence relations]
The confluent hypergeometric function of the second kind satisfies the following recurrence relations.
\begin{subequations}
\begin{align}
U(a+1,b;z) 		& = \frac{2a-b+z}{a(a-b+1)} U(a,b;z) - \frac{1}{a(a-b+1)} U(a-1,b;z),\\
U(a, b+1; z)	& = \frac{b-1+z}{z} U(a,b;z) + \frac{a-b+1}{z} U(a,b-1;z),\\
U(a+1,b+1;z)	& = \frac{b-1+z}{az} U(a,b;z) -\frac{1}{az} U(a-1,b-1;z).
\end{align}
\end{subequations}
\end{prop}
