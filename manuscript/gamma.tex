The \emph{gamma function} $\Gamma(z)$ is probably the most important special function of classical analysis after the so-called elementary functions. It is an extension of the factorial $n!$ to real and complex argument: it is related to the factorial by
\[\Gamma(n) = (n-1)!.\]
In this chapter we will focus, in particular, on the so-called \emph{incomplete gamma functions}. These are foud to be useful in heat conduction, probability theory and in the study of Fourier and Laplace transform \cite{CZ93}.

\section{The gamma function}
The \emph{gamma function} is defined as
\begin{align}
\Gamma(z) & = \int_0^\infty t^{z-1} \ee^{-t}\, dt, & \Re z > 0.
\label{eq:gamma_def}
\end{align}
Since it is intended to be an extension of the factorial to real and complex, we desire the following two properties:
\begin{subequations}\label{eqs:gamma_prop}
\begin{align}\label{eq:gamma_prop_2}
\Gamma(n) & = (n-1)!, &  n\in \NN.
\end{align}
and
\begin{align}\label{eq:gamma_prop_1}
\Gamma(z+1)& =z\Gamma(z), & \Re z > 0.
\end{align}
\end{subequations}
To prove \eqref{eq:gamma_prop_1}, we can just integrate by parts, obtaining
\begin{align*}
\Gamma (z+1)	& = \int_0^\infty t^z \ee^{-t}\, dt \\
				& = \sbr{-t^z\ee^{-t}}_{t=0}^{t=\infty} + \int_0^\infty z\,t^{z-1}\ee^{-t}\diff t\\
				& = z \int_0^\infty t^{z-1}\ee^{-t} \diff t\\
				& = z\,\Gamma(z).
\end{align*}
Again integrating by part, we obtain,
\begin{align*}
\Gamma(1) &= \int_0^\infty t\,\ee^{-t}\diff t\\
&= \sbr{ -t\ee^{-t} }_{t=0}^{t=\infty} + \int_0^\infty \ee^{-t}\diff t\\
&= \int_0^\infty \ee^{-t}\diff t\\
&= 1,
\end{align*}
which is exactly the factorial of zero. So it is easy to see (by induction), that also \eqref{eq:gamma_prop_2} holds true.\\

It follows from the uniform convergence of the integral \eqref{eq:gamma_def} that $\Gamma(z)$ is a continuous function for all $z>0$.
To investigate the behaviour of $\Gamma(z)$ as $z$ approaches the value zero from the right, we use the recurrence formula written in the form
\[\Gamma(z) = \frac{\Gamma(z+1)}{z},\]
obtaining
\[\lim_{z\to 0^+} \frac{\Gamma(z+1)}{z} = +\infty.\]
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\linewidth]{plots/plot_gamma.eps}
\caption{The gamma function.}
\end{figure}

An equivalent definition for the gamma function is given by the so-called \emph{Euler's formula}.
\begin{thm}[Euler's formula]The following relation holds true:
\begin{equation}
\Gamma(z) = \lim_{n\to\infty} \frac{n!\,n^z}{z(z+1)(z+2)\cdots(z+n)}.
\label{eq:euler_formula}
\end{equation}
\end{thm}
\begin{proof}
Let us show that formulations \eqref{eq:gamma_def} and \eqref{eq:euler_formula} are equivalent for positive values of $z$. To do it, set
\begin{align*}
F(z)&=\int_0^\infty \ee^{-t}t^{z-1}\diff t\\
&=\lim_{n\to\infty}\int_0^n \br{1-\frac{t}{n}}^n t^{z-1}\diff t, & z>0,
\end{align*}
where we used the following fact:
\[\ee^{-t} = \lim_{t\to\infty} \br{1-\frac{t}{n}}^n.\]
Using successive integration by parts, after making the change of variable $\t=t/n$, we find
\begin{align*}
F(z) &= \lim_{n\to\infty} n^z \int_0^1 (1-\t)^n \t^{z-1}\diff \t\\
&= \lim_{n\to\infty} n^z \br{\sbr{(1-\t)^n \, \frac{\t^z}{z}}_{\t=0}^{\t=1} + \frac{n}{z}\int_0^1 (1-\t)^{n-1} \t^z\diff \t }\\
&\,\,\,\vdots\\
&= \lim_{n\to\infty} n^z \br{ \frac{n(n-1)\cdots2\cdot 1}{z(z+1)\cdots(z+n-1)} \int_0^1 \t^{z+n-1} \diff \t}\\
&= \lim_{n\to\infty} \frac{n!n^z}{z(z+1)(z+2)\cdots(z+n)},
\end{align*}
as claimed.
\end{proof}

Euler's formula \eqref{eq:euler_formula} permits to extend the definition of the gamma function to all $z\in\CC\setminus\ZZ_0^-$.
From it, it is easy to observe that if $z=-n$ for $n=0,1,2,\ldots$, then $|\Gamma(z)|=\infty$.
$\Gamma(z)$ is single valued and analytic over the entire complex plane, save for the points $z=-n,n=0,1,2,\ldots$ where it possesses simple
poles with residue $(-1)^n/n!$.\\

Another important and useful relation is the so-called \emph{Euler's infinite product rule}.
\begin{thm}[Euler's infinite product rule]The following relation holds true:
\begin{align}
\frac{1}{\Gamma(z)} & = z \, \ee^{\gamma z} \prod_{n=1}^\infty \sbr{\br{1 + \frac{z}{n}} \ee^{\nicefrac{-z}{n}}}, & |z|<\infty,
\label{eq:euler_infinite_prod}
\end{align}
where $\gamma$ is the \emph{Euler's constant}
\begin{equation}
\gamma = \lim_{m\to\infty} \br{ \sum_{k=1} ^ m \frac{1}{k} - \log (m) } =\lim_{m\to\infty}\br{1+\frac{1}{2}+\frac{1}{3}+\frac{1}{4}+\ldots+\frac{1}{m}-\log{m}}
\label{eq:euler_mascheroni_constant}
\end{equation}
known also as \emph{Euler-Mascheroni constant}, which has (approximatively) value $\gamma~=~0.57721566490153\ldots$.
\end{thm}
We remark that property \eqref{eq:gamma_prop_1} can be iterated, obtaining
\begin{align*}
\Gamma (z+1) &= z \, \Gamma(z) \\
				& = z(z-1) \Gamma(z-1) \\
				& = z (z-1) (z-2) \Gamma (z-2)\\
				& \,\,\,\vdots \\
				& = z (z-1) (z-2) \cdots (z - M) \Gamma (z-M), & M \in \NN,
\end{align*}
that is
\begin{equation}\label{eq:prop_gamma_iter}
\Gamma (z+1) = \br{ \prod _{k=0} ^ M (z-k) }\, \Gamma (z - M).
\end{equation}
%% lgamma %%

\section{The logarithm of the gamma function}\label{sec:gammalog}
Since in many applications we are interested in multiplication and division of gamma functions (see for example the beta function in \S~\ref{chp:beta}), and the fact that $\Gamma(z)$ can be big also for small values of $z$, comporting possible oveflows or underfloes if these divisions are computed directly, we are interested in an accurate way to compute the natural logarithm of the gamma function
\[\lgamma(z) = \log(\Gamma(z)).\]
A stable way to compute it is given by \cite{BM04}:
\begin{equation}
\lgamma(z) = -\gamma z - \log(z) + \sum_{n=1}^\infty\br{\frac{z}{n} - \log\br{1+\frac{z}{n}}} .
\label{eq:llgamma}
\end{equation}
Equation \eqref{eq:llgamma} easy follows by taking the logarithm of the following expression, established by Karl Weierstrass:
\[ \Gamma ( z ) = \frac{ \ee ^ {\frac {-\gamma} {z} } } { z } \prod_{n=1} ^ \infty \sbr {\br { 1+\frac{z}{n} } ^ {-1} \ee^{\frac{z}{n}}} , \]
which can be obtained by taking the inverse of the Euler's infinite product rule~\eqref{eq:euler_infinite_prod}.

%% Incomplete gamma function %%
\section{The incomplete gamma function}\label{sec:gammainc}
The term \emph{incomplete gamma function} usually refers to the functions
\begin{subequations}\label{eq:gamma_Gamma_def}
\begin{align}
\gamma(a,x) &= \int_0^x \ee^{-t}t^{a-1}\diff t , & \Re a >0,\\
\Gamma(a,x) &= \int_x^\infty \ee^{-t}t^{a-1}\diff t , & \Re a > 0.
\end{align}
\end{subequations}
Instead, we will consider in this section the \emph{gamma function ratios}
\begin{subequations}\label{eq:inc_gamma_ratios}
\begin{align}
P(a,x) &= \frac{1}{\Gamma(a)}\int_0^x \ee^{-t}t^{a-1}\diff t, & \Re a > 0,\\
Q(a,x) &= \frac{1}{\Gamma(a)}\int_x^\infty \ee^{-t}t^{a-1}\diff t, & \Re a > 0.
\end{align}
\end{subequations}
We will limit ourselves to real values of $x$ and real positive values for $a$.\\

Trivially we have
\begin{equation}
P(a,x) + Q(a,x) = 1.
\label{eq:PQ1}
\end{equation}
We are interested in computing also the so-called ``scaled'' versions:
\begin{align*}
P_S(a,x) & = P(a,x) \frac{\Gamma(a+1) \ee^x}{x^a},\\
Q_S(a,x) & = Q(a,x) \frac{\Gamma(a+1) \ee^x}{x^a},
\end{align*}
which are, obviously, related by
\begin{equation}
P_S(a,x) + Q_S(a,x) = \frac{\Gamma(a+1) \ee^x}{x^a}.
\label{eq:PQs}
\end{equation}
We call $D(a,x)$ the reciprocal of this quantity:
\begin{equation}D(a,x) = \frac{x^a\ee^{-x}}{\Gamma(a+1)},
\label{eq:D_def}
\end{equation}
so that we can write
\begin{subequations}\label{eqs:def_scaled_gammainc}
\begin{equation}  \label{eq:def_scaled_gammainc_low}
P_S (a,x)  = \frac{P(a,x)}{D(a,x)},
\end{equation}
and
\begin{equation} \label{eq:def_scaled_gammainc_upp}
Q_S (a,x)  = \frac{Q(a,x)}{D(a,x)}.
\end{equation}
\end{subequations}
We will see in \S~\ref{sec:D_fun} a stable way to compute $D(a,x)$.

%% Gammainc: numerical evaluation
\subsection{Numerical evaluation}
To compute the incomplete gamma functions we employed different strategies depending on the value of the variables in the $x~-~a$ plane. To obtain such a partition, we tested all the implementations and we tried to recover a simple way to split the plane in an efficient way. A scheme of this partition is shown in Figure~\ref{fig:gammainc_scheme}.
\begin{figure}[hbt]
\centering
\resizebox{0.8\textwidth}{!}{
\input{manuscript/gammaincscheme.tex}}
\caption{Computation diagram for the incomplete gamma functions. The $x~-~a$ semi-plane is divided into various regions. The three green lines show the trivial-to-compute values $x=0$ and $a\in\{0,1\}$, the red lines show the values where we used relation \eqref{eq:gamma_a_N} for integer values for $a$, while the yellow and blue parts of the plane show, respectively, where we employed the series expansion and the continued fractions. Finally, the orange and violet parts symbolize the area where we use the recurrence relations \eqref{eqs:recurrences_gammainc}.}
\label{fig:gammainc_scheme}
\end{figure}
% trivial values
\paragraph{Trivial values.}
For some particular values of the parameters $x$ and $a$, the result is almost trivial. We remark that, even if the definition of the incomplete gamma functions require $\Re a > 0$ (and so, for us, $a\in\RR^+$), we add in our implementation the case $a = 0$, which has to be intended as a limit.\\

%% a = x = 0.
If both $a$ and $x$ are equal to zero, we get
\begin{subequations}
\begin{equation}
P(0,0) = P_S(0,0) = 0,
\end{equation}
and
\begin{equation}
Q(0,0)=Q_S(0,0) = 1.
\end{equation}
\end{subequations}
The result for $Q$ and $Q_S$ is immediate; for $P$ and $P_S$ it has to be interpreted as a limit, and it follows from relations \eqref{eq:PQ1} and \eqref{eq:PQs} respectively.\\

%% x = 0
If $x=0$ but $a\neq0$, then trivially $P(a,x)~=~0$ and $Q(a,x)~=~1$. For $Q_S$ we compute it as a limit
\begin{align*}
Q_S(a,0) &= \lim_{x\to 0^+} \frac{\Gamma(a+1)\ee^x}{x^a}\frac{1}{\Gamma(a)}\int_x^\infty \ee^{-t}t^{a-1}\diff t\\
&= \lim_{x \to 0^+} \frac{\Gamma(a+1)\ee^x}{x^a}\\
&=+\infty.
\end{align*}
%% FIXME: how to prove this
Finally, for $P_S$,
\[ \lim_{x \to 0^+}P_S(a,x) = 1. \]

%% a = 0
If $a=0$ but $x\neq 0$, we have
\begin{subequations}
\begin{align}
P(0,x) &= 1, \\
P_S(0,x)  &= \ee ^ x, \\
Q(0,x) = Q_S(0,x)  &= 0 .
\end{align}
\end{subequations}

%% a = 1
Let us suppose now that $a = 1$ and $x \neq 0$. Then
\[\Gamma(a) = 1, \quad \text{and}\quad t^{a-1}\ee^{-t} = \ee^{-t}.\]
Thus
\begin{align*}
P(1,x) & = \int_0^x \ee^{-t}\diff t = \sbr{ -\ee^{-t}}_{t=0}^{t=x}  = 1 - \ee^{-x},
\end{align*}
and
\begin{align*}
Q(1,x) & = \int_x^\infty \ee^{-t}\diff t = \sbr{ -\ee^{-t} }_{t=x}^{t=\infty} = \ee^{-x}.
\end{align*}
For the scaled versions, we start computing $D(a,x)$ for $a=1$:
\[D(1,x) = x\,\ee^{-x}.\]
Thus, we obtain
\begin{align*}
P_S(1,x) 	& = \frac{P(a,x)}{D(a,x)}  = (1-\ee^{-x})\frac{\ee^x}{x}  = \frac{\ee^{-x}-1}{x},
\end{align*}
and
\begin{align*}
Q_S(1,x) 	& = \frac{Q(a,x)}{D(a,x)}  = \ee^{-x}\,\frac{\ee^x}{x}  = \frac{1}{x}.
\end{align*}

%% a\in\NN
\paragraph{$a$ integer.}
If the parameter $a$ is a positive integer, $a \in \NN$, then \eqref{eq:gamma_Gamma_def} may be integrated completely to yield \cite{AW05}
\begin{align}
\gamma(n,x) & = (n-1)!\br{1-\ee^{-x}\sum_{s=0}^{n-1}\frac{x^s}{s!}}, & n = 1,2,\ldots,
\end{align}
and
\begin{align}
\Gamma(n,x) & = (n-1)! \ee^{-x}\sum_{s=0}^{n-1}\frac{x^s}{s!} , & n = 1,2,\ldots.
\end{align}
\label{eq:gamma_a_N}
Since $\Gamma(n)=(n-1)!$, we compute $P$ and $Q$ as
\begin{align}
P(n,x) & = 1-\ee^{-x}\sum_{s=0}^{n-1}\frac{x^s}{s!}, & n = 1,2,\ldots,
\end{align}
and
\begin{align}
Q(n,x) & = \ee^{-x}\sum_{s=0}^{n-1}\frac{x^s}{s!}, & n = 1,2,\ldots, 
\end{align}
respectively.
To recover $P_S$ and $Q_S$ we just divide by $D(n,x)$.\\
We used this expansion when
\[ (x,a) \in ((-\infty,36],\NN\cap[2,18]),\]
excluding, obviously, the trivial-to-compute values.

%% Taylor series
\paragraph{Series expansion.}
We start finding a series expansion for the function $\gamma(a,x)$.\\
Simply by integrating by parts, we obtain
\begin{align*}
\gamma(a,x) 	& = \int_0^x t^{a-1}\ee^{-t}\, dt \\
				& = \sbr{ \frac{\ee^{-t} t^a} {a} }_{t=0}^{t=x}  + \frac{1}{a} \int_0^x t^a \ee^{-t}\diff t \\
				& = \frac{\ee^{-x} x^a}{a} + \frac{1}{a} \, \gamma(a+1,x) \\
				& = \frac{\ee^{-x} x^a}{a} + \frac{1}{a} \br{ \frac{\ee^{-x}x^{a+1}}{a+1} + \frac{1}{a+1} \, \gamma(a+2,x) }\\
				& = \ee^{-x} x^a \br{ \frac{1}{a} + \frac{x}{a(a+1)} + \frac{1}{a(a+1)} \, \gamma(a+2,x) }\\
				& \, \, \vdots \\
				& = \ee^{-x} x^a \br{\frac{1}{a} + \frac{x}{a(a+1)} + \frac{x^2}{a(a+1)(a+2)} + \ldots},\\
\end{align*}
which can be rearranged as
\begin{equation}
\gamma(a,x) = \ee^{-x} x^a \sum_{n=0}^\infty \frac{\Gamma(a)}{\Gamma(a+n+1)}\,x^n.
\label{eq:gamma_series}
\end{equation}
Thus we can compute $P(a,x)$ as
\begin{equation}
P(a,x) = \ee^{-x} x^a \sum_{n=0}^\infty \frac{x^n}{\Gamma(a+n+1)}.
\end{equation}
and $P_S(a,x)$ as
\[P_S(a,x) = \Gamma(a+1) \sum_{n=0}^\infty \frac{x^n}{\Gamma(a+n+1)}.\]
In a more stable way, we can compute, using \eqref{eq:prop_gamma_iter} with $z=a+n$ and $M=n-1$, the ratio
\[ \frac{\Gamma (a+1)}{\Gamma(a+n+1)} = \dfrac{\Gamma (a+1)}{ \br{ \prod_{k=0} ^ {n-1} (a+n-k) } \Gamma (a+n - (n-1)) } = \br{ \prod_{k=0} ^ {n-1} (a+n-k)}^{-1},\]
so that
\begin{subequations}
\begin{equation}
 P_S(a,x) = \sum_{n=0} ^ \infty c_n (a) x^n, 
\end{equation}
where
\begin{align}
c_0 (a) &= 1,\\
c_j (a) &= \br { \prod_{k=1} ^ j (a+j) } ^{-1}, & j\in\NN.
\end{align}
In a recursive fashion
\begin{align}
c_j(a) & = \frac{c_{j-1}(a)}{a+j}, & j&\in\NN.
\end{align}
\end{subequations}
To compute $Q(a,x)$ we can simply use \eqref{eq:PQ1}, while to compute $Q_S(a,x)$ we use \eqref{eq:PQs}.\\
We used this expansion when
\[ (x,a) \in ((-20,1),\RR^+)\,\cup ((-20,+\infty),(0,5)) \cup ((-20,+\infty),(0,x+0.25)),\]
excluding, obviously, the values for which we already computed the functions.

%% C.F.

\paragraph{Continued fractions.}
From \eqref{eq:gamma_series} and using Euler connection (see \S~\ref{sec:Euler_connection}) we obtain the following continued fraction development:
\begin{subequations} \label{eq:gamma_CF}
\begin{equation}\label{eq:gamma_CF_1}
\frac{\gamma(a,x)}{x^a\ee^{-x}} = \frac{1}{x} \K_{m=1}^\infty \br{ \frac{ c_m(a) x } { 1 } },
\end{equation}
where the coefficients $c_m(a)$ are given by
\begin{align}
c_1(a)	& =	\frac{1}{a},\\
c_{ 2 j } ( a ) & = \frac{- (a + j + 1)}{ (a+2j-2) (a+2j-1) }, & j\in\NN,\\
c_{ 2 j + 1} ( a ) & = \frac{ j }{ (a+2j-1) (a+2j) },  & j\in\NN.
\end{align}
\end{subequations}
Analogously, we obtain the following continued fraction development for the upper incomplete gamma function:
\begin{subequations}\label{eqs:Gamma_CF}
\begin{equation}
\frac{\Gamma(a,x)}{x^a\ee^{-x}} =  \K_{m=1}^\infty \br{ \frac{ \frac{d_m(a)}{x} } { 1 } },
\end{equation}
where the coefficients $d_m(a)$ are given by
\begin{align}
d_1(a)	& =	1,\\
d_{ 2 j } ( a ) & = j-a, & j\in\NN, \\
d_{ 2 j + 1} ( a ) & = j, & j\in\NN.
\end{align}
This expression can be rearranged to obtain (in a more intuitive form)
\begin{equation}
 \frac{\Gamma(a,x)}{x^a\ee^{-x}} = \frac{ d_1 (a)}{x}\cplus \frac{ d_2 (a)}{1} \cplus \frac{ d_3 (a)}{x}\cplus \frac{ d_4 (a)}{1}\cplus \contdots .
\label{eq:Gamma_CF}
\end{equation}
\end{subequations}
To make the computation faster, we use the even part of \eqref{eq:Gamma_CF} (see \S~\ref{sec:manipulating_CF} for more details):
\begin{align}
\frac{\Gamma (a,x)}{x^a\ee^{-x}} & = \frac{1}{1+x-a}\cplus \K_{m=2}^\infty \br{ \frac{(1-m)(m-1-a)}{(2m-1)+z-a} }\nonumber\\
	& = \frac{1}{1+x-a}\cplus\frac{-(1-a)}{3+x-a}\cplus \frac{-2(2-a)}{5+x-a}\cplus\frac{-3(3-a)}{7+x-a}\cplus\contdots. \label{eq:Gamma_CF_2}
\end{align}
If we call $s_a(x)$ and $S_a(x)$ the right hand sides of $\gamma(a,x)/(x^a\ee^{-x})$ in \eqref{eq:gamma_CF_1} and of $\Gamma(a,x)/(x^a\ee^{-x})$ in \eqref{eq:Gamma_CF_2} respectively, we have
\[P(a,x) = \frac{\gamma(a,x)}{\Gamma(a)} = s_a(x)\frac{x^a\ee^{-x}}{\Gamma(a)} = a\,s_a(x)D(a,x),\]
and
\[Q(a,x) = \frac{\Gamma(a,x)}{\Gamma(a)} = S_a(x)\frac{x^a\ee^{-x}}{\Gamma(a)} = a\,S_a(x)D(a,x).\]
For the scaled versions $P_S(a,x)$ and $Q_S(a,x)$, we get
\[P_S(a,x) = P(a,x)\frac{1}{D(a,x)} = a\,s_a(x),\]
and
\[Q_S(a,x) = Q(a,x)\frac{1}{D(a,x)} = a\, S_a(x).\]
We employed this expansion for all remaining terms.
%%
\paragraph{Recurrence relations.}
For some values, in particular for small values of $a$ and negative values for $x$, using directly the series expansion or the continued fractions give a huge problem of numerical instability. To (at least partially) solve it, we employed a recurrence formula.
\begin{prop}[Recurrence formula for the incomplete gamma function] The lower gamma function ratio $P(a,x)$ satisfies the following recurrence formula
\begin{equation}\label{eq:recurr_rel_P}
P(a + 1, x) = P(a,x) - D(a,x).
\end{equation}
\end{prop}
\begin{proof}
By definition and integrating by parts:
\begin{align*}
P(a+1,x) 	& = \frac{1}{\Gamma(a+1)} \int_0^x \ee^{-t} t^a\diff t\\
			& = \frac{1}{\Gamma(a+1)} \br { \sbr {-\ee^{-t} t^a}_{t=0}^{t=x}  + a\int_0^x \ee^{-t} t^{a-1}\diff t} \\
			& = - \frac{\ee^{-x}x^a}{\Gamma(a+1)} + \frac{1}{a\Gamma(a)} a \gamma (a,x)\\
			& = - D(a,x) + P(a,x) ,
\end{align*}
as claimed.
\end{proof}
Since we want to use a recurrence formula when $a < 2$, and we want to use it for all four types of incomplete gamma function we are considering ($P(a,x)$, $Q(a,x)$, $P_S(a,x)$ and $Q_S(a,x)$), we have to work a bit on this formula.\\
By using two times \eqref{eq:recurr_rel_P}, we obtain, for $P(a,x)$,
\begin{subequations}\label{eqs:recurrences_gammainc}
\begin{align}
P(a,x) 	& = P(a+1,x) + D(a,x) \nonumber\\\label{eq:recurr2_P}
		& = P(a+2,x) + D(a+1,x) + D(a,x).
\end{align}
Now, by using \eqref{eq:PQ1} on this, we get, for $Q(a,x)$,
\begin{equation*}
1 - Q(a,x)  = 1 - Q(a+2,x) + D(a+1,x) + D(a,x),
\end{equation*}
thus
\begin{equation}
Q(a,x) = Q(a+2,x) - D(a+1,x) - D(a,x). \label{eq:recurr2_Q}
\end{equation}
Now, by using the definition \eqref{eq:def_scaled_gammainc_low} and the recurrence formula \eqref{eq:recurr2_P}, we obtain, for $P_S(a,x)$,
\begin{equation*}
P_S(a,x) D(a,x) = P_S(a+2,x) D(a+2,x) + D(a+1,x) + D(a,x).
\end{equation*}
We compute the ratios
\begin{align*}
\frac{D(a+1,x)}{D(a,x)} & = \frac{\ee^{-x} x ^ {a+1}}{\Gamma (a+2)} \frac{\Gamma(a+1)}{\ee^{-x} x^a} = \frac{x}{a+1},\\
\frac{D(a+2,x)}{D(a,x)} & = \frac{\ee^{-x} x ^ {a+2}}{\Gamma (a+3)} \frac{\Gamma(a+1)}{\ee^{-x} x^a} = \frac{x^2}{(a+2)(a+1)},
\end{align*}
so that we obtain
\begin{equation}
P_S(a,x) = P_S(a+2,x) \frac{x^2}{(a+2)(a+1)} + \frac{x}{a+1} + 1.
\end{equation}
Finally, using definition \eqref{eq:def_scaled_gammainc_upp} and the recurrence relation \ref{eq:recurr2_Q}, we obtain, for $Q_S(a,x)$,
\begin{equation*}
Q_S(a,x) D(a,x) = Q_S(a+2,x) D(a+2,x) - D(a+1,x) - D(a,x),
\end{equation*}
from which
\begin{equation}
Q_S(a,x) = Q_S(a+2,x) \frac{x^2}{(a+2)(a+1)} - \frac{x}{a+1} - 1.
\end{equation}
\end{subequations}
Using formulae \eqref{eqs:recurrences_gammainc}, we can compute the incomplete gamma functions for $ a < 2 $ and $ x < 0 $ by computing them for $a ' = a + 2$ and then rescaling. This permits to gain numerical accuracy.
\subsection{Numerical results}
To test our implementation, we compute the relative error for various values of $x$ and $a$, using as reference values those computed by SageMath\footnote{Free, open-source software for symbolic calculus, \url{http://www.sagemath.org/}.}. We report some of these results in Figure~\ref{fig:gammainc_result}. The tests have been done on $P(a,x)$.\\
From these results it is possible to observe that there is still room for improvement for certain couples of values, expecially for large values of $a$ and small values of $x$. However, a similar relative error is done by most softwares for numerical calculus.
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\linewidth]{plots/plot_err_gammainc.eps}
\caption{Relative error (in $\log_{10}$) for the function $P(x,a)$ for various values of $x$ and $a$.}
\label{fig:gammainc_result}
\end{figure}
%%
\section{The $D$ function}\label{sec:D_fun}
Since we have seen that the function $D(a,x)$ defined by \eqref{eq:D_def} is quite important in this argument, we now see a stable way to compute it. We use the strategy present in \cite{K86} in computing
\[p(a,x) = \frac{\ee^{-x}x^{a-1}}{\Gamma(a)},\]
noticing that
\[D(a,x) = p(a+1,x).\]
A simple way to compute it would be to compute its logarithm as
\begin{equation}
 \log ( p ( a , x ) ) = ( a - 1 ) \log ( x ) - x - \lgamma ( a ) ,
\label{eq:logp_v1}
\end{equation}
and then compute the exponential of this quantity. For large $a$ and $x$, however, this method suffers under severe cancellation. The effect of cancellation can be attenuated by introducing a scaled version of the gamma function
\[G ( x ) = \frac{ \Gamma ( x + 1 ) } { \br {\frac{x}{e}} ^ x}.\]
Now, manipulating the formula \cite{AS64}
\begin{multline*}
 \lgamma (z)  \approx \br{z-\frac{1}{2}} \log(z) - z + \frac{1}{2} \log (2\pi) + \frac{1}{12z} - \frac{1}{360 z^3} + \frac{1}{1260 z^5} - \frac{1}{1680 z ^ 7} + \ldots, \\ z\to\infty, \quad |\arg(z)|<\pi, %Abramowitz 6.1.41
\end{multline*}
we get
\begin{align*}
\log ( G ( x ) ) & \approx \frac{1}{2} \log (2 \pi x) + \frac{1}{12x} - \frac{1}{360 x^3} + \frac{1}{1260 x^5} - \frac{1}{1680 x ^ 7} + \ldots, & x\to\infty.
\end{align*}
For large $a$ and $x$, the central limit theorem yields the approximation
\[p(a,x) \approx \frac{1}{\sqrt{2\pi x}}\exp \br{\frac{-t^2}{2}},\]
where $t$ is defined as
\[t = \frac{a - 1 - x}{\sqrt{x}}.\]
We get form \eqref{eq:logp_v1} for $a>1$
\begin{equation}
 \log ( p ( a , x ) ) = ( b - x ) - b \log \br { \frac{b}{x} } \log ( G ( x ) ) ,
\label{eq:logp_v2}
\end{equation}
where $b = a-1>0$.\\
Both methods \eqref{eq:logp_v1} and \eqref{eq:logp_v2} are of the form $\log ( p ) = A - B$ where $AB>0$ and for large $b = a - 1$ and $x$ we have
\begin{align*}
A	& =	b \, \log(x)	&\text{in case \eqref{eq:logp_v1}},\\
A	& \approx t\sqrt{x} 	&\text{in case \eqref{eq:logp_v2}}.
\end{align*}
The effect of cancellation is less severe with \eqref{eq:logp_v2} in particular when $ t $ is small. For $x = b$, where $ p $ reach its maximum as function of $ x $, there is no cancellation at all and $ \log (p) $is given with full accuracy by \eqref{eq:logp_v2}.\\
Unfortunately, when $\nicefrac{b}{x}\approx 1$, the function $ \log \br{\nicefrac{b}{x}} $ is ill-conditioned. The problem can be overcome by
introducing the \emph{shifted log} function
\begin{align*}
\logs (x) & = \log (1 + x), & x\geq 0,
\end{align*}
and computing $\log\br{\nicefrac{b}{x}}$ as
\[ \log\br{\dfrac{b}{x}} = \begin{cases}
\logs \br {\dfrac{b-x}{x}} & \text{for }b\geq x,\\
- \logs \br {\dfrac{x-b}{b}} & \text{for }b< x,
\end{cases} \]
\section{The inverse of the incomplete gamma function}
We are interested in the inversion of $P$ and $Q$ as functions of $x$ with $a$ fixed. Our approach consists in a simple Newton's method. However, we need a good first approximations to start it. Those are fully provided by \cite{GST12}. We remark that these initial guesses are based on different expansions of $P$ and $Q$ from those that we saw and used to compute the incomplete gamma functions.\\
We solve the equations
\[P(a,x) = p,\quad Q(a,x) = q, \quad p,q\in[0,1]\]
for $ x $, with $ a $ as fixed positive parameter; thus, we will use the derivatives along $x$ of $P(a,x)$ and $Q(a,x)$
\begin{subequations}
\begin{align}
\pderiv{P(a,x)}{x} & = \frac{x ^ {a-1} \ee ^ {-x}}{\Gamma(a)},\\
\pderiv{Q(a,x)}{x} & = - \frac{x ^ {a-1} \ee ^ {-x}}{\Gamma(a)}.
\end{align}
\end{subequations}
In most cases we invert the equation with $ \min \{ p , q \} $. We remark that if $ x (p,a)$ denotes the solution to the first equation, then the solution of the second one satisfies 
\[x ( q , a ) = x ( 1 - p, a ).\]
\paragraph{Small values of $p$.}
We consider the following series expansion of $P(a,x)$:
\[ P ( a , x ) = \frac{x^a}{\Gamma (a) } \sum_{n=0}^\infty \frac{ (-1)^n x^n }{ (a+n) n!} .\]
The inversion problem can be written as
\[ x = r \br{ 1 + \sum_{n=0}^\infty \frac{a(-1)^n x^n}{(a+n)n!} } ^ {-\frac{1}{a}}, \]
with 
\[r = (p\,\Gamma(1+a))^{\frac{1}{a}}.\]
Inverting this relation, we obtain
\[x = r + \sum_{k=2}^\infty c_k r^k,\]
where the first coefficients $c_k$ are
\begin{align*}
c_2 & = \frac{1}{a+1},\\
c_3 & = \frac{3a+5}{2(a+1)^2(a+2)},\\
c_4 & = \frac{8a^2+33a+31}{3(a+1)^3(a+2)(a+3)},\\
c_5 & = \frac{125a^4 + 1179a^3 + 3971 a^2 + 5661a+2888}{24(1+a)^4(a+2)^2(a+3)(a+4)}.
\end{align*}
We can use this expression to obtain a four digits accuracy in $ x $. This is enough to start a stable Newton's method.\\
We use this strategy when $r < 0.2 (1+a)$, that is when
\[p < \frac{(0.2(1+a))^a}{\Gamma(a+1)}.\]
\paragraph{Small values of $q$.}
We consider the asymptotic expansion
\begin{align*}
Q(a,x) & \approx \frac{ x ^ {a - 1} \ee ^ {-x} }{\Gamma(a)} \sum_ {n=0} ^ \infty \frac{(-1)^n (1-a)_n}{ x^ n}, & x\to\infty,
\end{align*}
where $ (\cdot )_n $ denotes the \emph{Pochhammer symbol} defined as
\begin{equation}\label{eq:def_pochhammer}
(a)_n = \frac{\Gamma(a+n)}{\Gamma(a)} = (a+n-1)(a+n-2)\cdots(a+1)a.
\end{equation}
A first approximation $ x_0 $ of $ x $ is obtained by
\[ \ee^{-x_0} x_0 ^ a= q \Gamma(a). \]
Higher approximations of $ x $ are obtained in the form 
\[ x \approx x_0 - L + b \sum _{k=1} ^ \infty \frac{d_k}{x_0^k},\]
where
\[ b = 1 - a, \quad L = \log ( x_0 ), \]
with first coefficients
\begin{align*}
d_1 &= L-1,\\
d_2 &= \frac{1}{2} ( 3b - 2bL + L^2 - 2L + 2 ),\\
d_3 &= \frac{1}{6} ( 24bL - 11b^2 - 24b - 6L^2 + 12L - 12 -9bL^2 + 6b^2L + 2L^3 ),\\
d_4 & = \begin{multlined}[t] \frac{1}{12} ( 72 + 36L^2 + 3L^4 - 72L +162b - 168bL -12L^3 +25b^3 - 22bL^3 \\
 + 36b^2L^2 - 12b^3L + 84 bL^2 + 120 b^2 - 114 b^2L ).\end{multlined} 
\end{align*}
With $ a \in (0,10) $ and $ q < \ee^{-\nicefrac{a}{2}} / \Gamma(a+1) $, we obtain four digits accuracy in $ x $, which is enough to start the  Newton's method.
\paragraph{Small values of $a$.}
We consider the inversion of $P$ for $ a \in (0,1) $. We observe that
\[P(a,x) = \frac{1}{\Gamma ( a ) } \int _ 0 ^ x t^{a-1} \ee ^{-t} \, dt < \frac{1}{\Gamma ( a )} \int _0 ^ x t^{a-1}\diff t = \frac{x^a}{\Gamma ( a + 1 )}\]
and
\[ P(a,x) > \frac{1}{\Gamma (a) }\int _ 0 ^ x \ee^{-t}\diff t = \frac{1-\ee^{-x}}{\Gamma (a)}. \]
Let $x_l$ and $x_u$ be defined as
\[x_l = (p\Gamma(a+1)) ^ {\nicefrac{1}{a}},\quad \text{and}\quad x_u = -\log (1-p\Gamma(a+1))\]
respectively.
Then the solution $ x $ of $ P(a,x) = p $ with $ a \in (0,1) $ satisfies $x\in(x_l,x_u)$.\\
These bounds can be used to start a Newton's method. The best choice is $x_l$ since the Newton's method necessarily converges from this initial guess. The reason is that $P(a,x)$ is an increasing function with negative second derivative. This initial approximation may be inaccurate; however, convergence is certain.
\paragraph{Large values of $ a $.}
For more details we refer to \cite{GST12} and \cite{T92}, in particular to know the details on the function $ R_a $.\\
We consider the representations
\begin{align*}
P ( a , x ) & = \frac{1}{2} \erfc \br{-\eta \frac{a}{2}} - R_a(\eta)\\
Q ( a , x ) & = \frac{1}{2} \erfc \br{\eta \frac{a}{2}} + R_a(\eta)
\end{align*}
where
\[ \erfc(x) = \frac{2}{\sqrt{\pi}} \int_x^\infty \ee^{-t^2}\diff t \]
is the complementary error function and the quantity $ \eta $ is such that
\begin{equation}
\frac{1}{2} \eta ^ 2 = \lambda - 1 - \log\lambda 
\label{eq:eta_lambda}
\end{equation}
with $ \lambda = x / a $ and $\sign (\eta) = \sign (\lambda - 1 )$.\\
We rewrite the inversion problem in the form
\begin{align*}
\frac{1}{2} \erfc \br{ \eta \sqrt{ \frac{a}{2} } } + R_a(\eta) & = q, & q\in[0,1],
\end{align*}
and we denote the solution as $ \eta(q,a) $.\\
We consider $ R_a ( \eta ) $ as a perturbation and define $ \eta_0 = \eta_0 ( q , a ) $ as the real number satisfying
\[ \frac{1}{2} \erfc \br {\eta_0 \sqrt{\frac{a}{2}}} = q. \]
For large values of $ a $, the value $ \eta $ can be approximated by
\[ \eta ( q , a ) = \eta_0 ( q , a ) + \varepsilon (\eta_0 , a), \]
and it is possible to expand
\begin{equation}
\varepsilon (\eta_0 , a )\approx \sum_{j=1}^\infty \frac{\varepsilon_j (\eta_0 , a )}{a^j},
\label{eq:epsilon}
\end{equation}
as $a\to\infty$. We refer to \cite{T92} for the coefficients $\varepsilon_j$, the first one being
\[\varepsilon_1 ( \eta_0 , a ) = \frac{1}{\eta_0} \log \br{ \frac{\eta_0}{\lambda_0-1} },\]
where $\lambda_0$ follows from inverting \eqref{eq:eta_lambda} with $ \eta $ replaced by $ \eta_0 $.\\
Using \eqref{eq:epsilon} with four terms for $a \geq 1$ and $q\in(0,1)$ we can obtain three or four significant digits, which is enough to start a stable Newton's method.
