\emph{Bessel functions} are the canonical solutions $w(z)$ of the \emph{Bessel's differential equation}
\begin{align}\label{eq:bessel_eq}
z^2 \frac{d^2w}{dz^2} + z \frac{dw}{dz} + (z^2-\nu^2) w = 0, && \nu\in\CC.
\end{align}
This equation is actually the one describing the radial component of the 2D wave equation
\[ \pderiv[2]{u}{t} - c^2 \Delta u = 0.\]
In this work we present the implementation of the Bessel functions present in the Amos library\footnote{\url{http://netlib.org/amos/}}, which we discovered to be one of the (if not the) most accurate. The description of the various types of Bessel functions will not follow the ``standard'' order of presentation which can be found in most of the textbooks. The reason is that, in this library, only two kinds of Bessel functions are evaluated (and these will be the first we present), and all the others are computed using some relations with these two.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  MODIFIED BESSEL FUNCTIONS  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modified Bessel functions $I_\nu(z)$ and $K_\nu(z)$}
The second order differential equation
\begin{align*}
z^2 \frac{d^2w}{dz^2} + z \frac{dw}{dz} - (z^2+\nu^2) w = 0,&& \nu\in\CC
\end{align*}
is called \emph{modified Bessel differential equation}. The solutions $I_\nu (z)$ and $K_\nu (z)$ are called \emph{modified bessel functions}.\\
The \emph{modified Bessel function of the first kind} $I_\nu(z)$ is defined in terms of the Bessel function $J_\nu (z)$ (see \S~\ref{sec:besselj}):
\begin{subequations}\label{eqs:def_besselI}
\begin{align}
I_\nu (z) &= \exp\br{ -\ii\nu\frac{\pi}{2} } J_\nu (\ii z), & -\pi < \arg (z) \leq \frac{\pi}{2},\quad \nu\in\CC, \\
I_\nu (z) &= \exp\br{ \ii\nu\frac{3\pi}{2} } J_\nu (\ii z), & \frac{\pi}{2} < \arg (z) \leq \pi,\quad \nu\in\CC;
\end{align}
\end{subequations}
A definition which does not depend on $J_\nu(z)$ is
\begin{align*}
I_\nu (z) &= \frac{1}{\pi} \int_0^\pi \ee ^{z\cos(\theta)} \cos(\nu\theta)\diff \theta - \frac{\sin(\nu\pi)}{\pi} \int_0^\infty \ee ^{ -z\cosh(t) -\nu t }\diff t, & \Re z &> 0,
\end{align*}
which simplifies, for $\nu\in\NN$, to
\begin{align*}
 I_n(z) & = \frac{1}{\pi} \int_0^\pi \ee ^{z\cos(\theta)} \cos(n\theta)\diff \theta, & z&\in\CC,\quad n\in\NN.
\end{align*}

The \emph{modified Bessel function of the second kind} $K_\nu (z)$ is defined by
\begin{align}\label{eq:def_besselK}
K_\nu (z) &= \frac{\pi}{2} \frac{I_{-\nu} (z) - I_\nu (z)}{\sin (\nu \pi)}, & \nu\in\CC\setminus\ZZ.
\end{align}
For integer order $n\in\ZZ$, the right hand side of \eqref{eq:def_besselK} is replaced by its limit
\begin{align*}
K_n (z) &= \lim_{\nu \to n} \frac{\pi}{2} \csc (\nu \pi) (I_{-\nu} (z) - I_\nu (z)), & n\in\ZZ.
\end{align*}

The functions $I_{\nu}(z)$ and $K_{\nu} (z)$ are linearly independent for any $\nu \in \CC$, and the functions $I_\nu (z)$ and $I_{-\nu} (z)$ are linearly independent for $\nu\in\CC\setminus\ZZ$. The function $I_\nu(z)$ is sometimes called \emph{hyperbolic Bessel function}, and the function $K_\nu(z)$ is also called \emph{Basset function} or \emph{Macdonald function}.

\begin{prop}[Symmetry and reflection formulae for $I_\nu(z)$ and $K_\nu (z) $]
The modified Bessel functions $I_\nu(z)$ and $K_\nu (z)$ satisfy the symmetry properties
\begin{subequations}\label{eqs:sym_besI_besK}
\begin{align}
I_\nu (\bar{z}) &= \conj {I_\nu (z)}, & \nu\in \RR, \label{eq:sym_besselI}\\
K_\nu (\bar{z}) &= \conj {K_\nu (z)}, & \nu\in \RR, \label{eq:symm_besselK}
\end{align}
\end{subequations}
and the reflection formulae
\begin{subequations}\label{eqs:refl_besI_besK}
\begin{align}
I_\nu (-x) & = (-1)^\nu I_\nu (x), & x>0,\quad \nu\in\CC \label{eq:refl_besselIcmplx}\\
I_{- \nu} (z) & = I_\nu (z) + \frac { 2 }{ \pi } \sin (\pi \nu) K_\nu (z), & z \in \CC, \quad \nu \in \CC\\
I_{-n} (z) & = I_n (z), & z\in\CC,\quad n\in\ZZ \label{eq:refl_besselIint} \\
K_{-\nu} (z) & = K_\nu (z), & z \in \CC, \quad \nu \in \CC.
\end{align}
\end{subequations}
\end{prop}

\subsection{Numerical evaluation}
The basic idea is to compute the $I$ and $K$ functions in the right half $z$ plane $\Re z \geq 0$ and relate all other functions to these two (we will se how when we will talk about them). This idea extends also to their analytic continuations through the formulae
\begin{subequations}\label{eqs:anal_cont_besI_besK}
\begin{align}
I_\nu \br{z \, \ee^{\pm \ii \pi}} 	& = \ee^{\pm \ii \pi \nu } I_\nu (z), & \Re z > 0,\\
K_\nu \br{z \, \ee^{\pm \ii \pi}}	& = \ee^{\mp \ii \pi \nu} K_\nu (z) \mp \ii \pi I_\nu (z), & \Re z > 0.
\end{align}
\end{subequations}
Figures \ref{fig:besselK_scheme} and \ref{fig:besselI_scheme} show how the right half complex $z$ plane is divided in the various domains of computation.
\begin{figure}[hbt]
\centering
\resizebox{0.8\textwidth}{!}{
\input{manuscript/besselKscheme.tex}}
\caption{Computation diagram for $K_\nu (z)$. In the blue part we use the power series, in the yellow one the Miller algorithm, while in the violet one the uniform asymptotic expansion for $\nu\to\infty$.}
\label{fig:besselK_scheme}
\end{figure}

\paragraph{Power series of $K_\nu (z)$.}
Power series is applied to compute $K_\nu (z)$ and $ K_{\nu + 1} (z) $ for $ - \nicefrac{1}{2} \leq \nu < \nicefrac{1}{2} $. Forward recurrence on
\begin{equation}\label{eq:forw_rec_besK}
K_{\nu + 1} (z) = \frac{2\nu}{z} K_\nu (z) + K_{\nu - 1} (z)
\end{equation}
generates larger orders in the regions not covered by fundamental formulae. In this part we consider $|z| \leq 2$, $\Re z \geq 0$ and $ - \nicefrac{1}{2} \leq \nu < \nicefrac{1}{2} $. The cases for $\nu = \pm \nicefrac{1}{2}$ have explicit formulae
\[ K_{-\frac{1}{2}} (z) = K_{ \frac{1}{2} } (z) = \ee^{-z} \sqrt{\frac{\pi}{2z}}, \]
and so they can be computed directly.\\

The power series for $K_\nu (z)$ and $K_{\nu + 1} (z)$ can be written in the form \cite{Tem75}
\begin{subequations}
\begin{align}
K_\nu (z) = \sum_{k=0} ^ \infty c_k f_k,\\
K_{\nu+1} = \frac{2}{z} \, \sum_{k=0} ^ \infty c_k (p_k - k f_k),
\end{align}
with
\begin{align}
c_0 & = 1, & c_k &= \frac{z ^ {2k}}{4^k k!}, & k\geq 1,\\
p_0 & = \frac{1}{2} \br{ \frac{z}{2} } ^ {-\nu} \Gamma (1+\nu), & p_k &= \frac{p_{k-1}}{k-\nu}, & k\geq 1,\\
q_0 & = \frac{1}{2} \br { \frac{z}{2} }^\nu \Gamma (1-\nu), & q_k  & = \frac{q_{k-1}}{k+\nu}, & k \geq 1,\\
f_0 & = \frac{\pi\nu}{\sin (\pi\nu)} \br{ \Gamma_1 (\nu) \cosh(\mu) + \Gamma_2(\nu) \log \br{ \frac{2}{z} } \frac{\sinh (\mu)}{\mu} } \span\span\span,\\
f_k & =  \frac{k f_{k-1} + p_{k-1} + q_{k-1}}{k^2 - \nu ^2} &&& k \geq 1
\end{align}
where $\mu$ is defined as $\mu = \nu \log (\nicefrac{2}{z})$ and
\begin{align}
\Gamma_1(\nu) & = \frac{1}{2\nu} \br{ \frac{1}{\Gamma (1-\nu)} - \frac{1}{\Gamma (1+\nu)}}, \\
\Gamma_2(\nu) & = \frac{1}{2} \br{ \frac{1}{\Gamma (1-\nu)} + \frac{1}{\Gamma (1+\nu)}}.
\end{align}
\end{subequations}
These power series are a rearrangement of the definition \eqref{eq:def_besselK} using the power series of $I_\nu (z) $ and of $I_{-\nu} (z)$, that we will discuss later.
\paragraph{Miller algorithm for $K_\nu (z)$.}
In this part, $K_\nu (z) $ and $K_{\nu + 1} (z)$ on $\nicefrac{1}{2} < \nu \leq \nicefrac{1}{2}$, $|z| > 2$, are related to confluent hypergeometric functions of second kind (see \S~\ref{sec:hyperg_confl_second_kind} for further details), which are computed by the Miller algorithm. The method computes
\begin{subequations}\label{eqs:besK_hyperg}
\begin{equation}
K_\nu (z) = \sqrt{\pi} (2z)^\nu \ee^{-z} U\br{\nu + \frac{1}{2}, 2\nu + 1, 2z}
\end{equation}
and
\begin{equation}
K_{\nu+1} (z) = \frac{K_\nu (z)}{z} \br{ \nu+\frac{1}{2} + z - \frac{k_1(z)}{k_0(z)} }
\end{equation}
\end{subequations}
where the functions $k_n$ are defined as
\begin{align}
k_n(z) & = (-1)^n (\nu,n)\, U\br{ \nu + \frac{1}{2} + n, 2\nu+1, 2z }, & n&=0,1,2,\ldots,
\end{align}
where $(\nu,n)$ is the \emph{Hankel's symbol} given by
\[ (\nu, n) = \frac{\Gamma \br{ \nu + \frac{1}{2} + n }}{n!\, \Gamma \br{ \nu + \frac{1}{2} - n }} = \frac{(-1)^n\cos(\pi\nu)}{\pi\, n!} \Gamma \br{ \nu+\frac{1}{2} + n } \Gamma \br{\frac{1}{2} - \nu + n} \]
for $-\nicefrac{1}{2} \leq \nu < \nicefrac{1}{2}$, where $U$ is the confluent hypergeometric function of the second kind.\\

We will now describe how to compute the functions $k_0$ and $k_1$ using the so-called \emph{Miller's algorithm}. The procedure use the relation
\begin{subequations}\label{eqs:k_rel}
\begin{align}\label{eq:k_rel}
k_{n+1} (z) - b_n(z) k_n(z) + a_n(z) k_{n-1}(z) & = 0, & n&\geq 1,
\end{align}
where
\begin{align}
a_n & = \frac{ \br{n-\frac{1}{2}} ^2 - \nu ^ 2 }{n(n+1)}, \label{eq:a_coeff_def}\\
b_n & = \frac {2 (n+z) } {n+1},
\end{align}
\end{subequations}
and the normalizing relation
\begin{equation}\label{eq:k_norm_rel}
\sum_{n=0}^\infty k_n (z) = (2z) ^ {-\nu-\frac{1}{2}}.
\end{equation}
The procedure select a positive integer $M$ (we will se later how to compute it) and a sequence $\tilde{k}_0^{(M)}, \tilde{k}_1^{(M)},\ldots,\tilde{k}_M^{(M)}$ is computed using \eqref{eq:k_rel} in a backward direction with initial values $\tilde{k}_{M+1}^{(M)} = 0$ and $\tilde{k}^{(M)}_M = 1$. By normalizing $\tilde{k}_0^{(M)}$ and $\tilde{k}_1^{(M)}$ with \eqref{eq:k_norm_rel}, $k_0^{(M)}$ and $k_1^{(M)}$ are computed. Then
\begin{align*}
\lim_{M\to\infty} k_n^{(M)} & = k_n (z), & n &= 0,1.
\end{align*}
The relative error $\varepsilon$ of $k_n^{(M)}$ with respect to $k_n(z)$ can be expressed by
\[ k_n ^{(M)} = k_n(z) (1+\varepsilon), \]
where $\varepsilon$ depends on $M,z,n$ and $\nu$. As in \cite{Gau67}, the determination of $M$ can be based on an asymptotic formula for $k_n$. However, a more satisfactory approach is pointed out in \cite{OS72}. Beginning with $p_0=0$ and $p_1=1$, it can be computed the solution $p_n$ of \eqref{eq:k_rel} for $n=1,2,\ldots$. It can be also computed a sequence $\{ e_n \}$ defined as
\[ e_0 = 1, \quad  e_n = a_n e_{n-1}, \]
where $a_n$ is defined by \eqref{eq:a_coeff_def}, giving
\[ e_n = \frac{(-1)^n (\nu,n)}{(n+1)!}. \]
Next, the following quantity is introduced:
\begin{align}\label{eq:def_E_M}
E_M & = \sum_{k=N} ^ \infty \frac{e_k}{p_kp_{k+1}}, & M&\geq 1.
\end{align}
The selection of the starting index $M$ depends on the construction of a bound of $E_M$.\\
In order to construct this bound we consider real values of $z$ and $\nu$. We remark that it sufficies to consider $-\nicefrac{1}{2}\leq \nu < \nicefrac{1}{2}$ and we suppose $z>2$ (then the argument below will be extended to $|z| > 2$). Under these assumptions, we have $b_n \geq a_n + 1$, from which easily follows $P_{n+1} \geq p_n$ for $n\geq 0$. Moreover, $e_n \geq 0$ for $n\geq 0$. Hence, $E_M$ is dominated as follows
\begin{equation}\label{eq:E_M_bound}
E_M \leq \sum_{n=M}^\infty p_n ^ {-2} e_n = \frac{\cos(\pi\nu)}{\pi} \sum_{n=M}^\infty \frac{\Gamma \br{\frac{1}{2}+\nu+n} \Gamma \br {\frac{1}{2} - \nu + n} }{p_n ^ 2n!(n+1)!}.
\end{equation}
The series can be bounded using the following lemma.
\begin{lmm}
Let $a,b$ and $z$ be real numbers such that $b\geq a+1> 0$ and $z>0$. Then
\[ \frac{\Gamma(z+a)}{\Gamma(z+b)} \leq z^{a-b}. \]
\end{lmm}
\begin{proof}
From the equality
\[ \frac{\Gamma(b-a)\Gamma(z+a)}{\Gamma(z+b)} = \int_0^\infty \ee^{-(z+a+1)t} t ^ {b-a-1} \br{\frac{1-\ee^{-t}}{t}} ^ {b-a-1}\diff t .\]
By using
\begin{align*}
\ee^{-(a+1)t} \leq 1,\quad \frac{1-\ee^{-t}}{t} \leq 1, && t\geq 0,
\end{align*}
we obtain
\[ \frac{\Gamma(b-a)\Gamma(z+a)}{\Gamma(z+b)} \leq \int_0^\infty \ee^{-zt} t ^{b-a-1}\diff t, \]
from which the lemma follows.
\end{proof}
Applying the lemma to \eqref{eq:E_M_bound} we obtain
\begin{equation}\label{eq:E_M_est}
E_M \leq \frac{\cos(\pi\nu)}{\pi} \sum_{n=M}^\infty \frac{1}{n^2 p_n^2},
\end{equation}
where the function $p_n$ is a solution of \eqref{eq:k_rel}. Observe that it is possible to replace \eqref{eq:E_M_est} by
\begin{equation}\label{eq:E_M_est_2}
E_M \leq 2\frac{\cos(\pi\nu)}{\pi\sqrt{2z}M^{\frac{3}{2}} p_M^2}.
\end{equation}
The relative error in the Miller algorithm, in our case, is
\begin{equation}\label{eq:rel_err_Miller}
\sigma_M = E_M \sum _{n=0}^M p_n + \sum_{n=M+1}^\infty p_nE_n.
\end{equation}
Hence, by using \eqref{eq:E_M_est_2} for both series in \eqref{eq:rel_err_Miller}, we obtain for $\sigma_M$ the bound
\begin{equation}\label{eq:bound_sigma_M}
\sigma_M \leq \frac{\cos (\nu\pi)}{\pi z M p_M}.
\end{equation}
The least value of $M\geq 1$ for which \eqref{eq:bound_sigma_M} is smaller than the prescribed relative accuracy is taken as the starting index for the Miller algorithm.
\paragraph{Asymptotic expansion of $K_\nu(z)$.}
A uniform expansion for $\nu\to\infty$ is applied in the region $\nu > \nu_L(\varepsilon)$. The boundary value $\nu = \nu_L(\varepsilon)$ depends on the accuracy required and hence on the machine roundoff $\varepsilon$. The asymptotic expansion used by the library is
\begin{align}\label{eq:asymp_besselK}
K_\nu (z) &\approx \sqrt{ \frac{\pi t}{2\nu} } \ee ^{-\nu\xi} \br { 1 + \sum_{k=1}^\infty \frac{(-1)^k U_k (t)}{\nu ^ k} }, & |\arg (z)| < \frac{\pi}{2},
\end{align}
for
\begin{subequations}\label{eqs:asymp_besselI_besselK_coeff}
\begin{align}
t     & = \frac {1}{\sqrt{1 + \br{\frac{z}{\nu}} ^ 2 }}, \\
\xi   & = \sqrt { 1+ \br{ \frac{z}{\nu} } ^ 2 } -  \log\br{ \frac{1+\sqrt{1+\br{\frac{z}{\nu}} ^ 2 }}{\frac{z}{\nu}} },
\end{align}
where
\begin{align}
U_0(t) & = 1, \label{eq:U_0}\\
U_{k+1} (t) & = \frac{1}{2} t^2 \br{1-t^2} U_k'(t) +\frac{1}{8} \int_0^t \br{1 -5\t ^2} U_k(\t)\diff\t, & k & > 1. \label{eq:U_k}
\end{align}
\end{subequations}
We remark that this definition for $U_k$ is not suitable for numerical evaluation. We will come back to this aspect later.\\

Numerical experiments show that $\nu$ must grow dramatically as $\arg(z)$ approaches the boundaries $|\arg(z)|=\nicefrac{\pi}{2}$. As a result, this formulation is used when $|\arg(z)| \leq \nicefrac{\pi}{3}$. Because \eqref{eq:asymp_besselK} cannot be used in the whole right half $z$ plane, the following asymptotic expansions for Hankel functions $H_\nu^{(1)}(z), H_\nu^{(2)}(z)$ and for Bessel function of first kind $J_\nu(z)$ are used, being valid in a region larger than $|\arg(z)|\leq \nicefrac{\pi}{2}$
\begin{subequations}\label{eqs:asymp_hankel_bessel}
\begin{align}
J_\nu (z) & \approx \phi (z) \br{ \Ai (\alpha) S_1 (z,\nu) + \frac{\Ai ' (\alpha)}{\nu ^{\frac{4}{3} } } S_2 (z,\nu)}, \\
H_\nu^{(1)} (z) & \approx 2 \ee ^ {-\frac{\pi\ii}{3} } \phi(z) \br { \Ai(\beta_1) S_1(z,\nu) + \frac{\ee ^ {\frac{2\pi\ii}{3}} \Ai ' (\beta_1) }{\nu ^ {\frac{4}{3} } } S_2 (z,\nu) }, \\
H_\nu^{(2)} (z) & \approx 2 \ee ^ {\frac{\pi\ii}{3} } \phi(z) \br { \Ai(\beta_2) S_1(z,\nu) + \frac{\ee ^ {-\frac{2\pi\ii}{3}} \Ai ' (\beta_2) }{\nu ^ {\frac{4}{3} } } S_2 (z,\nu) }
\end{align}
for $ |\arg(z)| < \pi $, where $\Ai$ is the Airy function; the constants $\alpha,\beta_1$ and $\beta_2$ are
\begin{align}
\alpha &= \nu ^{\frac{2}{3}} \zeta,& \beta_1 &= \ee ^{\frac{2\pi\ii}{3}} \alpha, & \beta_2 &= \ee ^{-\frac{2\pi\ii}{3}} \alpha,
\end{align}
with
\begin{equation}
\frac{2}{3}\zeta^{\frac{3}{2}} = \log \br{ \frac{1 + \sqrt{ 1 - \br{ \frac{z}{\nu} } ^ 2 } } {\frac{z}{\nu}} } - \sqrt{ 1 - \br { \frac{z}{\nu} } ^ 2 };
\end{equation}
the functions $S_1$ and $S_2$ are defined as
\begin{align}
S_1(z,\nu) &= \sum_{k=0}^\infty \frac{A_k(\zeta)}{\nu ^{2k}}, & S_2(z,\nu) &= \sum_{k=0}^\infty \frac{B_k(\zeta)}{\nu ^{2k}},
\end{align}
with $A_k$ and $B_k$ defined as
\begin{align}
A_k(\zeta) &= \sum_{s=0}^{2k} \frac{\mu_s U_{2k-s}(t) }{\zeta ^ {\frac{3s}{2}}} , & B_k (\zeta) = -\sum_{s=0}^{2k+1} \frac{ \lambda_s U_{2k-s+1}(t) }{ \zeta^{\frac{3s+1}{2}} },
\end{align}
where the parameters are $\lambda_0=1$,
\begin{align}
\lambda_s &= \frac{(2s+1)(2s+3)\cdots(6s-1)}{s!\, 144^s}, & \mu_s &= -\frac{6s+1}{6s-1}\lambda_s, & s &=0,1,2,\ldots;
\end{align}
the function $\phi$ is
\begin{equation}
\phi (z) = \frac{1}{\nu ^ {\frac{1}{3}}} \br{ \frac{4\zeta}{1-\br{ \frac{z}{\nu} } ^ 2 } } ^ \frac{1}{4};
\end{equation}
the quantity $t$ is
\begin{equation}
t = \frac{1}{ \sqrt{ 1 - \br { \frac{z}{\nu} } ^ 2 } };
\end{equation}
and $U$ is defined as in \eqref{eq:U_0} and \eqref{eq:U_k}.\\
\end{subequations}
Then $K_\nu(z)$ can be expressed in terms of these functions in the right half $z$ plane by
\begin{subequations}\label{K_rel_half_z_plane}
\begin{align}
K_\nu (z) & = \frac{\pi\ii}{2} \ee ^ {\frac{\ii\pi\nu}{2}} H_\nu ^{(1)} \br { z\ee^{\frac{\ii\pi}{2} } },\\
K_\nu (z) & = -\frac{\pi\ii}{2} \ee ^ {-\frac{\ii\pi\nu}{2}} H_\nu ^{(2)} \br { z\ee^{-\frac{\ii\pi}{2} } };
\end{align}
\end{subequations}
while analytic continuation is handled by means of the formula
\begin{align}\label{eq:K_anal_cont}
K_\nu \br{z\ee ^ {\pm \ii\pi}} & = \ee ^ {\mp \ii\pi\nu} K_\nu (z) \mp \ii\pi I_\nu(z), & \Re z &> 0,
\end{align}
where a positive rotation is used when $z$ is in the fourth quadrant, and a negative rotation when $z$ is in the first quadrant in both \eqref{K_rel_half_z_plane} and \eqref{eq:K_anal_cont}. Even if \eqref{eq:asymp_besselK} cannot be used to cover the whole right half $z$ plane, it is used when $|\arg(z)| \leq \nicefrac{\pi}{3}$ since it involves less computations than using \eqref{eqs:asymp_hankel_bessel} and \eqref{K_rel_half_z_plane}.\\

As we already noticed, the definiton of the functions $U_k$ given by \eqref{eq:U_k} is not suitable for numerical evaluation. These polynomials have the form
\begin{subequations}
\begin{equation}\label{eq:U_poly}
U_k(t) = t^k \sum_{j=0} ^ k c_j(k) t^{2j},
\end{equation}
and the coefficients $c_j(k)$ can be generated by substituting \eqref{eq:U_poly} into \eqref{eq:U_k} and equating same powers of $t$. The result is a set of recurrences:
\begin{align}
c_0(0) &= 1, \\
c_0(k+1) & = c_0(k) \br{ \frac{k}{2} + \frac{1}{8(k+1)} }, &&& k&>0,\\
c_j(k+1)  & =
\begin{multlined}[t]
c_j(k) \br{ \frac{2j+k}{2} + \frac{1}{8(2j + k + 1)} } - c_{j-1} (k) \br { \frac{2j+k-2}{2} + \frac{5}{8(2j+k+1)} },  \\
 1  \leq j \leq k,
\end{multlined}\span\span\span\span\\
c_{k+1} (k+1) & = - c_k (k) \br { \frac{3k}{2} + \frac{5}{8(3k+3)} }, &&& k&>0.
\end{align}
\end{subequations}
\begin{figure}[hbt]
\centering
\resizebox{0.8\textwidth}{!}{
\input{manuscript/besselIscheme.tex}}
\caption{Computation diagram for $I_\nu (z)$. In the blue part we use the power series, in the yellow one the Miller algorithm, in the red one the asymptotic expansion for $|z|\to\infty$, in the orange one we use recurrence and Wronskian, while in the violet part we use the asymptotic expansion for $\nu\to\infty$.}
\label{fig:besselI_scheme}
\end{figure}
\paragraph{Power series of $I_\nu(z)$.}
In the region $|z| \leq 2\sqrt{\nu + 1}$ is employed the series expansion
\begin{subequations}
\begin{equation}
I_\nu(z) = \br{ \frac{z}{2} } ^ \nu \sum_{k=0} ^ \infty \frac{ z ^ {2k} }{ 4 ^ k k!\, \Gamma ( k+1+\nu ) } = \frac{z ^ \nu}{ 2 ^ \nu \Gamma( 1 + \nu ) } \br{ \sum_{k=0} ^ M A_k + R_M },
\end{equation}
where
\begin{align}
A_0 & = 1, \\
A_{k+1} & = \frac{A_k z^2}{4 (k+1) (k+\nu+1)}, & k & = 0,1,2,\ldots,
\end{align}
and $R_M$ is bounded by
\begin{align}
|R_M| & \leq \frac{2|A_M|}{M}, & M&\geq 1.
\end{align}
\end{subequations}
\paragraph{Asymptotic expansion of $I_\nu(z)$ as $|z|\to\infty$.}
The asymptotic expansion for $z\to\infty$ is appropriate in a region where a rapid termination is possible. An examination of the expansion
\begin{subequations}\label{eqs:asymp_besselI}
\begin{multline}
I_\nu (z) \approx \frac{ \ee ^ z }{ \sqrt{ 2 \pi z } } \br{ 1 + \sum _{k = 1} ^ \infty (-1) ^ k A_k (z) } + 
\ee ^ {\pm \br{ \nu + \frac{1}{2} } \pi \ii } \frac{ \ee ^ {-z} }{ \sqrt { 2 \pi z } } \br{ 1 + \sum_{k=1}^\infty A_k(z) }, \\
|\arg(z)| < \pi,
\end{multline}
where
\begin{equation}
A_k (z) = \frac{ \br{4\nu ^ 2 - 1}\cdots \br{4\nu^2 - (2k-1) ^ 2} }{ k!\, (8z)^k },
\end{equation}
\end{subequations}
shows that the factorials will dominate for small value of $k$ when $|8z| > 4\nu^2$, that is $2|z|>\nu ^ 2$. Here $(\nu + \nicefrac{1}{2})\pi\ii$ is taken for $z$ in the first quadrant, and $-(\nu + \nicefrac{1}{2})\pi\ii$ for $z$ in the fourth. Under these conditions, the termination occurs before
\[ k = 2 \floor{R_L(\varepsilon)} + 2 ,\]
where $R_L(\varepsilon)$ is the smallest magnitude needed to terminate the expansion on the condition $|\text{term}| < \varepsilon$. Experimentally, was found that \eqref{eqs:asymp_besselI} works better when
\[ |z| > \max \set{R_L(\varepsilon), \frac{\nu^2}{2}}, \]
where $R_L(\varepsilon)$ has the effect of truncating the region $|z|> \nicefrac{\nu^2}{2}$ from below so that $|z|$ cannot become too small.
\paragraph{Miller algorithm for $I_\nu(z)$.}
For the intermediate values of $|z|$ not covered by the power series and the asymptotic series, that is
\[ 2\sqrt{\nu +1}<|z|< \max\set{ R_L(\varepsilon), \frac{\nu^2}{2} }, \]
the Miller algorithm in the manner of \cite{OS72} is applied. The three terms relation
\begin{equation}\label{eq:recurr_besselI}
I_{\nu - 1} (z) = \frac{2\nu}{z} I_\nu (z) + I_{\nu + 1} (z)
\end{equation}
is used to generate ratios $r_\nu(z) = I_{\nu+1}(z) / I_\nu (z)$ and normalize on the relation
\begin{subequations}
\begin{align}
\sum_{k=0} ^ \infty \lambda_k I_{\nu + k} (z) &= \frac{z^\nu \ee ^ z}{2^\nu \Gamma(1+\nu)}, & 0&\leq\nu<1,
\end{align}
where the coefficients $\lambda_k$ have values
\begin{equation}
\lambda_k = \begin{cases}
1 & k=0 \\
\dfrac{2(\nu+k)\Gamma(k+2\nu)}{k!\,\Gamma(1+2\nu)} & k\geq 1,
\end{cases}
\end{equation}
\end{subequations}
to obtain the $I$ functions themselves. The computation of the truncation index $M$ is detailed in \cite{OS72} and it is similar to the argument used for $K_\nu(z)$. The method views \eqref{eq:recurr_besselI} as a tridiagonal system of equations which solution is obtained by Gauss elimination. The elimination procedure and error test requires a solution $p_m$ of \eqref{eq:recurr_besselI} which is generated by forward recurrence on $p_{\tilde{\nu}} = 0$ and $p_{\tilde{\nu} + 1}$,
\begin{align}
p_{m+1} & = p_{m-1} - \frac{2m}{z} p_m, & m & = \tilde{\nu} + 1, \tilde{\nu} + 2, \ldots, & \bar{\nu} \geq \floor{|z|}.
\end{align}
We will discuss later how to compute the index $\tilde\nu$.\\
Recurrence continues until
\begin{align}
|p_m| &> T_1 = \sqrt{\frac{|p_L||p_{L+1}|}{\varepsilon}}, & L &= \max\set{ \floor{|z|} + 1, \floor{\nu} + 1 }
\end{align}
is satisfied at index $m=N_1$. Then, recurrence continues until
\begin{equation}
|p_m| \geq T_1 \sqrt{ \frac{\rho_{N_1}}{ \rho_{N_1} ^ 2 - 1 } }
\end{equation}
is satisfied at index $m=M$ where
\[\rho_m  = \min\set{\beta_m,|k_m|}, \]
with
\begin{align*}
\beta_m &= \frac{m+1}{|z|} + \sqrt{ \br{\frac{m+1}{|z|}} ^ 2 - 1} , & k_m &= \frac{ p_{m+1} }{ p_m } = \frac{2m}{z} - \frac{p_{m-1}}{p_m}.
\end{align*}
Backward recurrence with \eqref{eq:recurr_besselI} from $m=M$,
\begin{subequations}\begin{align}\label{eq:besselI_y_recurrence}
y_{M+1} ^ M = 0,\qquad y_M^M = \varepsilon,\span\span\\
y_{m-1}^M = \frac{ 2 (\nu_f + \tilde\nu + m) }{ z } y_m^M + y_{m-1}^M,&& m=M,M-1,\ldots,1,
\end{align}\end{subequations}
guarantees that the ratios, beginning at $\tilde\nu + \nu_f$, with $\nu_f = \nu - \floor{\nu}$,
\begin{align}\label{eq:besselI_r_ratio}
r_{\tilde\nu + \nu_f + m}^M &= \frac{ y_{m+1}^M }{ y_m^M }, & m&= L,L-1,\ldots,0
\end{align}
have relative errors no more than $\varepsilon$. In this implementation the largest possible $\tilde\nu = \max\set{ \floor{|z|}, \floor{\nu} }$ is taken. Then \eqref{eq:recurr_besselI} takes the form
\[ |p_m| > T_1 = \sqrt{ \frac{ |p_{\nu+2}| }{ \varepsilon } } .\]
When $\nu < |z|$, further recurrence from $\tilde\nu + \nu_f$ to $\nu$ using \eqref{eq:besselI_y_recurrence} is required.\\
The procedure for the normalizing relation is
\[ S_M = y_0^M + \sum_{k=1}^M \lambda_ky_k^M + R_M, \]
where an estimate of the relative truncation error used in the test is
\begin{align}\label{eq:besselI_mill_trunc_err_est}
|R_M| &= \frac{ 2\rho_{ \tilde\nu + 2 }^2 (M+1) ^ 2 }{ ( \rho_{\tilde\nu+2} ^ 2 - 1 )( \rho_{\tilde\nu +2} - 1 )| p_M | } < \varepsilon, &
\rho_{\tilde\nu + 2} &= \beta_{\tilde\nu + 2},
\end{align}
for the index $M$ if $\nu < |z|$. If $\nu\geq|z|$, the maximum of $\floor{\nu} + M$ in \eqref{eq:besselI_y_recurrence} and $\floor{|z|} + M$ in \eqref{eq:besselI_mill_trunc_err_est} is used to start backward recurrence.
\paragraph{Wronskian relation for $I_\nu (z)$.}
For $R_L(\varepsilon) <|z| < \nu_L(\varepsilon)$ and $ \sqrt{2|z|} < \nu < \nu_L(\varepsilon)$ the following relation is used to compute $I_\nu(z)$:
\begin{equation}
I_\nu(z) = \frac{1}{ z \br{ K_{\nu+1} (z) + r_\nu (z) K_\nu (z) } }
\end{equation}
where $r_\nu$ is defined by \eqref{eq:besselI_r_ratio}.

\paragraph{Asymptotic expansion of $I_\nu(z)$ as $\nu\to\infty$.}
The case $I_\nu(z)$ as $\nu\to\infty$ is handled in the same way as for $K_\nu(z)$. We consider the uniform asymptotic expansion
\begin{align}\label{eq:asymp_besselI}
I_\nu (z) &\approx \sqrt{ \frac{t}{2\pi\nu} } \ee ^ { \nu\xi } \br{ 1+\sum_{k=1} ^ \infty \frac{U_k(t)}{\nu ^ k} }, & |\arg(z)| &< \frac{\pi}{2},
\end{align}
where $t,\xi$ and the $U_k$'s are defined by \eqref{eqs:asymp_besselI_besselK_coeff}.\\
As for the asymptotic expansion for $K_\nu(z)$ \eqref{eq:asymp_besselK}, also \eqref{eq:asymp_besselI} is more useful when $|\arg(z)|\leq \nicefrac{\pi}{3}$. Thus, also for this function, the expansions \eqref{eqs:asymp_hankel_bessel} are used, and then $I_\nu(z)$ is recovered by
\begin{equation}
I_\nu(z) = \ee ^ {\mp \frac{\ii\pi\nu}{2}} J_\nu \br { z \ee ^ {\pm \frac{\ii\pi}{2}} };
\end{equation}
while analytic continuation is handled by means of the formula
\begin{align}
I_\nu \br{z \ee ^{ \pm\ii\pi\nu }} & = \ee ^ { \pm\ii\pi\nu } I_\nu(z), & \Re z & > 0,
\end{align}
where a positive rotation is used when $z$ is in the fourth quadrant, and a negative rotation is used when $z$ in in the first one.\\

For values $|z| > \nu_L(\varepsilon)$ and $\sqrt{2|z|}<\nu<\nu_L(\varepsilon)$, $I_\nu(z)$ is computed using this asymptotic expansion, followed by a backward recurrence.

\paragraph{Resolution of the indeterminant form for $z = \nu$ in \eqref{eqs:asymp_hankel_bessel}.}
The implementation of \eqref{eqs:asymp_hankel_bessel} seems to be straightforward. However, when $z = \nu,\zeta = 0$ and the terms $A_k(\zeta)$ and $B_k(\zeta)$ become infinite, leading to $\infty~-~\infty$ types of indeterminant forms. The resolution of these indeterminant forms is accomplished by means of the power series in the variable
\[ w^2 = 1 - \br{ \frac{ z }{ \nu } } ^ 2 \]
which is zero when $z=\nu$. Thus, we seek to compute coefficients for the expansions
\begin{align*}
\zeta & = w^2 \sum_{j=0} ^ \infty d_j w ^ {2j}, & \phi(z) & = \sqrt{2} \br{ \frac{ \zeta }{ w^2 } } ^ \frac{1}{4} = \sum_{j=0} ^ \infty \varphi_j w ^{2j},\\
A_k(\zeta) & = \sum_{r=0} ^ \infty a_r(k) w ^{2r}, & B_k(\zeta) & = \sum_{r=0} ^ \infty b_r(k) w ^{2r}.
\end{align*}
This is accomplished by noting
\[ \zeta ^ {\frac{3}{2}} = \frac{3}{2} \br{\frac{1}{2} \log \br{ \frac{1+w}{1-w} } - w} = \frac{3}{2} w ^ 3 \sum_{k=0} ^ \infty \frac{ w ^ {2k} }{ 2k+3 }\]
and writing the power series for $\zeta ^ {-\nicefrac{3s}{2}}$ and $\zeta ^ {-\nicefrac{3(s+1)}{2}} $
\begin{subequations}
\begin{align}
\zeta ^ {-\frac{3s}{2}} & = w ^ {-3s} \sum_{j=0} ^ \infty u_j(s) w ^{2j},\\
\zeta ^ {-\frac{3(s+1)}{2}} & = w ^ {-3s - 1} \sum_{j=0} ^ \infty v_j(s) w ^{2j},
\end{align}
\end{subequations}
so that
\begin{align*}
A_k(\zeta) & = \sum_{s=0}^{2k} \frac{\mu_s}{w^{2k-s}} \br { \sum_{j=0}^{2k - s} c_j(2k-s) w ^{-2j} } w ^{-3s} \sum_{i=0}^\infty u_i(s) w ^{2i}\\
& = \sum_{s=0}^{2k} \mu_s \sum_{j=0} ^ {2k - s} c_j (2k - s) w ^ {-2(k+s+j)} \sum_{i=0}^\infty u_i(s) w ^{2i}
\end{align*}
Now, the reciprocal powers of $w^2$ must drop out in order to have a limit as $w\to 0$. This leads to the condition $i > k+s+j$ and
\begin{equation}
A_k(\zeta) = \sum_{r=0}^{\infty} \br{ \sum_{s=0} ^ {2k} \mu_s \sum_{j=0}^{2k-s} c_j(2k-s) u_{k+s+j+r} (s) } w ^{2r}.
\end{equation}
Then
\begin{align}
a_r(k) & = \sum_{s=0}^{2k} \mu_s \sum_{j=0} ^ {2k-s} c_j(2k-s) u_{k+s+j+r} (s), & r>0, \quad k > 0.
\end{align}
Analogously for $b_r(k)$,
\begin{equation}
B_k(\zeta) = -\sum_{s=0}^{2k+1} \lambda_s \sum_{j=0}^{2k-s+1} c_j (2k-s+1) w ^{-2(k+s+j+1)} \sum_{i=0}^\infty v_i(s) w ^{2i}.
\end{equation}
This time, we must have $i>k+s+j+1$ and we get
\begin{equation}
b_r(k) = -\sum_{s=0}^{2k+1}\lambda_s\sum_{j=0}^{2k-s+1} c_j(2k-s+1)v_{k+s+j+r+1}(s).
\end{equation}
Thus, we have explicit formulae for the coefficients $d_j, \varphi_j,a_r(k)$ and $b_r(k)$. These can be computed if we can compute $\set{c_k}$ in terms of $\set{a_k}$ in the expression
\[ \exp\br{ \alpha \, \log\br{ \sum_{k=0}^\infty a_k x^k } } = \sum_{k=0}^\infty c_k x ^ k. \]
The recurrence needed for this task results from a straightforward manipulation of power series.
%%%%%%%%%%%%%%%%%%%%
% HANKEL FUNCTIONS %
%%%%%%%%%%%%%%%%%%%%
\section{Hankel functions}
The classical solutions to equation \eqref{eq:bessel_eq} $J_\nu(z)$ and $Y_\nu(z)$ will be discussed later (since $Y_\nu(z)$ is computed using relations with Hankel functions that we discuss now). Another pair of solution is given by the \emph{Bessel functions of third kind} or \emph{Hankel functions} defined as
\begin{subequations}
\begin{align}
H^{(1)}_\nu(z) & = J_\nu (z) + \ii Y_\nu (z), & \nu&\in\CC, \\
H^{(2)}_\nu(z) & = J_\nu (z) - \ii Y_\nu (z), & \nu&\in\CC.
\end{align}
\end{subequations}

As others Bessel functions, also Hankel functions satisy various symmetry and reflection properties.
\begin{prop}[Symmetry and reflection formulae for Hankel functions]
Hankel functions $H^{(1)}_\nu(z)$ and $H^{(2)}_\nu(z)$ satisfy
\begin{subequations}
\begin{align}
H^{(1)}_{-\nu}(z) & = \ee ^{ \ii\nu\pi }H^{(1)}_\nu(z), & \nu&\in\CC,\\
H^{(2)}_{-\nu}(z) & = \ee ^{ -\ii\nu\pi }H^{(2)}_\nu(z), & \nu&\in\CC,\\
H^{(1)}_{\nu}(\bar{z}) & = \conj{ H^{(2)}_\nu (z) }, & \nu & \in\RR, \\
H^{(2)}_{\nu}(\bar{z}) & = \conj{ H^{(1)}_\nu (z) }, & \nu & \in\RR.
\end{align}
\end{subequations}
\end{prop}
\subsection{Numerical evaluation}
Hankel functions are evaluated using two relations with the modified Bessel function of second kind $K_\nu (z)$. In particular, they are computed as
\begin{subequations}
\begin{align}
H^{(1)}_\nu (z) & = \frac{2}{\pi\ii} \ee ^ {-\frac{\ii\pi\nu}{2}} K_\nu \br { z\ee ^ {-\frac{\ii\pi}{2}} },\\
H^{(2)}_\nu (z) & = - \frac{2}{\pi\ii} \ee ^ {\frac{\ii\pi\nu}{2}} K_\nu \br { z\ee ^ {\frac{\ii\pi}{2}} }.
\end{align}
\end{subequations}
%%%%%%%%%%%%%%%%%%%%
% BESSEL FUNCTIONS %
%%%%%%%%%%%%%%%%%%%%
\section{Bessel function of first kind $ J_\nu (z) $ and of second kind $Y_\nu (z) $}\label{sec:besselj}
The Bessel functions of first and second kind are the solution of \eqref{eq:bessel_eq}, and they are defined as
\begin{align}\label{eq:def_besselJ}
J_\nu (z) & = \br{\frac{z}{2}}^\nu \sum_{k=0}^\infty \frac{(-1)^k}{k! \Gamma(\nu+k+1)}\br{\frac{z}{2}}^{2k}, & |\arg (z) | < \pi, \quad \nu\in\CC.
\end{align}
and
\begin{align} \label{eq:def_besselY}
Y_\nu (z) & = \frac{J_\nu (z) \cos(\nu\pi) - J_{-\nu}(z)}{\sin(\nu\pi)}, & |\arg(z)|<\pi,\quad \nu\in\CC\setminus\ZZ
\end{align}
respectively. For integer order $n\in\ZZ$, the right hand side of \eqref{eq:def_besselY} is replaced by its limit
\begin{align*}
Y_n (z) &= \lim_{\nu\to n} (J_\nu (z) \cot (\nu\pi) - J_{-\nu} (z) \csc (\nu\pi) ) & n\in\ZZ.
\end{align*}
The first one admits also an integral definition:
\begin{align*}
J_\nu(z) & = \frac{1}{\pi} \int_0^\pi \cos (\nu\theta - z\sin(\theta))\diff \theta - \frac{\sin(\nu\pi)}{\pi}\int_0^\infty \ee ^{ -z\sinh(t) - \nu t} \diff t , & \Re z > 0,
\end{align*}
which, for $\nu\in\NN$ simplifies in
\begin{align*}
J_n(z) &= \frac{1}{\pi} \int_0^\pi \cos(n\theta - z\sin(\theta))\diff \theta, & \Re z &> 0.
\end{align*}

The functions $J_\nu (z)$ and $Y_\nu (z)$ are linearly independent for all $\nu\in\CC$ and the functions $J_\nu (z)$ and $J_{-\nu} (z)$ are linearly independent for $\nu\in\CC\setminus\ZZ$. The function $J_\nu (z)$ is also called \emph{cylinder function}, and the function $Y_\nu (z)$ is also called the \emph{Weber function} or the \emph{Neumann function}. In the special case $\nu \in \NN_0$ the Bessel functions of the first kind are also known as \emph{Bessel coefficients}. This terminology follows from the fact that the functions $J_n (z)$ can be used to express the functions $\sin(z \sin (\theta))$ and $\cos(z\sin(\theta))$ as follows:
\begin{align*}
\sin (z\cos(\theta)) & = \sum_{n=-\infty}^\infty J_n(z) \sin(n\theta),
\end{align*}
and
\begin{align*}
\cos (z\sin(\theta)) & = \sum_{n=-\infty}^\infty J_n(z) \cos(n\theta).
\end{align*}
\begin{prop}[Symmetry and reflection formulae for $J_\nu(z)$ and $Y_\nu(z)$]
The Bessel functions $J_\nu (z)$ and $Y_\nu (z)$ satisfy the symmetry properties
\begin{subequations}
\begin{align}
J_\nu (\bar z) & = \conj{J_\nu(z)}, & \nu\in\RR, \label{eq:symm_besselJ} \\ 
Y_\nu (\bar z) & = \conj{Y_\nu(z)}, & \nu\in\RR. \label{eq:symm_besselY}
\end{align}
\end{subequations}
and the reflection formulae
\begin{subequations}
\begin{align}
J_{-n} (z) & = (-1)^n J_n (z), & n\in\ZZ, \label{eq:refl_besselJ} \\ 
Y_{-n} (z) & = (-1)^n Y_n(z), & n\in\ZZ. \label{eq:refl_besselY}
\end{align}
\end{subequations}
\end{prop}
\subsection{Numerical evaluation}
Bessel functions $ J_\nu (z) $ and $ Y_\nu(z) $ are computed using relations with modified Bessel function $I_\nu(z)$ and Hankel functions $H^{(1)}_\nu (z)$ and $H^{(2)}_\nu(z) $. In fact, $J_\nu(z)$ is computed as
\begin{align}
J_\nu (z) & = \ee ^ {\mp\frac{\ii\pi\nu}{2}} I_\nu \br{ z \ee ^{ \pm\frac{\ii \pi}{2} } }, & \Re z \geq 0;
\end{align}
while analytic continuation for $\Re z < 0$ is obtained through the formula
\begin{align}
J_\nu \br{ z \ee ^ { \pm\ii\pi } } & = \ee ^ {\pm\ii\pi\nu} J_\nu(z), & \Re z & > 0.
\end{align}
The Bessel function of second kind $Y_\nu(z)$ can be directly computed using Hankel functions as
\begin{equation}
Y_\nu (z) = \frac{H^{(1)}_\nu(z) - H^{(2)}_\nu(z) }{ 2\ii }.
\end{equation}
