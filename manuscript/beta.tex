Another important special function, which is also related to the gamma function, is the so-called \emph{beta function}. It was the first known scattering amplitude in string theory. In this work we will focus specially on the \emph{incomplete beta function}, which is an important probability distribution function.
\section{The beta function}
The \emph{beta function} is defined as
\begin{align}
B(x,y) & = \int _0 ^1 t^{ x - 1 } (1-t) ^ { y - 1 }\diff t, & \Re x > 0,\quad \Re y > 0.
\label{eq:def_beta}
\end{align}
Before seeing how to compute it, we present briefly some properties:
\begin{prop}
The beta function $B(x,y)$ satisfies the following properties:
\begin{subequations}\label{eq:prop_beta}
	\begin{align}
	 B(x,y) &= B(y,x), \label{eq:symm_beta}\\
	B(x+1,y) &= B(x,y)  \frac{x}{x+y},\\
	B(x,y+1) &= B(x,y) \frac{y}{x+y}, \\
	B(x,y) B(x+y,1-y) &= \frac{\pi}{x\sin(\pi y)}. 
	\end{align}
\end{subequations}
\end{prop}
We can relate gamma and beta function thanks to the following result:
\begin{thm} The beta function can be related to the gamma function by
\begin{equation}
B(x,y) = \frac{ \Gamma(x) \Gamma (y) } { \Gamma (x+y) } .
\end{equation}
\end{thm}
\begin{proof}
We compute
\begin{align*}
\Gamma (x) \Gamma (y) 	& = \int_0^\infty \ee^{ -u } u ^ { x - 1 }\diff u \int_0^\infty \ee^ {-v} v ^{ y - 1 }\diff v\\
						& = \int_0^\infty \int_0^\infty \ee ^ {- u - v} u ^ {x-1} v ^ {y-1} \diff u\diff v
\end{align*}
We now make the following change of variables:
\begin{align*}
u & = f(t , z) = zt,\\
v & = g(t , z) = z(1-t),
\end{align*}
obtaining
\begin{align*}
\Gamma (x) \Gamma (y)
	& = \int_0^\infty \int_0^1 \ee ^ {-z} (zt) ^ {x-1} (z(1-t))^{y-1} |J(t,z)|\diff t\diff z\\
	& = \int_0^\infty \int_0^1 \ee ^ {-z} (zt) ^ {x-1} (z(1-t))^{y-1} z\diff t\diff z\\
	& = \int_0^\infty \ee^{-z} z ^{x + y - 1} \diff z \int_0^1 t^{x-1} (1-t) ^ {y-1}\diff t\\
	& = \Gamma (x + y) B(x,y).
\end{align*}
where $|J(t,z)|$ denotes the determinant of the Jacobian of $ u = f(t , z)$ and $ v = g(t,z)$.
\end{proof}
Thanks to this property, we can compute the logarithm of the beta function, $\lbeta(x,y) = \log(B(x,y))$ simply via
\[ \lbeta (x,y)  = \lgamma (x) + \lgamma (y) - \lgamma (x + y),\]
having provided a stable way to compute $\lgamma$ in \S~\ref{sec:gammalog}. Thus, we can obtain $B(x,y)$ easily by computing the exponential of this quantity:
\[B(x,y) = \exp (\lbeta (x,y)).\]
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.5\linewidth]{plots/plot_beta.eps}\includegraphics[width=0.5\linewidth]{plots/plot_beta_log.eps}
\caption{The beta function (on the left) and its base 10 logarithm (on the right).}
\end{figure}
\section{The incomplete beta function}\label{sec:betainc}
The term \emph{incomplete beta function} usually refers to the function
\begin{align*}
B_x(a,b) & = \int _0 ^x t ^ {a-1} (1-t) ^ {b-1}\diff t , & 0 \leq x \leq 1.
\end{align*}
Instead, we will consider in this section the \emph{regularised incomplete beta function}, or, for short, the \emph{regularised beta function}
\begin{align*}
I_x(a,b) & = \frac{B_x(a,b)}{B(a,b)} , & 0 \leq x \leq 1.
\end{align*}
This function satisfies various recurrence formulae.
\begin{prop}[Recurrence formulae for the regularised beta function]
	The function $I_x(a,b)$ satisfies the following properties:
	\begin{align*}
		I_x (a , b) 		& = x I_x (a-1 , b) + (1-x) I_x (a,b-1),\\
		(a+b-ax) I_x (a,b)	& = a (1-x) I_x(a+1,b-1) + b I_x(a,b+1),\\
		(a+b) I_x(a,b)		& =aI_x(a+1,b) + b I_x(a,b+1).
	\end{align*}
\end{prop}
These relations can be used to gain more accuracy in some implementations. Actually, we did not use them in our algorithm, but only the following symmetry property.
\begin{prop}[Symmetry of the regularised beta function]
	The regularised beta function satisfies
	\begin{equation}
		I_x(a,b) = 1 - I_{1-x} (b,a).
	\label{eq:betainc_u_l}
	\end{equation}
\end{prop}
\begin{proof}
By definition, we can write $I_{1-x}(b,a)$ as
\begin{align*}
 I_{1-x} (b,a) &= \frac{1}{B(b,a)} \int_0^{1-x} t^{b-1} (1-t)^{a-1}\diff t .
\end{align*}
If we now make the change of variable $ \t = 1-t $, we obtain
\begin{align*}
I_{1-x} (b,a) 	& = -\frac{1}{B(b,a)} \int _1 ^ x (1-\t) ^ {b-1} \t ^ {a-1}\diff \t\\
				& = \frac{1}{B(b,a)} \int _x ^ 1 (1-\t) ^ {b-1} \t ^ {a-1}\diff \t .
\end{align*}
We thus obtain, renaming $\t$ in $t$ and using the symmetry property \eqref{eq:symm_beta} of $B(b,a)$,
\begin{align*}
I_x(a,b) + I_{1-x}(b,a) & = \frac{1}{B(a,b)}\int_0^x t^{b-1} (1-t)^{a-1}\diff t + \frac{1}{B(a,b)}\int_x^1 t^{b-1} (1-t)^{a-1}\diff t\\
						 & = \frac{1}{B(a,b)} \int _0 ^ 1	t^{b-1} (1-t)^{a-1}\diff t\\
						 & = \frac{1}{B(a,b)} B(a,b)\\
						 & = 1,
\end{align*}
as claimed.
\end{proof}
\subsection{Numerical evaluation}
The regularised beta function has series representation \cite{AS64}
\begin{subequations}\label{eqs:betainc_series}
\begin{align}\label{eq_betainc_series_1}
I_x(a,b) & = \frac{x^a}{aB(a,b)} \, {}_2F_1(a,1-b;a+1;x), & a,b>0,\quad 0\leq x < 1,
\end{align}
where ${}_2F_1(a,b;c;z)$ is the hypergeometric series \eqref{eq:hyperg21}. Alternatively, by using the relation \eqref{eq:hypergeom_recurr3}, we get
\begin{align}\label{eq:betainc_series_2}
I_x(a,b) & = \frac{x^a(1-x)^b}{aB(a,b)}\,{}_2F_1(1,a+b;a+1;x), & a,b>0,\quad 0\leq x < 1.
\end{align}
\end{subequations}
From the series representation \eqref{eq:betainc_series_2} and the C-fraction \eqref{eq:hyperg_C_frac_2}, we obtain the following C-fraction representation for $I_x(a,b)$:
\begin{subequations}\label{eqs:betainc_C_frac}
\begin{align}
I_x(a,b) & = \frac{x^{a-1}(1-x)^b}{aB(a,b)} \K_{m=1}^\infty \br {\frac{\alpha_m(a,b) x}{1}}, & a,b>0,\quad 0\leq x < 1,
\end{align}
where
\begin{align}
\alpha_1(a,b) & = 1,\\
\alpha_{2j+2}(a,b) &= -\frac{(a+j)(a+b+j)}{(a+2j)(a+2j+1)}, & j\geq 0,\\
\alpha_{2j+1}(a,b) &= \frac{j(b-j)}{(a+2j-1)(a+2j)}, & j\geq 1.
\end{align}
\end{subequations}
From the series representation \eqref{eq:betainc_series_2} and the M-fraction representation \eqref{eqs:hyperg_M_frac}, we obtain
\begin{subequations}\label{eq:betainc_M_frac}
\begin{multline}
\frac{B(a,b) I_x(a,b)}{x^a(1-x)^b} = \frac{1}{a+(1-a-b)x}\cplus \K_{m=2}^\infty \br { \frac{c_m(b)x}{e_m(a) + d_m(a,b) x} },\\
	a,b > 0,\quad 0\leq x < 1,
\end{multline}
where
\begin{align}
c_j(b) &= (j-1)(b-j+1), &  j\geq 2, \\
e_j(a) &= a+j-1, &  j\geq 2,\\
d_j(a,b) &= -(a+b-j), &  j\geq 2.
\end{align}
\end{subequations}
Both \eqref{eqs:betainc_C_frac} and \eqref{eq:betainc_M_frac} can be used to compute $I_x(a,b)$. However, numerical experiments reveal that the following continued fraction, given by \cite{DM92}, converges in fewer steps:
\begin{subequations}\label{eqs:betainc_C_F_cool}
\begin{align}
\frac{B(a,b)I_x(a,b)}{x^a(1-x)^b} & = \K_{m=1}^\infty \br{\frac{\alpha_m(a,b;x)}{\beta_m(a,b;x)}}, & a,b>0,\quad 0\leq x < 1,
\end{align}
where the coefficients $\alpha_m(x)$ and $\beta_m(x)$ are given by
\begin{align}
\alpha_1(a,b;x) &= 1,\\
\alpha_{j+1} (a,b;x) &= \frac{(a+j-1)(a+b+j-1)(b-j)j}{(a+2j-1)^2}\,x^2, & j\geq 1,\\
\beta_{j+1} (a,b;x) &= a + 2j + \br{ \frac{j(b-j)}{a+2j-1} - \frac{(a+j)(a+b+j)}{a+2j+1} }x, & j\geq 0.
\end{align}
\end{subequations}
In view of the fact that \eqref{eqs:betainc_C_F_cool} is more useful when $x\leq a/(a+b)$, the role of $a$ and $b$ and of $x$ and $1-x$ may need to be interchanged, using \eqref{eq:betainc_u_l}, when evaluating $I_x(a,b)$.\\
We are interested also in computing the ``upper'' incomplete beta function ratio
\begin{align}
I_x^U (a,b) & = \frac{1}{B(a,b)} \int_x^1 t ^ {a-1} (1-t) ^ {b-1}\diff t , & 0 \leq x \leq 1.
\end{align}
Trivially we have
\begin{align}\label{eq:betainc_upper_rel}
I_x(a,b) + I_x^U (a,b) &= 1, & 0\leq x \leq 1.
\end{align}
That means that we can easily compute it as
\[ I_x^U (a,b) = I_{1-x} (b,a).\]
Actually, our implementation computes the continued fraction for the version of the incomplete beta function which satisfies $x\leq a/(a+b)$, and then, depending on the version which is required, proceed, if necessary, to rescale the obtained value using \eqref{eq:betainc_upper_rel}.
\subsection{Numerical results}
To test our implementation, we compute the relative error for various values of $x$, $a$ and $b$. We report some of these results in Figure~\ref{fig:betainc_result}. The tests have been done on the lower version for values $x \in[0,0.5]$. As for the incomplete gamma function, the reference values has been computed with SageMath.
\begin{figure}[hbtp]
\centering
\includegraphics[width=1\linewidth]{plots/plot_err_betainc.eps}
\caption{Relative error (in $\log_{10}$) for the function $I_x(a,b)$.}
\label{fig:betainc_result}
\end{figure}
\section{The inverse of the incomplete beta function}
We are interested in the inversion of $I_x(a,b)$ and $I_x^U(a,b)$ with respect to $x$ for fixed parameters $a$ and $b$. The inversion of the incomplete beta function is quite simpler than the inversion of the incomplete gamma function. Indeed, since $I_x(a,b) : [0,1]\to[0,1]$, as function of $x$, with $a$ and $b$ fixed parameters, we can estimate an initial guess using a startdand bisection method. With just ten iterations, we can obtain a three digits accuracy value (since we divide the interval $2^{10}\approx 1000$ times), which is enough to start a stable Newton's method, where the derivative with respect to $x$ is
\[\pderiv{I_x(a,b)}{x} = \frac{x^{a-1}(1-x)^{b-1}}{B(a,b)}\]
for the lower version, while it is
\[\pderiv{I_x^U(a,b)}{x} = -\frac{x^{a-1}(1-x)^{b-1}}{B(a,b)}\]
for the upper one.\\
Since the solution $x_L = x(y,a,b)$ of the lower version is equal to the solution $x_U = x (1-y, a, b)$ of the upper version, we decided to invert the lower version $I_x(a,b)$ when $y \leq 0.5$, while we invert the upper version $I_x^U(a,b)$ when $y > 0.5$.
