The exponential integrals $E_n(z)$ and $\Ei (z)$ and the related functions form a family of hypergeometric functions. These functions can often be found as solutions of thermal diffusion problem \cite{BT98}.
\section{Exponential integral}
The term \emph{exponential integral} usually refers to a set of functions which depend on a parameter $ n \in \NN $.
\begin{mydef}
The \emph{exponential integrals} $ E_n (z) $ are defined by
\begin{align}\label{eq:expint_def}
E_n (z) & = \int_1^\infty \frac{ \ee ^ { -zt } }{ t ^ n } \diff t, & \Re z > 0, \quad n \in \NN.
\end{align}
\end{mydef}
Some refers to exponential integral to the function
\begin{align}\label{eq:Ei_def}
\Ei (x) & = - \int_{-x}^\infty \frac{\ee^{-t}}{t}\diff t, & x>0,
\end{align}
where the integral has to be intended in the sense of its principal value. It is extended to the negative real axis by 
\begin{align*}
\Ei (-x) & = -E_1 (x), & x > 0 .
\end{align*}

Analytic continuation of $ E_n (z) $ to the cut plane $ |\arg(z)| < \pi $ extends the definition and yields a single-valued function. The functions $ E_n (z) $ are related to the complementary incomplete gamma function $ \Gamma (a , z)$, defined by \eqref{eq:gamma_Gamma_def}, by
\begin{align*}
E_n(z) 	& = z ^ {n-1} \Gamma (1 - n , z), & \Re z > 0 ,\quad n \in \NN,\\
		& = z ^ {n-1} \int_z ^\infty \frac {\ee^{-t}}{t^n}\diff t.
\end{align*}
Since $ \Gamma (a,z) $ is defined for all $ a \in \CC $, the exponential integrals $ E_n(z)$ can be continued analytically by
\begin{align}\label{eq:expint_anal_cont}
E_\nu (z) & = z^{\nu - 1}\Gamma (1-\nu , z), & |\arg (z) | < \pi,\quad \nu \in \CC.
\end{align}
This relationship is crucial in deriving most of the representations for the exponential integrals and related functions.
From \eqref{eq:expint_def} and \eqref{eq:expint_anal_cont}, we obtain
\begin{subequations}\label{eq:symm_rel}
\begin{align}
E_n ( \bar {z}) &= \conj {E_n(z)}, & n\in\NN, \\
E_{\bar{\nu}} (\bar{z}) & = \conj {E_\nu(z)}, & \nu \in \CC.
\end{align}
\end{subequations}
\begin{prop}[Recurrence relations for the exponetial integrals] The functions $E_n(z)$ satisfy the following recurrence relations:
\begin{subequations}\label{eqs:expint_recurr_rel}
\begin{align}
E_{n+1} ( z ) 	& = \frac{\ee^{-z}}{n} - \frac{z}{n} E_n(z), & n \in \NN, \label{eq:expint_recurr_1}\\
E_n(z)			& = \ee^{-z} \sum_{k=0} ^ {r-1} \frac{(-1)^k(n)_k}{z ^ {k+1}} + \frac{(-1)^r (n)_r}{z ^ r} E_{n + r}(z), & n,r\in\NN, \label{eq:expint_recurr_2}
\end{align}
\end{subequations}
where $(\cdot)_r$ denotes the Pochhammer symbol introduced in \eqref{eq:def_pochhammer}.
\end{prop}
\begin{proof}
To prove \eqref{eq:expint_recurr_1} we just need to integrate by parts:
\begin{align*}
E_{n+1} ( z ) 	& = \int_1^\infty \ee^{-zt} t^{-n-1}\diff t \\
				& =-\sbr{\frac{t^{-n}}{n}\,\ee^{-zt}}_{t=1}^{t=\infty} - \int_1^\infty -z\ee^{-zt} \br{-\frac{t^{-n}}{n}}\diff t \\
				& = \frac{\ee^{-z}}{n} -\frac{z}{n} \int_1^\infty \ee^{-zt} t^{-n}\diff t\\
				& = \frac{\ee^{-z}}{n} -\frac{z}{n} E_n(z).
\end{align*}
Relation \eqref{eq:expint_recurr_2} follows by iterating $r$ times relation \eqref{eq:expint_recurr_1}.
\end{proof}
\subsection{Numerical evaluation}
We focus on the function
\[ E_1 (z) = \int_1^\infty \frac{\ee^{-zt}}{t}\diff t,\]
and we remark that, by making the change of variable $\t = zt$ in the integral, we can rewrite $E_1$ as
\[ E_1(z) = \int_z^\infty \frac{\ee^{-t}}{t}\diff t. \]

Even if $ E_1 $ could be computed using the relation with the incomplete gamma function as $\Gamma (0,z)$, due to its importance it is convenient to have an independent, more accurate way to compute it. Moreover, in this way, we will be also able to evaluate it for any complex value of $z$, while for the incomplete gamma functions we limited ourselves on real values.\\
We will use three different strategies: series expansion, continued fractions and asymptotic expansion. We tested these three representation in the complex plane, obtaining a partition of the same schematized in Figure~\ref{fig:expint_scheme}.
\begin{figure}[hbt]
\centering
\resizebox{0.8\textwidth}{!}{
\input{manuscript/expintscheme.tex}}
\caption{Computation diagram for $E_1 (z)$. The complex plane is divided into three regions, delimited by two ellipses. Inside the blue ellipse we use the series expansion, inside the green one we use the continued fractions, in the red part (all the remaining values) we use the asymptotic expansion.}
\label{fig:expint_scheme}
\end{figure}
%% SERIES
\paragraph{Series expansion.}
Simply by integrating the Taylor series for $ \ee^{-t}/t $, it is possible to obtain the following series expansion for $E_1(z)$:
\begin{align} \label{eq:expint_series}
E_1(z) & = -\gamma -\log (z) - \sum_{k=1}^\infty \frac{(-1)^kz^k}{k\,k!}, & |\arg(z)|<\pi,
\end{align}
where $\gamma$ denotes the Euler-Mascheroni constant. We remark that the coefficients $c_k = (-1)^k / (k\, k!)$ of the sum satisfy
\[ c_{k+1} = -\frac{k}{(k+1)^2} c_k.  \]
This permit to easily compute the next term of the sum.\\
We used this expansion when $z$ satisfies
\[ \frac{(\Re z + 19.5)^2}{20.5^2} + \frac{(\Im z) ^2}{10^2} \leq 1. \]
%% C.F.
\paragraph{Continued fractions.}
Based on the continued fraction representation for the incomplete gamma function \eqref{eqs:Gamma_CF} we get the modified S-fraction representation
\begin{subequations}
\begin{align}
\ee^z E_n (z) & = \frac{1}{z} \cplus \frac{n}{1} \cplus \frac{1}{z} \cplus \frac{n+1}{1} \cplus \frac{2}{z} \cplus \frac{n+2}{1}\cplus \contdots , & |\arg(z)|<\pi,\quad n\in\NN \nonumber\\
			& = \frac{\frac{1}{z}}{1} \cplus \K_{m=2}^\infty \br { \frac{\frac{a_m(n)}{z}}{1} },  & |\arg(z)|<\pi,\quad n\in\NN,
\end{align}
with coefficients
\begin{align}
a_{2j}(n) &= j+n-1, & j\geq 1, \\
a_{2j+1}(n) &= j, & j\geq 1.
\end{align}
\end{subequations}\label{expint_CF}
Thus, we have, for $E_1(z)$,
\begin{subequations}
\begin{align}
E_1(z) & = \frac{\ee ^{-z}}{z} \cplus \K_{m=2}^\infty \br{\frac{\alpha_m}{\beta_m}}, & |\arg(z)|&<\pi,
\end{align}
where $\alpha_m$ and $\beta_m$ are defined as
\begin{align}
\alpha_j & = \floor{\frac{j}{2}}, & j \geq 1,\\
\beta_j & = \begin{cases}
z & m\text{ odd,}\\
1 & m\text{ even,}
\end{cases} & j\geq 1.
\end{align}
\end{subequations}
We used this expansion when $z$ satisfies
\[ \frac{(\Re z + 1)^2}{38^2} + \frac{(\Im z) ^2}{40^2} \leq 1, \]
excluding, obviously, the terms already computed using the series expansion.

\paragraph{Asymptotic expansion.}
To obtain such an expansion, let us consider the function \cite{BH86}
\begin{align}\label{eq:expint_support}
I(x) & = x \, \ee^{x} E_1(x), & x \in \RR^+.
\end{align}
In trying to estimate $I$ for large arguments, it is natural to seek for an expansion about $x = \infty$. Such an expansion is quickly obtained by repeatedly integrating by part \eqref{eq:expint_support}. Indeed, after $N$ integrations by parts, we obtain
\[ I(x) = \underbrace{\sum_{k=0}^{N-1} \frac{(-1)^k k!}{x^k}}_{S_N(x)} + \underbrace{(-1)^N N! \, x \, \ee^x \int_x^\infty \frac{\ee^{-t}}{t^{N+1}}\diff t}_{\varepsilon (x,N)}.\]
Unfortunately, the series
\[S_\infty (x) = \sum_{k=0}^{\infty} \frac{(-1)^k k!}{x^k}\]
is divergent, since the ratio between successive terms is
\begin{equation}\label{eq:ratio_asymp_expint}
 \left| \frac{(k+1)\text{-st term}}{k\text{-th term}} \right| = \frac{n}{x},
\end{equation}
and it increases without bound as $n\to\infty$ for any $x$. We now observe that, since $x$ is positive, $\varepsilon (x,N)$ is positive when $N$ is even and negative when $N$ is odd. This implies that
\begin{align*}
S_N(x)\leq I(x) \leq S_{N+1} (x) && N\text{ even},\\
S_{N+1}(x)\leq I(x) \leq S_{N} (x) && N\text{ odd}.\\
\end{align*}
Thus, for any value of $x$, the actual value of $I(x)$ lies between two successive partial sums of the divergent series. Obviously, for fixed $x$, the best approximation for $I(x)$ is the one achieved for the integer $N$ which minimizes $|\varepsilon (x,N)|$.\\
Since $\varepsilon (x,N)$ alternates in sign with $N$, the following relation holds:
\[ |\varepsilon (x,N)| < \frac{N!}{x^N}. \]
Thus, for fixed $x$, we might expect that the estimate $S_N(x)$ improves with $N$ as long as the absolute value of the ratio of successive terms remains less or equal to one. Hence from \eqref{eq:ratio_asymp_expint} we are led to predict that the optimum value for $N$ is
\[ \hat N (x) = \floor{x}. \]
Empirical experiments show that this strategy can be extended to compute $E_1$ for complex values $z$ as follows:
\[ E_1(z) \approx \frac{\ee^{-z}}{z} \sum_{k=0}^N \frac{(-1)^k k!}{z^k}, \]
with
\[N = \floor{|z|}.\]
We used this expansion for all the remaining values.
\subsection{Numerical results}
To test our implementation, we compute the relative error for various values of $\Re z$ and $\Im z$, using as reference values the one computed by the symbolic package of Octave\footnote{\url{https://octave.sourceforge.io/symbolic/}.}. We report some of these results in Figure~\ref{fig:expint_result}.
\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\linewidth]{plots/plot_err_expint.eps}
\caption{Relative error (in $\log_{10}$) for the function $E_1(z)$.}
\label{fig:expint_result}
\end{figure}
\section{Sine integral and cosine integral}
The \emph{sine integral} and the \emph{cosine integral} are defined as 
\begin{align} \label{eq:sinint_def}
\Si (z) & = \int_0^z \frac{\sin{(t)}}{t}\diff t, & z\in\CC,
\end{align}
and
\begin{align} \label{eq:cosint_def}
\Ci (z) & = - \int_z^\infty \frac{\cos(t)}{t}\diff t, & | \arg (z) | < \pi,
\end{align}
respectively.\\
The cosine integral $\Ci$ has an equivalent forulation.
\begin{prop}[Equivalent formulation of the cosine integral] The function $\Ci$ can be written as
\begin{align}\label{eq:cosint_equiv_def}
\Ci (z)& = \gamma + \log (z) + \int_0^z \frac{\cos{(t)} - 1}{t}\diff t, & |\arg (z) | < \pi,
\end{align}
where $\gamma$ is the Euler-Mascheroni constant.
\end{prop}
Another important result, that will be useful in obtaining an efficient way to compute $\Si$ and $\Ci$ is the \emph{Dirichlet integral}.
\begin{thm}[Dirichlet integral]
The value of $\,\,\Si$ at infinity is
\begin{equation}\label{eq:sinint_oo}
\lim_{|z|\to\infty}\Si (z) = \int_0^\infty \frac{\sin (t)}{t} \diff t = \frac{\pi}{2}.
\end{equation}
\end{thm}
\subsection{Strategies of computation}
To compute the sine and cosine integral functions, we employed two strategies: for small values of $|z|$ we used a series expansion, while for big values of $|z|$ we used some relations with $E_1(z)$.
\paragraph{Relations with $E_1(z)$.}
We start by proving the following relation between $E_1,\Ci$ and $\Si$.
\begin{prop}
The exponential integral, sine integral and cosine integral are related by
\begin{equation}\label{eq:rel_int_fnc}
E_1 (\ii z) = -\Ci (z) + \ii\br{ \Si (z) - \frac{\pi}{2} }.
\end{equation}
\end{prop}
\begin{proof}
By definition and making the change of variables $\t = -\ii t$ between \eqref{eq:proof_rel_1} and \eqref{eq:proof_rel_2}
\begin{subequations}
\begin{align}
E_1 (\ii z)	& = \int_{\ii z}^\infty \frac{\ee^{-t}}{t} \diff t \label{eq:proof_rel_1}\\
			& = \int_z^\infty \frac{\ee^{-\ii\t}}{\ii\t} \, \ii\diff\t \label{eq:proof_rel_2}\\
			& = \int_z^\infty \sbr{ \frac{\cos(-\t)}{\t} + \frac{\ii\sin(-\t)}{\t} } \diff \t \nonumber \\
			& = \int_z^\infty \frac{\cos(\t)}{\t}\diff \t - \int_z^\infty \frac{\ii\sin(\t)}{\t}\diff\t \nonumber \\
			& = -\Ci(z) - \ii\sbr{ \int_0^\infty \frac{\sin(\t)}{\t}\diff\t - \int_0^z \frac{\sin(\t)}{\t}\diff \t } \label{eq:proof_rel_3}\\
			& =  -\Ci (z) - \ii\br{\frac{\pi}{2} - \Si (z)}\label{eq:proof_rel_4} \\
			& = -\Ci (z) + \ii\br{\Si (z) - \frac{\pi}{2}}, \nonumber 
\end{align}
\end{subequations}
where we used the Dirichlet integral \eqref{eq:sinint_oo} between \eqref{eq:proof_rel_3} and \eqref{eq:proof_rel_4}.
\end{proof}
From this, we can express both the sine integral and the cosine integral using $E_1$.
\begin{cor}
The sine integral and cosine integral can be expressed as
\begin{align}\label{eq:expint_sinint}
\Si (z) & = -\frac{\ii}{2} \br{ E_1 (\ii z) - E_1 (-\ii z) } + \frac{\pi}{2}, & | \arg (z) | < \frac{\pi}{2},
\end{align}
and
\begin{align}\label{eq:expint_cosint}
\Ci (z) & = -\frac{1}{2} \br{ E_1 (\ii z) + E_1(-\ii z) }, & | \arg (z) | <\frac{\pi}{2},
\end{align}
respectively.
\end{cor}
Besides these relations, we used also some symmetries.
\begin{prop}[Symmetry relation for the sine and cosine integral]
The following relations hold:
\begin{subequations}\label{eqs:symm_sinint}
\begin{align}
\Si(-z) & = -\Si (z),\\
\Si(\bar{z}) &= \conj{\Si(z)},
\end{align}
\end{subequations}
\begin{subequations}\label{eqs:symm_cosint}
and
\begin{align}
\Ci (-z) & = \Ci (z) - \ii \pi & 0<\arg(z)<\pi,\\
\Ci(\bar{z}) & = \conj{\Ci(z)}.
\end{align}
\end{subequations}
\end{prop}
So, it would be possible to evaluate both $\Si$ and $\Ci$ using relations \eqref{eq:expint_sinint}, \eqref{eq:expint_cosint}, \eqref{eqs:symm_sinint} and \eqref{eqs:symm_cosint}. However, these relations are more useful when the value of $E_1$ is ``small''.\\
We used these relations when $ | z | > 2 $.
%% Series
\paragraph{Series expansion.}
By directly integrating the Taylor expansions of $ \sin (t) / t$ and of $ ( \cos (t) - 1) / t $, we obtain the following series expansions for $\Si$ and $\Ci$:
\begin{equation}
\Si (z)  = \sum_{k=0}^\infty \frac{(-1)^k z^{2k+1}}{(2k+1)\,(2k+1)!},
\end{equation}
and
\begin{equation}
\Ci (z)  = \gamma + \log (z) + \sum_{k=0}^\infty \frac{(-1)^k z^{2k}}{2k\,(2k)!}.
\end{equation}
We remark that, if we call $c_k $ the coefficients of the sum defining $\Si(z)$ and $d_k$ the coefficients of the sum defining $\Ci(z)$, that is
\begin{align*}
\Si(z) & = \sum_{k=0} ^\infty c_k z ^{2k+1}, & \Ci (z) & = \gamma + \log(z) + \sum_{k=0}^\infty d_k z ^{2k},
\end{align*}
then, we can recursively construct the coefficients as
\begin{align*}
c_{k+1}  & = -\frac{(2k+1)}{(2k+3)^2 (2k+2)}c_k, \\
d_{k+1}  & = -\frac{(2k)}{(2k+2)^2 (2k+1)}d_k,
\end{align*}
respectively.\\
We used these expansions when $ | z | \leq 2 $.
\subsection{Numerical results}
As for $E_1(z)$, we tested the sine integral and the cosine integral for various values of $\Re z$ and $\Im z$. We focused our tests on the values computed using the series, since the error for values $|z| > 2$ depends on the error done on $E_1(z)$. In Figure~\ref{fig:err_trigint} are reported these results.

\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\linewidth]{plots/plot_err_sinint.eps}
\includegraphics[width=0.8\linewidth]{plots/plot_err_cosint.eps}
\caption{Relative error (in $\log_{10}$) for $\Si$ (upper figure) and for $\Ci$ (lower figure).}
\label{fig:err_trigint}
\end{figure}
