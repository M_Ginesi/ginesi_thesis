clear all
close all

load codes/trigint_ground_truth.mat

N = 30;
x = linspace(-2,2, N);
X = x + 1i * x.';
c = cosint (X);
ERR_REL_C = abs (c - c_exp) ./ abs (c_exp);
[x,y] = meshgrid(x,x);
figure
meshc (x,y,log10(ERR_REL_C))
xlabel('$\Re z$')
ylabel('$\Im z$')
print('-depslatexstandalone','plot_err_cosint','-F:14');
system('latex plot_err_cosint.tex');
system('dvips -E plot_err_cosint.dvi -o plot_err_cosint.eps');

s = sinint (X);
ERR_REL_S = abs (s - s_exp) ./ abs (s_exp);


figure
meshc (x,y,log10(ERR_REL_S))
xlabel('$\Re z$')
ylabel('$\Im z$')
print('-depslatexstandalone','plot_err_sinint','-F:14');
system('latex plot_err_sinint.tex');
system('dvips -E plot_err_sinint.dvi -o plot_err_sinint.eps');
