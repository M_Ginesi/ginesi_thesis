clear all
close all

x = linspace(-5,5,1e3);
y = gamma(x);

figure
plot(x,y)
axis([-5,5,-5,5])
xlabel('$x$')
ylabel('$\Gamma(x)$')
grid on
print('-depslatexstandalone','plot_gamma','-F:14')
system('latex plot_gamma.tex');
system('dvips -E plot_gamma.dvi -o plot_gamma.eps');
