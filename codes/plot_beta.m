clear all
close all

x = linspace(1e-1,10,50);
[X,Y] = ndgrid(x,x);
Z = beta(X,Y);

figure
meshc(X,Y,Z)
xlabel('$x$')
ylabel('$y$')
zlabel('$B(x,y)$')
print('-depslatexstandalone','plot_beta','-F:14')
system('latex plot_beta.tex');
system('dvips -E plot_beta.dvi -o plot_beta.eps');

figure
meshc(X,Y,log10(Z))
xlabel('$x$')
ylabel('$y$')
zlabel('$\log_{10}(B(x,y))$')
print('-depslatexstandalone','plot_beta_log','-F:14')
system('latex plot_beta_log.tex');
system('dvips -E plot_beta_log.dvi -o plot_beta_log.eps');
