clear all
close all

N = 30;
x = linspace(-50,50, N);
X = x + 1i * x.';
load codes/expint_ground_truth.mat
y = expint (X);
ERR_REL = abs (y - y_exp) ./ abs (y_exp);
[x,y] = meshgrid(x,x);

figure
meshc (x,y,log10(ERR_REL))
xlabel('$\Re z$')
ylabel('$\Im z$')
print('-depslatexstandalone','plot_err_expint','-F:14');
system('latex plot_err_expint.tex');
system('dvips -E plot_err_expint.dvi -o plot_err_expint.eps');
